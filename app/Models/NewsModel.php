<?php namespace App\Models;
 
use CodeIgniter\Model;
 
class NewsModel extends Model{

    protected $table = 'news';
    

    public function saveItem($data = array() ){
        if( $data ){
            $type = explode(' ', $data['5']);
            //[0] => ПАО «СМУ-15» [1] => 11/02 [2] => 10.03.2020 [3] => 491350,13 [4] => [5] => Кадастрирование [6] =>
            $this->db->query(" INSERT INTO `table_test` SET 
                `client` = '".$data[0] . "' , 
                `number` = '".$data[1] . "' , 
                `date` = '". date( 'Y-m-d', strtotime($data[2]) ) . "' , 
                `profit` = '".str_replace(',','.', $data[3])."' , 
                `type_work` = '".$data['5'] . "' , 
                `type_pay` = '' , 
                `date_added` = NOW() , 
                `status` = 1 
            ");
            $id = $this->db->insertID();
            foreach ($type as $key => $value) {
                switch ( trim($value) ) {
                    case 'Проектирование':
                        $_type = 1;
                        break;
                    case 'Кадастрирование':
                        $_type = 2;
                        break;
                    case 'Перепланировка':
                        $_type = 3;
                        break;
                    
                    default:
                        # code...
                        break;
                }
                $this->db->query(" INSERT INTO `type_list_work` SET  `id_type` = '". $_type ."', `id_works` = '". $id ."' ");
            }
        }
    }
    
    public function getItemsTotal( $data = array() ){
        if( $data ){
            $sql = ' WHERE 1=1 ';
            $left = '';
            if( isset( $data['type_work'] ) ){
                if( is_array( $data['type_work'] ) ){
                    //$sql .= " AND tl.id_type in (" . implode(',', $data['type_work'] ) . ") " ;
                    foreach ($data['type_work'] as $key => $value) {
                        $left .= " LEFT JOIN type_list_work tl".$key." ON ( t.id = tl".$key.".id_works AND tl".$key.".id_type = " . $this->db->escape( $value ) ." ) ";
                        $sql .= " AND tl".$key.".id IS NOT NULL " ;
                    }
                    
                }else{
                    $left = " LEFT JOIN type_list_work tl ON ( t.id = tl.id_works ) ";
                    $sql .= " AND tl.id_type = " . $this->db->escape( $data['type_work'] ) ;
                }
                
            }
            if( isset( $data['search'] ) ){
                $sql .= ' AND ( ';

                $or[] = ' t.client LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' t.number LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' t.profit LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' t.type_pay LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                
                $sql .=  implode(' OR ', $or);

                $sql .= ' ) ';
            }
            
            $query = $this->db->query(" SELECT COUNT( DISTINCT( t.id )  ) as total FROM `table_test` t ". $left ." " . $sql);
        }else{
            $query = $this->db->query(" SELECT COUNT( id ) as total FROM `table_test` ");
        }
        $table = array();
        if( $query->getRow() ){
            $total = $query->getRow();
            return $total->total;
        }else{
            return false;
        }
    }

    public function getItems( $data = array(), $limit = 20, $start = 0 ){
        if( $data ){
            $sql = ' WHERE 1=1 ';
            
            $left = '';
            if( isset( $data['type_work'] ) ){
                if( is_array( $data['type_work'] ) ){
                    //$sql .= " AND tl.id_type in (" . implode(',', $data['type_work'] ) . ") " ;
                    foreach ($data['type_work'] as $key => $value) {
                        $left .= " LEFT JOIN type_list_work tl".$key." ON ( t.id = tl".$key.".id_works AND tl".$key.".id_type = " . $this->db->escape( $value ) ." ) ";
                        $sql .= " AND tl".$key.".id IS NOT NULL " ;
                    }
                    
                }else{
                    $left = " LEFT JOIN type_list_work tl ON ( t.id = tl.id_works ) ";
                    $sql .= " AND tl.id_type = " . $this->db->escape( $data['type_work'] ) ;
                }
                
            }
            if( isset( $data['search'] ) && !empty( $data['search'] ) ){
                $sql .= ' AND ( ';

                $or[] = ' t.client LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' t.number LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' t.profit LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' t.type_pay LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                
                $sql .=  implode(' OR ', $or);

                $sql .= ' ) ';
            }
            $sql .= ' GROUP BY t.id ';
            if( isset( $data['sort']['column'] ) ){
                $sql .= ' ORDER BY t.' . $this->db->escape( $data['sort']['column'] );
            }else{
                $sql .= ' ORDER BY t.id ';
            }
            if( isset( $data['sort']['dir'] ) ){
                $sql .= ' ' . $this->db->escape( $data['sort']['dir'] );
            }else{
                $sql .= ' ASC ';
            }

            if( $start && $limit ){
                $sql .= ' LIMIT '. $this->db->escape( $start ) .','. $this->db->escape( $limit ) ;
            }else{
                $sql .= ' LIMIT 0,20 ' ;
            }
            
            $query = $this->db->query(" SELECT t.*, t.id as id FROM `table_test` t ". $left ." " . $sql);
        }else{
            $query = $this->db->query(" SELECT * FROM `table_test` LIMIT ". $start.','.$limit);
        }
        $table = array();
        if( $query->getRow() ){
            return $query->getResult('array');
        }else{
            return false;
        }
    }

    public function getItem( $id ){
        $query = $this->db->query(" SELECT * FROM `table_test` WHERE id = '". $id ."' ");
        $table = array();
        if( $query->getRow() ){
            return $query->getRow();
        }else{
            return false;
        }
    }

    public function getWorks(  ){
        $query = $this->db->query(" SELECT * FROM `type_works` ");
        $table = array();
        if( $query->getRow() ){
            return $query->getResult('array');
        }else{
            return false;
        }
    }

    public function getTypeWork( $id ){
        $query = $this->db->query(" SELECT * FROM `type_list_work` lw LEFT JOIN type_works w ON (lw.id_type = w.id) WHERE lw.id_works = '". $id ."' ");
        if( $query->getRow() ){
            return $query->getResult('array');
        }else{
            return false;
        }
    }

    public function saveContact( $data ){
        $builder = $this->db->table('table_test');
        $builder->insert($data);
        return $this->db->insertID();
    }

    public function updateContact( $id, $data ){

        $builder = $this->db->table('table_test');
        $builder->where('id', $id);
        $builder->update($data);
        $this->db->query("DELETE FROM `type_list_work` WHERE `id_works` ='".$id."' ");
    }

    public function deleteContact( $id ){

        $builder = $this->db->table('table_test');
        $builder->where('id', $id);
        $builder->delete();
    }

    public function saveTypeWork( $id, $name ){
        switch ( trim($name) ) {
            case 'Проектирование':
                $_type = 1;
                break;
            case 'Кадастрирование':
                $_type = 2;
                break;
            case 'Перепланировка':
                $_type = 3;
                break;
            
            default:
                # code...
                break;
        }
        $this->db->query("INSERT INTO `type_list_work` (`id_type`, `id_works`) VALUES ( ". $_type .", ".$id.")");
    }

    
    public function saveInTable( $data, $table ){
        $builder = $this->db->table($table);
        $builder->insert($data);
        return $this->db->insertID();
    }

    
    public function getItemsClientUser( $data = array(),  $limit = 20, $start = 0 ){
        if( $data ){
            $sql = ' WHERE deleted=0 ';
            
            
            if( isset( $data['search'] ) && !empty( $data['search'] ) ){
                $sql .= ' AND ( ';

                $or[] = ' client_id LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' address LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' name LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' contact LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' task LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' referrer LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' manager LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                
                
                $sql .=  implode(' OR ', $or);

                $sql .= ' ) ';
            }
            $sql .= ' GROUP BY client_id ';
            if( isset( $data['sort']['column'] ) ){
                $sql .= ' ORDER BY ' . $data['sort']['column'];
            }else{
                $sql .= ' ORDER BY client_id ';
            }
            if( isset( $data['sort']['dir'] ) ){
                $sql .= ' ' . $data['sort']['dir'];
            }else{
                $sql .= ' ASC ';
            }

            if( $start && $limit ){
                $sql .= ' LIMIT '. $start.','.$limit ;
            }else{
                $sql .= ' LIMIT 0,20 ' ;
            }
            
            $query = $this->db->query(" SELECT * FROM `clients`  " . $sql);
        }else{
            $query = $this->db->query(" SELECT * FROM `clients` LIMIT ". $start.','.$limit);
        }
        $table = array();
        if( $query->getRow() ){
            return $query->getResult('array');
        }else{
            return false;
        }
    }
    
    public function getItemsClientUserTotal( $data = array() ){
        if( $data ){
            $sql = ' WHERE deleted=0 ';
            
            if( isset( $data['search'] ) ){
                $sql .= ' AND ( ';

                $or[] = ' client_id LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' address LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' name LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' contact LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' task LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' referrer LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' manager LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                
                $sql .=  implode(' OR ', $or);

                $sql .= ' ) ';
            }
            
            $query = $this->db->query(" SELECT COUNT( DISTINCT( client_id )  ) as total FROM `clients`  " . $sql);
        }else{
            $query = $this->db->query(" SELECT COUNT( client_id ) as total FROM `clients` ");
        }
        $table = array();
        if( $query->getRow() ){
            $total = $query->getRow();
            return $total->total;
        }else{
            return false;
        }
    }
    
    public function getItemClients( $id ){
        $query = $this->db->query(" SELECT * FROM `clients` WHERE client_id = '". $id ."' ");
        $table = array();
        if( $query->getRow() ){
            return $query->getRow();
        }else{
            return false;
        }
    }
    public function saveContactClients( $data ){
        $builder = $this->db->table('clients');
        $builder->insert($data);
        return $this->db->insertID();
    }

    public function getPartners( $limit = 50, $start = 0 ){
        if( empty($start) ){
            $start = 0;
        }
        if( empty($limit) ){
            $limit = 50;
        }
        $query = $this->db->query(" SELECT * FROM `partners` WHERE deleted=0 LIMIT ". $start.','.$limit);
        $table = array();
        if( $query->getRow() ){
            return $query->getResult('array');
        }else{
            return false;
        }
    }
    
    public function getPartnersTotal( ){
        
        $query = $this->db->query(" SELECT COUNT( partners_id ) as total FROM `partners` WHERE deleted=0 ");
        $table = array();
        if( $query->getRow() ){
            $total = $query->getRow();
            return $total->total;
        }else{
            return false;
        }
    }

    public function getPartner($id){
        $query = $this->db->query(" SELECT * FROM `partners` WHERE partners_id = '". $id ."' ");
        $table = array();
        if( $query->getRow() ){
            return $query->getRow();
        }else{
            return false;
        }
    }
    
    public function getObjects($data = array(), $limit = 50, $start = 0 ){
        if( $data ){
            $sql = ' WHERE deleted=0 ';
            
            if( isset( $data['search'] ) ){
                $sql .= ' AND ( ';

                $or[] = ' object_id LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' contract_date LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' customer_name LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' object_district LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' object_address LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' object_type LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' work_type LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' target LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' object_area LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                
                $sql .=  implode(' OR ', $or);

                $sql .= ' ) ';
            }

            
            if( isset( $data['sort']['column'] ) ){
                $sql .= ' ORDER BY ' . $data['sort']['column'];
            }else{
                $sql .= ' ORDER BY client_id ';
            }
            if( isset( $data['sort']['dir'] ) ){
                $sql .= ' ' . $data['sort']['dir'];
            }else{
                $sql .= ' ASC ';
            }

            if( $start && $limit ){
                $sql .= ' LIMIT '. $start.','.$limit ;
            }else{
                $sql .= ' LIMIT 0,20 ' ;
            }
            
            $query = $this->db->query(" SELECT * FROM `objects`  " . $sql);
        }else{
            $query = $this->db->query(" SELECT * FROM `objects` ");
        }
        
        //$query = $this->db->query(" SELECT * FROM `objects` WHERE deleted=0 LIMIT ". $start.','.$limit);
        $table = array();
        if( $query->getRow() ){
            return $query->getResult('array');
        }else{
            return false;
        }
    }
    
    public function getObjectsTotal( $data = array() ){
        
        if( $data ){
            $sql = ' WHERE deleted=0 ';
            
            if( isset( $data['search'] ) ){
                $sql .= ' AND ( ';

                $or[] = ' object_id LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' contract_date LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' customer_name LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' object_district LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' object_address LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' object_type LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' work_type LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' target LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' object_area LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                
                $sql .=  implode(' OR ', $or);

                $sql .= ' ) ';
            }

            
            $query = $this->db->query(" SELECT COUNT( object_id ) as total FROM `objects`  " . $sql);
        }else{
            $query = $this->db->query(" SELECT COUNT( object_id ) as total FROM `objects` ");
        }
        $table = array();
        if( $query->getRow() ){
            $total = $query->getRow();
            return $total->total;
        }else{
            return false;
        }
    }

    public function getObject($id){
        $query = $this->db->query(" SELECT * FROM `objects` WHERE object_id = '". $id ."' ");
        $table = array();
        if( $query->getRow() ){
            return $query->getRow();
        }else{
            return false;
        }
    }

    
    public function getContracts($data = array(),  $limit = 50, $start = 0 ){
        
        if( $data ){
            $sql = ' WHERE deleted=0 ';
            
            if( isset( $data['search'] ) ){
                $sql .= ' AND ( ';

                $or[] = ' contract_id LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' contract_date LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' customer_id LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' contract_start LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' contract_end LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' payment LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                
                $sql .=  implode(' OR ', $or);

                $sql .= ' ) ';
            }

            
            if( isset( $data['sort']['column'] ) ){
                $sql .= ' ORDER BY ' . $data['sort']['column'];
            }else{
                $sql .= ' ORDER BY client_id ';
            }
            if( isset( $data['sort']['dir'] ) ){
                $sql .= ' ' . $data['sort']['dir'];
            }else{
                $sql .= ' ASC ';
            }

            if( $start && $limit ){
                $sql .= ' LIMIT '. $start.','.$limit ;
            }else{
                $sql .= ' LIMIT 0,20 ' ;
            }
            
            $query = $this->db->query(" SELECT * FROM `contracts`  " . $sql);
        }else{
            $query = $this->db->query(" SELECT * FROM `contracts` ");
        }

        //$query = $this->db->query(" SELECT * FROM `contracts` WHERE deleted=0 LIMIT ". $start.','.$limit);
        $table = array();
        if( $query->getRow() ){
            return $query->getResult('array');
        }else{
            return false;
        }
    }
    
    public function getContractsTotal( $data = array() ){
        
        if( $data ){
            $sql = ' WHERE deleted=0 ';
            
            if( isset( $data['search'] ) ){
                $sql .= ' AND ( ';

                $or[] = ' contract_id LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' contract_date LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' customer_id LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' contract_start LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' contract_end LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' payment LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                
                $sql .=  implode(' OR ', $or);

                $sql .= ' ) ';
            }

            
            $query = $this->db->query(" SELECT COUNT( contract_id ) as total FROM `contracts`  " . $sql);
        }else{
            $query = $this->db->query(" SELECT COUNT( contract_id ) as total FROM `contracts` ");
        }
        $table = array();
        if( $query->getRow() ){
            $total = $query->getRow();
            return $total->total;
        }else{
            return false;
        }
    }

    public function getContract($id){
        $query = $this->db->query(" SELECT * FROM `contracts` WHERE contract_id = '". $id ."' ");
        $table = array();
        if( $query->getRow() ){
            return $query->getRow();
        }else{
            return false;
        }
    }



    
    public function getItemsСustomer( $data = array(),  $limit = 20, $start = 0 ){
        if( $data ){
            $sql = ' WHERE deleted=0 ';
            
            
            if( isset( $data['search'] ) && !empty( $data['search'] ) ){
                $sql .= ' AND ( ';

                $or[] = ' customer_id LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' company_name LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' company_name LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' name LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' firstname LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' lastname LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' middlename LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' contact LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                
                $sql .=  implode(' OR ', $or);

                $sql .= ' ) ';
            }
            $sql .= ' GROUP BY customer_id ';
            if( isset( $data['sort']['column'] ) ){
                $sql .= ' ORDER BY ' . $data['sort']['column'];
            }else{
                $sql .= ' ORDER BY customer_id ';
            }
            if( isset( $data['sort']['dir'] ) ){
                $sql .= ' ' . $data['sort']['dir'];
            }else{
                $sql .= ' ASC ';
            }

            if( $start && $limit ){
                $sql .= ' LIMIT '. $start.','.$limit ;
            }else{
                $sql .= ' LIMIT 0,20 ' ;
            }
            
            $query = $this->db->query(" SELECT * FROM `customer`  " . $sql);
        }else{
            $query = $this->db->query(" SELECT * FROM `customer` LIMIT ". $start.','.$limit);
        }
        $table = array();
        if( $query->getRow() ){
            return $query->getResult('array');
        }else{
            return false;
        }
    }
    
    public function getItemsСustomerTotal( $data = array() ){
        if( $data ){
            $sql = ' WHERE deleted=0 ';
            
            if( isset( $data['search'] ) ){
                $sql .= ' AND ( ';

                $or[] = ' customer_id LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' company_name LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' company_name LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' name LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' firstname LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' lastname LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' middlename LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                $or[] = ' contact LIKE '. $this->db->escape( "%".$data['search']."%" ) .' ';
                
                $sql .=  implode(' OR ', $or);

                $sql .= ' ) ';
            }
            
            $query = $this->db->query(" SELECT COUNT( customer_id  ) as total FROM `customer`  " . $sql);
        }else{
            $query = $this->db->query(" SELECT COUNT( customer_id ) as total FROM `customer` ");
        }
        $table = array();
        if( $query->getRow() ){
            $total = $query->getRow();
            return $total->total;
        }else{
            return false;
        }
    }
    
    public function getСustomer( $id ){
        $query = $this->db->query(" SELECT * FROM `customer` WHERE customer_id = '". $id ."' ");
        $table = array();
        if( $query->getRow() ){
            return $query->getRow();
        }else{
            return false;
        }
    }

    public function selectUsers(){
        $query = $this->db->table('users')->select('user_id, name, lastname, middlename')->get();
        if( $query->getRow() ){
            return $query->getResult('array');
        }else{
            return false;
        }
        
    }
    
    public function selectUser($id){
        $query = $this->db->table('users')->select('user_id, name, lastname, middlename')->where('user_id', $id )->get();
        if( $query->getRow() ){
            return $query->getResult('array');
        }else{
            return false;
        }
        
    }

    
    public function selectApiPartners(){
        $query = $this->db->table('partners')->where('status', 1 )->select('partners_id, name')->get();
        if( $query->getRow() ){
            return $query->getResult('array');
        }else{
            return false;
        }
        
    }
    
    public function selectApiPartner($id){
        $query = $this->db->table('partners')->select('partners_id, name')->where('status', 1 )->where('partners_id', $id )->get();
        if( $query->getRow() ){
            return $query->getResult('array');
        }else{
            return false;
        }
        
    }
}