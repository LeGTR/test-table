<?php namespace App\Models;
 
use CodeIgniter\Model;
 
class PostModel extends Model{

    protected $table = 'news';
    

    /*
    public function saveContact( $data ){
        $builder = $this->db->table('table_test');
        $builder->insert($data);
        return $this->db->insertID();
    }

    public function updateContact( $id, $data ){

        $builder = $this->db->table('table_test');
        $builder->where('id', $id);
        $builder->update($data);
        $this->db->query("DELETE FROM `type_list_work` WHERE `id_works` ='".$id."' ");
    }

    public function deleteContact( $id ){

        $builder = $this->db->table('table_test');
        $builder->where('id', $id);
        $builder->delete();
    }
    */

    public function saveTable( $data, $table = '' ){
        if( empty($table) ){
            return false;
        }
        $builder = $this->db->table($table);
        $builder->insert($data);
        return $this->db->insertID();
    }

    public function updateTable($column, $id, $data, $table = '' ){
        if( empty($table) ){
            return false;
        }
        $builder = $this->db->table($table);
        $builder->where($column, $id);
        $builder->update($data);
    }

    public function deletedTable( $column, $id, $table = '' ){
        if( empty($table) ){
            return false;
        }
        $builder = $this->db->table($table);
        $builder->where( $column, $id );
        $builder->delete();
    }
    


    public function saveClient( $data ){
        $builder = $this->db->table('clients');
        $builder->insert($data);
        return $this->db->insertID();
    }

    public function updateClient( $id, $data ){

        $builder = $this->db->table('clients');
        $builder->where('id', $id);
        $builder->update($data);
    }

    public function deleteClient( $id ){

        $builder = $this->db->table('clients');
        $builder->where('id', $id);
        $builder->delete();
    }

    public function getCustomer(){
        return $this->db->table('customer')->get()->getResult('array');
    }
    public function getObjects(){
        return $this->db->table('objects')->get()->getResult('array');
    }
}