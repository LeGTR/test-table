<?=$header;?>
<?=$left_menu;?>
        <div id="page-wrapper" class="gray-bg">
            <?=$head;?>
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h3>Проектирование <small class="m-l-sm"> отображены все проекты по этой услуге</small></h3>
                                <div class="ibox-tools">
                                    <a  href="/project/added" class="btn btn-primary btn-sm" type="button"><i class="fa fa-plus"></i>&nbsp;Новый проект&nbsp;<i class="fa fa-edit"></i></a>
                                    <a class="fullscreen-link">
                                        <i class="fa fa-expand"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <table class="table table-striped table-hover border-bottom mt-3 display" id="dTable" style="width:100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="float-right">
                    10Гб из <strong>250Гб</strong> свободно.
                </div>
                <div>
                    <strong>Все права защищены</strong> ООО &laquo;Варди&raquo; &copy; 2020-2021
                </div>
            </div>
        </div>

        
    <div id="extendFilter" class="d-none">
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
          <label class="btn btn-primary btn-outline active" data-status="all">
            <input type="radio" name="options" id="option1" autocomplete="off" checked=""> Все
          </label>
          <label class="btn btn-primary btn-outline" data-status="1">
            <input type="radio" name="options" id="option2" autocomplete="off"> Просроченные
          </label>
          <label class="btn btn-primary btn-outline" data-status="2">
            <input type="radio" name="options" id="option3" autocomplete="off"> В работе
          </label>
          <label class="btn btn-primary btn-outline" data-status="0">
            <input type="radio" name="options" id="option4" autocomplete="off"> Архив
          </label>
        </div>
    </div>
<?=$footer_scripts;?>

<script>

var dTable = null;
var s_actionButtons = null;
var works_types = { primary: 'Проектирование', warning: 'Кадастрирование', danger: 'Перепланировка' };
var works_types_k = Object.keys(works_types);
var works = '';

$(document).ready(function() {
    //s_actionButtons = $('#buttonHolder').html();
    
    $(document).on('preInit.dt', function(e, settings) {
        
        $('.extendFilter').html($('#extendFilter').html());
    });

    $(document).on('draw.dt', function(e, settings) {
        $('table#dTable span.pie').peity("pie");
    });

    dTable = $('table#dTable').DataTable({
        language: { url: '/js/plugins/dataTables/Russian.json' },
        pageLength: 50,
        responsive: true,
        paging: true,
        searching: true,
        serverSide: false, // ТОЛЬКО ДЛЯ ТЕСТА, В ПРОДАКШ ОБРАБОТКА ДОЛЖНА БЫТЬ СЕРВЕРНАЯ
        ajax: {
            url: '<?=$ajax;?>',
            type: 'POST',
            dataSrc: ''
        },
        columnDefs: [
            { className: "align-middle", targets: "_all" },

        ],
        columns: [
            { name: 'id', data: 0, title: '№' },
            {
                name: 'object',
                data: 1,
                width: '40%',
                //className: "issue-info",
                title: 'Объект',
                render: function(data, type, row, meta) {
                    return type === 'display' ? '<a href="objects.html?id='+data[0]+'">'+data[2]+'</a><small class="d-block">'+data[1]+'</small>': data[2];
                }
            },
            {
                name: 'manager',
                data: 2,
                title: 'Ответственный',
                render: function(data, type, row, meta) {
                    //console.log(data, type, row, meta);
                    return type === 'display' ? '<a href="contacts.html?id=' + data[0] + '">' + data[1] + '</a>': data[1];
                }
            },
            {
                name: 'date_end',
                data: 3,
                title: 'Завершение',
                //width: '70%',
                render: function(data, type, row, meta) {
                    //console.log(data, type, row, meta);
                    return type === 'display' ? getDateFromUNIX(data) : data;
                }
            },
            {
                name: 'client',
                data: 4,
                title: 'Заказчик',
                render: function(data, type, row, meta) {
                    //console.log(data, type, row, meta);
                    return type === 'display' ? '<a href="clients.html?id=' + data[0][0] + '">' + data[0][4] + '</a>': data[0][3];
                }
            },
            {
                name: 'finalize',
                data: 5,
                searchable: false,
                title: '<i class="fa fa-calendar">&nbsp;</i>',
                render: function(data, type, row, meta) {
                    //console.log(data, type, row, meta);
                    if (type === 'display') {

                        return '<span class="pie" data-peity=\'{ "fill": ["rgb('+data[1].join(',')+')", "#d7d7d7"]}\'>'+data[0]+'/1</span>';
                    }
                    return data[0];
                }
            },
            {
                name: 'works',
                data: 6,
                title: 'Работы',
                render: function(data, type, row, meta) {
                    out = '';
                    if(type === 'display'){
                        data.forEach(function(c,i,a){
                            out = out + ' <span class="badge">'+works[c]+'</span>'
                        });
                    }else{
                        data.forEach(function(c,i,a){
                            out = out + ' '+works[c];
                        });
                    }
                    return out;
                }
            },
            {name: 'status', data: 7, visible: false}
        ],
        order: [
            [1, 'desc']
        ],
        dom: "<'row'<'col-md-4'f><'col-md-4 extendFilter'><'col-md-4 text-right'i>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row mt-2'<'col-sm-12 col-md-6'p><'col-sm-12 col-md-6 text-right pull-right'l>>",
    });

    $(document).on('click', '.extendFilter label',function() {
        if($(this).data('status') == 'all'){
            dTable.search($('.dataTables_filter input').val()).columns( 7 ).search('').draw();
        }else{
            dTable
                .columns( 7 )
                .search( $(this).data('status') )
                .draw();
        }
    });            
    
});
</script>
<?=$footer;?>