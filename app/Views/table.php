<?=$header;?>
<?=$left_menu;?>
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                        <form role="search" class="navbar-form-custom" action="search_results.html">
                            <div class="form-group">
                                <input type="text" placeholder="Поиск по всей CRM..." class="form-control" name="top-search" id="top-search">
                            </div>
                        </form>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <span class="m-r-sm text-muted welcome-message">Вт, 28 ноября 2020 11:13</span>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-bell"></i> <span class="label label-primary">8</span>
                            </a>
                            <ul class="dropdown-menu dropdown-alerts">
                                <li>
                                    <a href="activity_stream.html" class="dropdown-item">
                                        <div>
                                            <i class="fa fa-envelope fa-fw"></i> Новых 16 сообщений
                                            <span class="float-right text-muted small">4 минуты назад</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="dropdown-divider"></li>
                                <li>
                                    <a href="activity_stream.html" class="dropdown-item">
                                        <div>
                                            <i class="fa fa-twitter fa-fw"></i> 3 Просроченных задачи
                                            <span class="float-right text-muted small">12 дней назад</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="dropdown-divider"></li>
                                <li>
                                    <a href="activity_stream.html" class="dropdown-item">
                                        <div>
                                            <i class="fa fa-upload fa-fw"></i> 5 проектов завершены
                                            <span class="float-right text-muted small">4 недели назад</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="dropdown-divider"></li>
                                <li>
                                    <div class="text-center link-block">
                                        <a href="activity_stream.html" class="dropdown-item">
                                            <strong>Все уведомления</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="login.html">
                                <i class="fa fa-sign-out"></i> Выход
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="wrapper wrapper-content  animated fadeInRight">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h4>Контакты <small class="m-l-sm"> могут стать заказчиками если заключить с ними договор</small></h4>
                                <div class="ibox-tools">
                                    <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#addContact"><i class="fa fa-plus"></i>&nbsp;Добавить контакт</button>
                                    <a class="fullscreen-link">
                                        <i class="fa fa-expand"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <table class="table table-striped table-hover border-bottom mt-3 display" data-page-size="15" id="contactsTable">
                                
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="addContact" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                        <h4 class="modal-title">Новый контакт</h4>
                        <small>Контакт - это свободная запись контактных данных с человеком, потенциальным Заказчиком</small>
                    </div>
                    <div class="modal-body">
                        <form method="POST" id="addClientForm" class="form_addcontacts">
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Клиент</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control required" name="client">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Номер</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control typeahead required" name="number" placeholder=""> 
                                    <span class="form-text m-b-none small "></span>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Выручка	В работе</label>
                                <div class="col-sm-5">
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon">руб.</span>
                                        </div>
                                        <input type="text" placeholder="" name="profit" class="form-control required">
                                        <span class="form-text m-b-none small">Вы должны распределить всю сумму по этапам</span>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="input-group date">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" class="form-control required" name="date" placeholder="Дата договора">
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Вид работ</label>
                                <div class="col-sm-10">
                                <? foreach ($works as $key => $value) {?>
                                    <label> 
                                        <input type="checkbox" class="check" name="type_work[]" value="<?=$value['name_work'];?>" id="inlineCheckbox<?=$value['id'];?> "> <?=$value['name_work'];?> 
                                    </label> 
                                    <br>
                                <?} ?>
                                </div>
                                <?/*
                                <div class="col-sm-10">
                                    <input type="text" class="form-control typeahead" name="type_work" placeholder=""> 
                                    <span class="form-text m-b-none small"></span>
                                </div>
                                */?>
                            </div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Тип оплаты</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control typeahead required" name="type_pay" placeholder=""> 
                                    <span class="form-text m-b-none small"></span>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer" style="display:block !important">
                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                            </div>
                            <div class="col">
                                <button type="button" id="saveContact" class="btn btn-primary pull-right" data-style="zoom-in">Сохранить</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal inmodal" id="editContact" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                        <h4 class="modal-title">Редактировать запись</h4>
                        <small>Контакт - это свободная запись контактных данных с человеком, потенциальным Заказчиком</small>
                    </div>
                    <div class="modal-body">
                        <form method="POST" id="editClientForm" class="form_addcontacts">
                        
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">ID</label>
                                <div class="col-sm-10">
                                    <input type="text" readonly class="form-control" name="id">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Клиент</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control required" name="client">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Номер</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control typeahead required" name="number" placeholder=""> 
                                    <span class="form-text m-b-none small "></span>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Выручка	В работе</label>
                                <div class="col-sm-5">
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon">руб.</span>
                                        </div>
                                        <input type="text" placeholder="" name="profit" class="form-control required">
                                        <span class="form-text m-b-none small">Вы должны распределить всю сумму по этапам</span>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="input-group date">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" class="form-control required" name="date" placeholder="Дата договора">
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Вид работ</label>
                                <div class="col-sm-10">
                                <? foreach ($works as $key => $value) {?>
                                    <label> 
                                        <input type="checkbox" class="check" name="type_work[]" value="<?=$value['name_work'];?>" id="inlineCheckbox<?=$value['id'];?> "> <?=$value['name_work'];?> 
                                    </label> 
                                    <br>
                                <?} ?>
                                </div>
                                <?/*
                                <div class="col-sm-10">
                                    <input type="text" class="form-control typeahead" name="type_work" placeholder=""> 
                                    <span class="form-text m-b-none small"></span>
                                </div>
                                */?>
                            </div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Тип оплаты</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control typeahead required" name="type_pay" placeholder=""> 
                                    <span class="form-text m-b-none small"></span>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer" style="display:block !important">
                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                            </div>
                            <div class="col">
                                <button type="button" id="updateContact" class="btn btn-primary pull-right" data-style="zoom-in">Обновить</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
        <div class="modal inmodal" id="deleteContact" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                        <h4 class="modal-title">Удаление записи</h4>
                        <small>Вы уверены что хотите удалить запись?</small>
                    </div>
                    <div class="modal-body">
                        
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">ID</label>
                            <div class="col-sm-10">
                                <input type="text" readonly class="form-control" name="id">
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>

                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Клиент</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control required" name="client">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer" style="display:block !important">
                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                            </div>
                            <div class="col">
                                <button type="button" id="deleteContactBtn" class="ladda-button btn btn-danger pull-right" data-style="zoom-in">Удалить</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal inmodal" id="addClient" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                        <h4 class="modal-title">Новый Заказчик</h4>
                        <small>Заказчики - это сущности в CRM, с которыми можно заключить договор</small>
                    </div>
                    <div class="modal-body">
                        <form method="get">
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Имя</label>
                                <div class="col-sm-10"><input type="text" class="form-control"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">От кого</label>
                                <div class="col-sm-10"><input type="text" class="form-control typeahead" placeholder="Начните вводить Имя сотрудника"> <span class="form-text m-b-none small">Оставьте пустым, если не знаете от кого пришел клиент</span>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Адрес</label>
                                <div class="col-sm-10"><input type="text" class="form-control" placeholder="Используется autocomplete по API Яндекс.Карт">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Запрос</label>
                                <div class="col-sm-10"><textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Контакт</label>
                                <div class="col-sm-10">
                                    <div class="inputHolder">
                                        <div class="input-group m-b">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Телефон" value="(916)345-6789">
                                        </div>
                                        <div class="input-group m-b">
                                            <div class="input-group-prepend"><span class="input-group-addon"><i class="fa fa-skype"></i></span></div>
                                            <input type="text" class="form-control" data-mask="(999) 999-9999" placeholder="Телефон" value="skype_login">
                                            <a class="input-group-addon text-danger deleteContact">
                                                <span class="fa fa-times"></span>
                                            </a>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary btn-sm contactAdd" data-inputMask="(999) 999-9999" data-placeholder="Телефон" type="button">+&nbsp;<i class="fa fa-phone"></i></button>
                                    <button class="btn btn-warning btn-sm contactAdd" data-placeholder="Почта" type="button">+&nbsp;<i class="fa fa-envelope"></i></button>
                                    <button class="btn btn-success btn-sm contactAdd" data-placeholder="Skype" type="button">+&nbsp;<i class="fa fa-skype"></i></button>
                                    <button class="btn btn-info btn-sm contactAdd" data-placeholder="Мессенджер" type="button">+&nbsp;<i class="fa fa-telegram"></i></button>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Ответственный</label>
                                <div class="col-sm-10">
                                    <select class="form-control m-b" name="account">
                                        <option>Сотрудник 1</option>
                                        <option>Сотрудник 2</option>
                                        <option>Сотрудник 3</option>
                                        <option>Сотрудник 4</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer" style="display:block !important">
                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                            </div>
                            <div class="col">
                                <button type="button" class="ladda-button btn btn-primary pull-right" data-style="zoom-in">Сохранить</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="yearHolder" class="d-none">
            <div data-toggle="buttons-checkbox" class="btn-group">
                <button class="btn btn-primary type_work_btn" var_type="1" type="button" aria-pressed="false">Проектирование</button>
                <button class="btn btn-primary type_work_btn" var_type="2" type="button" aria-pressed="false">Кадастрирование</button>
                <button class="btn btn-primary type_work_btn" var_type="3" type="button" aria-pressed="false">Перепланировка</button>
            </div>
        </div>
<?=$footer_scripts;?>

<script>
        var stuff = null;
        var partners = null;
        var fio_random = ['Сергей', 'Максим', 'Дмитрий', 'Вячеслав ГЮ', 'Руслан', 'М.Женя', 'Эльдар замер', 'София Перелыгина', 'РОман', 'Энди', 'Виктор', 'Руслан', 'Елена Мирославовна', 'Ольга', 'Армен', 'Елена Викторовна от К', 'Андрей', 'Рафаэль от К', 'Юрий от К', 'Алла', 'Ольга Исаева', 'Лакоба', 'Лакоба', 'Илья', 'Оксана'];
        var contact_random = ['Сергей', 'Максим', 'Дмитрий', 'Вячеслав ГЮ', 'Руслан', 'М.Женя', 'Эльдар замер', 'София Перелыгина', 'РОман', 'Энди', 'Виктор', 'Руслан', 'Елена Мирославовна', 'Ольга', 'Армен', 'Елена Викторовна от К', 'Андрей', 'Рафаэль от К', 'Юрий от К', 'Алла', 'Ольга Исаева', 'Лакоба', 'Лакоба', 'Илья', 'Оксана'];
        var s_actionButtons = $('#buttonHolder').html();
        var type = '';
        var type_1 = '';
        var type_2 = '';
        var type_3 = '';
        $(document).ready(function() {
            $(document).on('preInit.dt', function(e, settings) {
                $('.yearsSelector').html($('#yearHolder').html());
                console.log('Init');
            });
            //$.fn.modal.Constructor.prototype.enforceFocus = function() {};
            //$.fn.modal.Constructor.prototype._enforceFocus = function() {};
            $.fn.DataTable.ext.classes.sFilterInput = "form-control form-control-lg";

            //var dataJson = { [csrfName]: csrfHash };
            contactsTable = $('table#contactsTable').DataTable({
                language: { url: '/js/plugins/dataTables/Russian.json' },
                pageLength: 10,
                processing: true,
                responsive: true,
                paging: true,
                searching: true,
                serverSide: true, // пока у нас не много партнеров, нет смысла перегружать сервер пагинацией, сортировкой и фильтрацией
                ajax: {
                    url: '<?=$ajax;?>',
                    type: 'POST',
                    dataSrc: 'aaData',
                    //data:dataJson
                },
                columnDefs: [
                    { className: "align-middle", targets: "_all" },
                ],
                columns: [
                    { "name": "id", data:0,  title: "№" },
                    { "name": "client", data:1,  title: "Клиент" },
                    { "name": "number", data:2,  title: "Номер" },
                    { "name": "date", data:3,  title: "Дата" },
                    { "name": "profit", data:4,  title: "Выручка" },
                    { "name": "work_status", data:5,  title: "В работе?" },
                    { "name": "type_work", data:6,  title: "Вид работ", "orderable": false },
                    { "name": "type_pay", data:7,  title: "Тип оплаты" },
                    { "name": "date_added", data:8,  title: "Добавлен" },
                    { "name": "status", data:9,  title: "Статус" },
                    { "name": "action", data:10,  title: "Действие", "orderable": false }
                ],
                order: [
                    [0, 'asc']
                ],
                dom: "<'row'<'col-sm-12 col-md-3'f><'col-sm-12 col-md-6 yearsSelector'><'col-sm-12 col-md-3 text-right'i>>" + 
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row mt-2'<'col-sm-12 col-md-6'p><'col-sm-12 col-md-6 text-right pull-right'l>>",

            });
            
            $('body').on('click','.type_work_btn', function () {
                setTimeout( function () {
                    var type = $('.yearsSelector').find('.active');
                    var arr = [];
                    for (let index = 0; index < type.length; index++) {
                        const element = type[index];
                        arr[index] = $(element).attr('var_type');
                    }
                    contactsTable
                        .columns( 6 )
                        .search( String(arr) )
                        .draw();
                }, 100 )
            })

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.deleteContact').click(function() { $(this).parent().detach() });

            var l = $('.ladda-button').ladda();


            l.click(function() {
                // Start loading
                l.ladda('start');

                // Do something in backend and then stop ladda
                // setTimeout() is only for demo purpose
                setTimeout(function() {
                    l.ladda('stop');
                    $('#addContact').modal('hide');
                    swal({
                        title: "Успешно!",
                        text: "Новый клиент успешно сохранен",
                        type: "success"
                    });

                }, 500);

            });

        });
        /*
        function deleteContact(el) {
            $(el).parent().detach();
        }
        */
       
        $('body').on('click','.popup_edit', function(e){
            e.preventDefault();
            id = $(this).attr('var_id');
            if( id ){
                $.ajax({
                    url: '/home/selectContactForm/'+id,
                    type: 'post',
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    complete: function() {
                    },
                    success: function(json) {
                        console.log( json );
                        $('#editClientForm input[type="checkbox"]').prop("checked", false);
                        Object.keys(json).forEach(function(key) {
                            let val = json[key];
                            if( $('#editClientForm input[name="'+ key +'"]') ){
                                $('#editClientForm input[name="'+ key +'"]').val(val);
                            }
                            if ( key == 'type_work_array' ) {
                                //$('#editClientForm input[name="type_work[]"]');
                                for (let index = 0; index < val.length; index++) {
                                    console.log( val[index] );
                                    $('#editClientForm input[value="'+val[index]['name_work']+'"]').prop("checked", true);
                                }
                                //type_work[]
                                //'name_work'
                            }
                        })
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            }
            
        });
        
        $('body').on('click','.popup_delete', function(e){
            e.preventDefault();
            id = $(this).attr('var_id');
            if( id ){
                $.ajax({
                    url: '/home/selectContactForm/'+id,
                    type: 'post',
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    complete: function() {
                    },
                    success: function(json) {
                        
                        Object.keys(json).forEach(function(key) {
                            let val = json[key];
                            if( $('#deleteContact input[name="'+ key +'"]') ){
                                $('#deleteContact input[name="'+ key +'"]').val(val);
                            }
                            
                        })
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            }
            href = 'urik';
            console.log($(this).hasClass('btn-success'), $(this).prop("tagName"));
            if ($(this).hasClass('btn-success') || $(this).text().includes('Физическое')) {
                href = "fizik";
            }
            //window.location.href = 'project_add.html?type=' + href;

        });
        </script>
        
        <script>
        $(document).ready(function() {
            $('.calendar-palnning').on('click','button',function() {
                if($(this).hasClass('btn-info')) {
                    thisStepObj = $(this).parent().parent().parent().parent();
                    newStep = thisStepObj.clone();
                    newStep.find()
                    newStep.find('input').val('');
                    newStep.insertAfter(thisStepObj);
                }
                else if($(this).hasClass('btn-danger'))
                {
                     thisStepObj = $(this).parent().parent().parent().parent();
                     thisStepObj.remove();
                }

                $('.calendar-palnning-step').each(function(index){
                    $(this).removeClass('first');
                    $(this).removeClass('last');
                    if(index == 0){
                        $(this).find('button.btn-danger').prop('disabled',true);
                        $(this).addClass('first');
                    }
                    $(this).attr('id','step_'+(index+1));
                    $(this).find('label').html('Этап №'+(index+1));
                    $('#step_'+(index+1)).datepicker({language: "ru", inputs: $('.date-range input')});
                    
                });
            });
            //$.fn.datepicker.defaults.format = "dd.mm.yyyy";

            $('.date input').datepicker({language: "ru",});
            $('.calendar-palnning-step').each(function(){
                $(this).datepicker({
                    language: "ru",
                    inputs: $('.date-range input')
                });
            });
            //var action = getUrlParameter('action');
            //if ('addNew' == action) $('#addContract').modal('show');

            //$('.footable').footable();

        });

        $('#saveContact').click(function () { 
            $('.error').removeClass('error');

            var input = $('#addClientForm').find('input');
            var error = false;
            for (let index = 0; index < input.length; index++) {
                const element = input[index];
                console.log( element );

                if( $(element).hasClass('required') && !$(element).val() ){
                    $(element).addClass('error');
                    error = true;
                }
            }
            if ( error ) {
                return false;
            }
            var l = $(this).ladda();
            l.ladda('start');

                var data = $('#addClientForm').find('input').serialize();
                $.ajax({
                    url: '/home/saveContact',
                    type: 'post',
                    data: data,
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    complete: function() {
                        l.ladda('stop');
                        $('#addContact').modal('hide');
                        swal({
                            title: "Успешно!",
                            text: "Новый клиент успешно сохранен",
                            type: "success"
                        });
                    },
                    success: function(json) {
                        contactsTable.ajax.reload(null, false);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            
            /*
                // Start loading
                l.ladda('start');

                // Do something in backend and then stop ladda
                // setTimeout() is only for demo purpose
                setTimeout(function() {
                    l.ladda('stop');
                    $('#addContact').modal('hide');
                    swal({
                        title: "Успешно!",
                        text: "Новый клиент успешно сохранен",
                        type: "success"
                    });

                }, 500);
            */
         })

         
        $('#updateContact').click(function () { 
            $('.error').removeClass('error');

            var input = $('#editClientForm').find('input');
            var error = false;
            for (let index = 0; index < input.length; index++) {
                const element = input[index];

                if( $(element).hasClass('required') && !$(element).val() ){
                    $(element).addClass('error');
                    error = true;
                }
            }

            if (error) {
                return false;
            }
            var l = $(this).ladda();
            l.ladda('start');

            var data = $('#editClientForm').find('input').serialize();
            $.ajax({
                url: '/home/updateContact',
                type: 'post',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                },
                complete: function() {
                    contactsTable.ajax.reload(null, false);
                    l.ladda('stop');
                    $('#editContact').modal('hide');
                    swal({
                        title: "Успешно!",
                        text: "Клиент обновлен",
                        type: "success"
                    });
                },
                success: function(json) {
                },
                error: function(xhr, ajaxOptions, thrownError) {

                }
            });
            
         })

         $('#deleteContactBtn').click(function () { 
            var l = $('.ladda-button').ladda();
            l.ladda('start');

            var id = $('#deleteContact').find('input[name="id"]').val();
                
                $.ajax({
                    url: '/home/deleteContact/'+id,
                    type: 'post',
                    data: [],
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    complete: function() {
                        l.ladda('stop');
                        $('#deleteContact').modal('hide');
                        swal({
                            title: "Успешно!",
                            text: "Запись удалена",
                            type: "success"
                        });
                    },
                    success: function(json) {
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            
         })

         

         /*
        $('body').on('click','.popup_edit', function (e) {
            e.preventDefault();
            $('#addContract').modal('show');
        })
        */

        $('#search_btn').click(function (e) {
            var url = '<?=$_url;?>';

            var value = $('#search').val();

            if (value) {
                url += '<?=$s;?>search=' + encodeURIComponent(value);
                location = url;
            }else{
                alert( 'Введите значение' );
            }

        })
        $("#search").on("keydown", function(t) {
            if( 13 == t.keyCode ){
                t.preventDefault();
                $("#search_btn").trigger("click")
            }
        })
        </script>
<?=$footer;?>