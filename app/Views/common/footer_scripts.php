
        <!-- Mainly scripts -->
        <script src="/js/jquery-3.1.1.min.js"></script>
        <script src="/js/popper.min.js"></script>
        <script src="/js/moment.min.js"></script>
        <script src="/js/bootstrap.js"></script>
        <script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
        <!-- Flot -->
        <script src="/js/plugins/flot/jquery.flot.js"></script>
        <script src="/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
        <script src="/js/plugins/flot/jquery.flot.spline.js"></script>
        <script src="/js/plugins/flot/jquery.flot.resize.js"></script>
        <script src="/js/plugins/flot/jquery.flot.pie.js"></script>
        <script src="/js/plugins/flot/jquery.flot.symbol.js"></script>
        <script src="/js/plugins/flot/jquery.flot.time.js"></script>
        <!-- Custom and plugin javascript -->
        <script src="/js/inspinia.js"></script>
        <script src="/js/plugins/dataTables/datatables.min.js"></script>
        <script src="/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
        <script src="/js/plugins/pace/pace.min.js"></script>
        <!-- Ladda -->
        <script src="/js/plugins/ladda/spin.min.js"></script>
        <script src="/js/plugins/ladda/ladda.min.js"></script>
        <script src="/js/plugins/ladda/ladda.jquery.min.js"></script>
        <!-- iCheck -->
        <script src="/js/plugins/iCheck/icheck.min.js"></script>
        <!-- Typehead -->
        <script src="/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
        <!-- Input Mask-->
        <script src="/js/plugins/jasny/jasny-bootstrap.min.js"></script>
        <!-- Sweet alert -->
        <script src="/js/plugins/sweetalert/sweetalert.min.js"></script>
        <!-- FooTable -->
        <script src="/js/plugins/footable/footable.min.js"></script>
        <script src="/js/plugins/datapicker/bootstrap-datepicker.js"></script>
        <script src="/js/plugins/datapicker/bootstrap-datepicker.ru.min.js"></script>
        
        <script type="text/javascript" src="//cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.bootstrap4.min.js"></script>

        <script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
        <!-- jQuery UI -->
        <script src="/js/plugins/jquery-ui/jquery-ui.min.js"></script>
        <!-- Jvectormap -->
        <script src="/js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
        <script src="/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- EayPIE -->
        <script src="/js/plugins/easypiechart/jquery.easypiechart.js"></script>
        <!-- Sparkline -->
        <script src="/js/plugins/sparkline/jquery.sparkline.min.js"></script>
        <!-- Sparkline demo data  -->
        <script src="/js/demo/sparkline-demo.js"></script>
        <!-- Peity -->
        <script src="/js/plugins/peity/jquery.peity.min.js"></script>