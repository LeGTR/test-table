
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <img alt="image" class="rounded-circle" src="/img/profile_small.jpg" />
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="block m-t-xs font-bold">Николай Муравлёв</span>
                                <span class="text-muted text-xs block">Главный тут <b class="caret"></b></span>
                            </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a class="dropdown-item" href="profile.html">Профиль</a></li>
                                <li><a class="dropdown-item" href="login.html">Выйти</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            CRM
                        </div>
                    </li>
                    <? foreach ($url as $key => $value) {?>
                        <li <? if( $value['active'] !== false ) echo 'class="active"'; ?>>
                        
                            <? if( isset( $value['child'] ) ){
                                $value['url'] = '#';
                            } ?>
                            <a href="<?=$value['url'];?>"><?=$value['ico'];?> 
                                <span class="nav-label"><?=$value['name'];?></span>
                                <? if( isset($value['more']) ){?>
                                    <span class="label label-warning float-right"><?=$value['more'];?></span>
                                <?} ?>
                            </a>
                            <? if( isset( $value['child'] ) ){?>
                                <ul class="nav nav-second-level collapse">
                                    <? foreach ($value['child'] as $key2 => $value2) {?>
                                        <li <? if( $value2['active'] !== false ) echo 'class="active"'; ?>><a href="<?=$value2['url'];?>"><?=$value2['ico'];?> <?=$value2['name'];?></a></li>
                                    <?} ?>
                                </ul>
                            <?} ?>
                        </li>
                    <?} ?>
                </ul>
            </div>
        </nav>