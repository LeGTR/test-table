<?=$header;?>
<?=$left_menu;?>
        <div id="page-wrapper" class="gray-bg">
            <?=$head;?>
            <div class="wrapper wrapper-content  animated fadeInRight">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h4>Контакты <small class="m-l-sm"> могут стать заказчиками если заключить с ними договор</small></h4>
                                <div class="ibox-tools">
                                    <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#addContact"><i class="fa fa-plus"></i>&nbsp;Добавить контакт</button>
                                    <a class="fullscreen-link">
                                        <i class="fa fa-expand"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <table class="table table-striped table-hover border-bottom mt-3 display" data-page-size="15" id="contactsTable">
                                
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    <div class="modal inmodal" id="addContact" role="dialog" aria-modal="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                    <h4 class="modal-title">Новый контакт</h4>
                    <small>Контакт - это свободная запись контактных данных с человеком, потенциальным Заказчиком</small>
                </div>
                <div class="modal-body">
                    <form method="get" id="addClientForm">
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Имя (ФИО)</label>
                            <div class="col-sm-10"><input type="text" name="name" class="form-control"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">От кого</label>
                            <div class="col-sm-10"> <select class="form-control col-sm-10 m-b" name="referrer" id="partner">
                                    <option value="0" selected="selected">Выберите партнера, от кого пришел контакт</option>
                                </select> <span class="form-text m-b-none small">Оставьте пустым, если не знаете от кого пришел клиент</span>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Адрес</label>
                            <div class="col-sm-10"><input type="text" name="address" class="form-control" placeholder="Используется autocomplete по API Яндекс.Карт">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Запрос</label>
                            <div class="col-sm-10"><input type="text" name="task" class="form-control" id="exampleFormControlTextarea1" /></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row" id="social_form">
                            <label class="col-sm-2 col-form-label">Контакт</label>
                            <div class="col-sm-10">
                                <div class="inputHolder">
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                        </div>
                                        <input type="text" class="form-control" name="contact" data-mask="(999) 999-9999" placeholder="Телефон" value="(916)345-6789">
                                    </div>
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend"><span class="input-group-addon"><i class="fa fa-skype"></i></span></div>
                                        <input type="text" class="form-control" name="social[][skype]" placeholder="Телефон" value="skype_login">
                                        <a class="input-group-addon text-danger deleteContact">
                                            <span class="fa fa-times"></span>
                                        </a>
                                    </div>
                                </div>
                                <button class="btn btn-primary btn-sm contactAdd" data-inputMask="(999) 999-9999" data-placeholder="Телефон" type="button">+&nbsp;<i class="fa fa-phone"></i></button>
                                <button class="btn btn-warning btn-sm contactAdd" data-placeholder="Почта" type="button">+&nbsp;<i class="fa fa-envelope"></i></button>
                                <button class="btn btn-success btn-sm contactAdd" data-placeholder="Skype" type="button">+&nbsp;<i class="fa fa-skype"></i></button>
                                <button class="btn btn-info btn-sm contactAdd" data-placeholder="Мессенджер" type="button">+&nbsp;<i class="fa fa-telegram"></i></button>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Ответственный</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-10 m-b" name="manager" id="account_manager">
                                    <option value="0" selected="selected">Выберите ответственного сорудника</option>
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Комментарий</label>
                            <div class="col-sm-10"><textarea rows="3" class="form-control" name="comment" placeholder="Резюме ответственного  сотрудника по теккущему запросу, дата первого изменения этого поля будет отдельно сохранена в БД"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer" style="display:block !important">
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-primary pull-right" data-target="#addClientF" id="saveContactСlientF" data-style="zoom-in">Сохранить (Физ лицо)</button>
                            <button type="button" class="btn btn-primary pull-right" data-target="#addClientU" id="saveContactСlientU" data-style="zoom-in">Сохранить (Юр лицо)</button>
                            <button type="button" class="btn btn-primary pull-right" id="saveContact" data-style="zoom-in">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
        
    <div class="modal inmodal" id="editContact" role="dialog" aria-modal="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                    <h4 class="modal-title">Новый контакт</h4>
                    <small>Контакт - это свободная запись контактных данных с человеком, потенциальным Заказчиком</small>
                </div>
                <div class="modal-body">
                    <form method="get">
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Имя (ФИО)</label>
                            <div class="col-sm-10"><input type="text" class="form-control"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">От кого</label>
                            <div class="col-sm-10"> <select class="form-control col-sm-10 m-b" name="account" id="partner">
                                    <option value="0" selected="selected">Выберите партнера, от кого пришел контакт</option>
                                </select> <span class="form-text m-b-none small">Оставьте пустым, если не знаете от кого пришел клиент</span>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Адрес</label>
                            <div class="col-sm-10"><input type="text" class="form-control" placeholder="Используется autocomplete по API Яндекс.Карт">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Запрос</label>
                            <div class="col-sm-10"><input type="text" class="form-control" id="exampleFormControlTextarea1" /></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Контакт</label>
                            <div class="col-sm-10">
                                <div class="inputHolder">
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Телефон" value="(916)345-6789">
                                    </div>
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend"><span class="input-group-addon"><i class="fa fa-skype"></i></span></div>
                                        <input type="text" class="form-control" data-mask="(999) 999-9999" placeholder="Телефон" value="skype_login">
                                        <a class="input-group-addon text-danger deleteContact">
                                            <span class="fa fa-times"></span>
                                        </a>
                                    </div>
                                </div>
                                <button class="btn btn-primary btn-sm contactAdd" data-inputMask="(999) 999-9999" data-placeholder="Телефон" type="button">+&nbsp;<i class="fa fa-phone"></i></button>
                                <button class="btn btn-warning btn-sm contactAdd" data-placeholder="Почта" type="button">+&nbsp;<i class="fa fa-envelope"></i></button>
                                <button class="btn btn-success btn-sm contactAdd" data-placeholder="Skype" type="button">+&nbsp;<i class="fa fa-skype"></i></button>
                                <button class="btn btn-info btn-sm contactAdd" data-placeholder="Мессенджер" type="button">+&nbsp;<i class="fa fa-telegram"></i></button>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Ответственный</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-10 m-b" name="account" id="account_manager">
                                    <option value="0" selected="selected">Выберите ответственного сорудника</option>
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Комментарий</label>
                            <div class="col-sm-10"><textarea rows="3" class="form-control" placeholder="Резюме ответственного  сотрудника по теккущему запросу, дата первого изменения этого поля будет отдельно сохранена в БД"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer" style="display:block !important">
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                        </div>
                        <div class="col">
                            <button type="button" class="ladda-button btn btn-primary pull-right" data-style="zoom-in">Сохранить</button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    <div class="modal inmodal" id="addClientF" tabindex="-1" role="dialog" aria-modal="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                    <h4 class="modal-title">Новый ЗАКАЗЧИК - ФИЗИЧЕСКОЕ ЛИЦО</h4>
                    <small>Заказчик физическое лицо, идентифицируйтся в системе по паспорту</small>
                </div>
                <div class="modal-body">
                    <form method="get" id="addClientFormF">
                        <input type="hidden" name="client_id" value="0">
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Фамилия</label>
                            <div class="col-sm-10"><input type="text" name="lastname" class="form-control"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Имя</label>
                            <div class="col-sm-10"><input type="text" name="firstname" class="form-control"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Отчество</label>
                            <div class="col-sm-10"><input type="text" name="middlename" class="form-control"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Паспорт</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="passport_series" placeholder="серия">
                            </div>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="passport_number" placeholder="номер">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row social_form">
                            <label class="col-sm-2 col-form-label">Контакт</label>
                            <div class="col-sm-10">
                                <div class="inputHolder">
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                        </div>
                                        <input type="text" class="form-control" name="contact" data-mask="(999) 999-9999" placeholder="Телефон" value="(916)345-6789">
                                    </div>
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend"><span class="input-group-addon"><i class="fa fa-skype"></i></span></div>
                                        <input type="text" class="form-control" name="social[]" placeholder="Телефон" value="skype_login">
                                        <a class="input-group-addon text-danger deleteContact">
                                            <span class="fa fa-times"></span>
                                        </a>
                                    </div>
                                </div>
                                <button class="btn btn-primary btn-sm contactAdd" data-inputmask="(999) 999-9999" data-placeholder="Телефон" type="button">+&nbsp;<i class="fa fa-phone"></i></button>
                                <button class="btn btn-warning btn-sm contactAdd" data-placeholder="Почта" type="button">+&nbsp;<i class="fa fa-envelope"></i></button>
                                <button class="btn btn-success btn-sm contactAdd" data-placeholder="Skype" type="button">+&nbsp;<i class="fa fa-skype"></i></button>
                                <button class="btn btn-info btn-sm contactAdd" data-placeholder="Мессенджер" type="button">+&nbsp;<i class="fa fa-telegram"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer" style="display:block !important">
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                        </div>
                        <div class="col">
                            <button type="button" id="saveContactF_next" class="ladda-button btn btn-success pull-right" data-style="zoom-in">Сохранить и новый объект</button>
                            <button type="button" id="saveContactF" class="ladda-button btn btn-primary pull-right mr-1" data-style="zoom-in">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal inmodal" id="addClientU" tabindex="-1" role="dialog" aria-modal="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                    <h4 class="modal-title">Новый ЗАКАЗЧИК - ЮРИДИЧЕСКОЕ ЛИЦО</h4>
                    <small>Заказчики - это сущности в CRM, с которыми можно заключить договор</small>
                </div>
                <div class="modal-body">
                    <form method="get" id="addClientFormU">
                        <div class="form-group row"><label class="col-sm-2 col-form-label">ИНН</label>
                            <div class="col-sm-10"><input name="itn" type="text" class="form-control" placeholder="Начините вводить цифры">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">ОПФ</label>
                            <div class="col-sm-10"> <select class="form-control m-b" name="BPA">
                                    <option>ООО</option>
                                    <option>ИП</option>
                                    <option>АО</option>
                                    <option>ГУП</option>
                                </select></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Название</label>
                            <div class="col-sm-10"><input type="text" name="company_name" class="form-control typeahead" placeholder="Название организации">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Имя (ФИО)</label>
                            <div class="col-sm-10"><input type="text" name="name" class="form-control" placeholder="Контаткного лица">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row social_form">
                            <label class="col-sm-2 col-form-label">Контакт</label>
                            <div class="col-sm-10">
                                <div class="inputHolder">
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                        </div>
                                        <input type="text" class="form-control" data-mask="(999) 999-9999" name="contact" placeholder="Телефон" value="(916)345-6789">
                                    </div>
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend"><span class="input-group-addon"><i class="fa fa-skype"></i></span></div>
                                        <input type="text" class="form-control" name="social[][skype]" placeholder="Телефон" value="skype_login">
                                        <a class="input-group-addon text-danger deleteContact">
                                            <span class="fa fa-times"></span>
                                        </a>
                                    </div>
                                </div>
                                <button class="btn btn-primary btn-sm contactAdd" data-inputmask="(999) 999-9999" data-placeholder="Телефон" type="button">+&nbsp;<i class="fa fa-phone"></i></button>
                                <button class="btn btn-warning btn-sm contactAdd" data-placeholder="Почта" type="button">+&nbsp;<i class="fa fa-envelope"></i></button>
                                <button class="btn btn-success btn-sm contactAdd" data-placeholder="Skype" type="button">+&nbsp;<i class="fa fa-skype"></i></button>
                                <button class="btn btn-info btn-sm contactAdd" data-placeholder="Мессенджер" type="button">+&nbsp;<i class="fa fa-telegram"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer" style="display:block !important">
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                        </div>
                        <div class="col">
                            <button type="button" id="saveContactU_next" class="ladda-button btn btn-success pull-right" data-style="zoom-in">Сохранить и новый договор</button>
                            <button type="button" id="saveContactU" class="ladda-button btn btn-primary pull-right mr-1" data-style="zoom-in">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      
    <div class="modal inmodal" id="addObject" tabindex="-1" role="dialog" aria-modal="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                    <h4 class="modal-title">Новый Объект</h4>
                    <small>ОБъекты это адреса по которым ведется работа. Связаны с Заказчиками</small>
                </div>
                <div class="modal-body">
                    <form method="get" id="addObjectForm">
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Заказчик</label>
                            <div class="col-sm-10">
                                <select class="clientsInModal form-control" name="customer_id">
                                    <option></option>
                                    <option value="new">Новый заказчик</option>
                                    <option value="22">ЗАО &laquo;АРКС&raquo;</option>
                                    <option value="12">ИП Бахруничев Игорь Петрович</option>
                                    <option value="2">Сидоренко Анна Павловна</option>
                                    <option value="7" selected>Игнатов Ахмет аль Саалах Оглы</option>
                                    <option value="4">ООО &laquo;Застройщик&raquo;</option>
                                    <option value="8">Паша (сосед по даче)</option>
                                    <option value="17">ГУП &laquo;Водоканал&raquo;</option>
                                    <option value="32">Министрество промышленности и торговли Российской Федерации</option>
                                </select>
                                <span class="form-text m-b-none small">Если на предыдущей форме заказчик был выбран, то и в этом списке будет выбран тот же заказчик</span>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Краткое название</label>
                            <div class="col-sm-10"><input type="text" name="object_name" class="form-control typeahead" placeholder="Обязательное поле">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Адрес</label>
                            <div class="col-sm-10"><input type="text" name="object_address" class="form-control" placeholder="Используется autocomplete по API Яндекс.Карт">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Площадь</label>
                            <div class="col-sm-10">
                                <div class="input-group m-b">
                                    <input type="text" name="object_area" class="form-control" placeholder="Только число">
                                    <div class="input-group-append" >
                                        <span class="input-group-addon">м<sup>2</sup></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer" style="display:block !important">
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                        </div>
                        <div class="col">
                            <button type="button" id="saveObject" class="ladda-button btn btn-primary pull-right" data-style="zoom-in">Сохранить</button>
                            <button type="button" id="saveObject_next" class="ladda-button btn btn-primary pull-right" data-style="zoom-in">Сохранить и создать договор</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  

    
    <div class="modal inmodal" id="addContract" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                        <h4 class="modal-title">Новый договор</h4>
                    </div>
                    <div class="modal-body">
                        <form method="get" id="addContractForm">
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Заказчик</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="customer_id" id="clientSelect"></select>
                                </div>
                                <div class="col-sm-2">
                                    <div class="btn-group">
                                        <button data-toggle="dropdown" class="btn btn-primary btn-lg btn-block btn-outline dropdown-toggle"><i class="fa fa-plus"></i>&nbsp;Новый</button>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="#">Юридическое лицо</a></li>
                                            <li><a class="dropdown-item" href="#" class="font-bold">Физическое лицо</a></li>
                                        </ul>
                                    </div>
                                    <!--button class="btn btn-primary btn-lg btn-block btn-outline" type="button"><i class="fa fa-plus"></i>&nbsp;Заказчик</button-->
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Объект</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="object_id" id="objectSelect"></select>
                                </div>
                                <div class="col-sm-2">
                                    <button class="btn btn-primary btn-lg btn-block btn-outline" type="button"><i class="fa fa-plus"></i>&nbsp;Объект</button>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group  row">
                                <label class="col-sm-2 col-form-label">Номер</label>
                                <div class="col-sm-4">
                                    <input name="number" type="text" class="form-control" placeholder="Автоподстановка инкрементного номера">
                                </div>
                                <div class="col-sm-6"> 
                                    <select name="type_payment" class="contactsInModal form-control">
                                        <option value="" selected="yes">Выберите тип оплаты</option>
                                        <option value="0">Наличка</option>
                                        <option value="1">Безналичка</option>
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Сумма</label>
                                <div class="col-sm-5">
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon"><i class="fa fa-rouble"></i></span>
                                        </div>
                                        <input type="text" name="payment" placeholder="" class="form-control">
                                        <span class="form-text m-b-none small">Вы должны распределить всю сумму по этапам</span>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="input-group date">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="contract_date" class="form-control" value="<?php echo date('d.m.Y'); ?>" placeholder="Дата договора">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="calendar-palnning">
                                <div class="form-group row">
                                    <div class="col-sm-2">
                                    </div>
                                    <div class="col-sm-10">
                                        <h4>Календарный план договора</h4>
                                    </div>
                                </div>
                                <div id="step_1" class="calendar-palnning-step first">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label step">Этап №1</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="plan[stage_name][]" class="form-control" placeholder="Название этапа">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-2">
                                            <div class="btn-group" role="group" aria-label="actions">
                                                <button class="btn btn-info" type="button"><i class="fa fa-plus"></i></button>
                                                <button class="btn btn-danger" type="button" disabled=""><i class="fa fa-trash"></i></button>
                                            </div>
                                        </div>
                                        <div class="input-group date-range col-sm-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" name="plan[stage_start][]" class="form-control" placeholder="Начало этапа">
                                        </div>
                                        <div class="input-group date-range col-sm-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" name="plan[stage_end][]" class="form-control" placeholder="Окончание этапа">
                                        </div>
                                        <div class="input-group col-sm-4">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon"><i class="fa fa-rouble"></i></span>
                                            </div>
                                            <input type="text" name="plan[stage_summ][]" placeholder="сумма этапа" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Комментарий</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="plan[comment][]" class="form-control" placeholder="Будет виден только вам">
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer" style="display:block !important">
                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                            </div>
                            <div class="col">
                                <button type="button" id="saveContract" class="ladda-button btn btn-primary pull-right" data-style="zoom-in"><span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span></button>
                                <button type="button" id="saveContract_next" class="ladda-button btn btn-primary pull-right" data-style="zoom-in"><span class="ladda-label">Сохранить и перейти к планированию</span><span class="ladda-spinner"></span></button>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?=$footer_scripts;?>

<script src="/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="/js/plugins/datapicker/bootstrap-datepicker.ru.min.js"></script>

<script src="/js/plugins/select2/select2.full.min.js"></script>
<script>
        var stuff = null;
        var partners = null;
        var fio_random = ['Сергей', 'Максим', 'Дмитрий', 'Вячеслав ГЮ', 'Руслан', 'М.Женя', 'Эльдар замер', 'София Перелыгина', 'РОман', 'Энди', 'Виктор', 'Руслан', 'Елена Мирославовна', 'Ольга', 'Армен', 'Елена Викторовна от К', 'Андрей', 'Рафаэль от К', 'Юрий от К', 'Алла', 'Ольга Исаева', 'Лакоба', 'Лакоба', 'Илья', 'Оксана'];
        var contact_random = ['Сергей', 'Максим', 'Дмитрий', 'Вячеслав ГЮ', 'Руслан', 'М.Женя', 'Эльдар замер', 'София Перелыгина', 'РОман', 'Энди', 'Виктор', 'Руслан', 'Елена Мирославовна', 'Ольга', 'Армен', 'Елена Викторовна от К', 'Андрей', 'Рафаэль от К', 'Юрий от К', 'Алла', 'Ольга Исаева', 'Лакоба', 'Лакоба', 'Илья', 'Оксана'];
        var s_actionButtons = $('#buttonHolder').html();
        $(document).ready(function() {
            $(document).on('preInit.dt', function(e, settings) {
                //console.log('Init');
            });
            //$.fn.modal.Constructor.prototype.enforceFocus = function() {};
            //$.fn.modal.Constructor.prototype._enforceFocus = function() {};
            $.fn.DataTable.ext.classes.sFilterInput = "form-control form-control-lg";

            //var dataJson = { [csrfName]: csrfHash };
            contactsTable = $('table#contactsTable').DataTable({
                language: { url: '/js/plugins/dataTables/Russian.json' },
                pageLength: 10,
                processing: true,
                responsive: true,
                paging: true,
                searching: true,
                serverSide: true, // пока у нас не много партнеров, нет смысла перегружать сервер пагинацией, сортировкой и фильтрацией
                ajax: {
                    url: '<?=$ajax;?>',
                    type: 'POST',
                    dataSrc: 'aaData',
                    //data:dataJson
                },
                columnDefs: [
                    { className: "align-middle", targets: "_all" },
                ],
                columns: [
                    { name: 'id', data: 0, title: '№', width: "15px" },
                    // Дата приходит в виде UNIX-метки в миллисекундах
                    {
                        name: 'date',
                        data: 1,
                        title: 'Дата',
                        width: "60px",
                    },
                    {
                        name: 'address',
                        data: 2,
                        title: 'Адрес объекта',
                        //width: '70%',
                    },
                    // В наборе тестовых данных от сервера не приходит ФИО, потому просто создаем его случайно на клиенте
                    {
                        name: 'who',
                        data: 3,
                        title: 'ФИО',
                    },
                    // В наборе тестовых данных от сервера не приходит ФИО, потому просто создаем его случайно на клиенте
                    // по хорошему сюда нужно отдавать JSON объект содержащий все контактные данные этого контакта, с указанием типа и маски ровно так как это хранится в форме ввода этих контаткных данных.
                    {
                        name: 'contact',
                        data: 4,
                        title: 'Связь',
                        width: '150px',
                    },
                    { name: "task", data: 5, title: "Суть запроса" },
                    { name: "referrer", data: 6, title: "От кого" },
                    { name: "manager", data: 7, title: "Поручен" }, 
                    {
                        name: "action",
                        data: 8,
                        orderable: false,
                        title: "Действие",
                    },
                ],
                order: [
                    [0, 'asc']
                ],
                dom: "<'row'<'col-sm-12 col-md-3'f><'col-sm-12 col-md-6 yearsSelector'><'col-sm-12 col-md-3 text-right'i>>" + 
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row mt-2'<'col-sm-12 col-md-6'p><'col-sm-12 col-md-6 text-right pull-right'l>>",

            });
            
            $('body').on('click','.type_work_btn', function () {
                setTimeout( function () {
                    var type = $('.yearsSelector').find('.active');
                    var arr = [];
                    for (let index = 0; index < type.length; index++) {
                        const element = type[index];
                        arr[index] = $(element).attr('var_type');
                    }
                    contactsTable
                        .columns( 6 )
                        .search( String(arr) )
                        .draw();
                }, 100 )
            })

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.deleteContact').click(function() { $(this).parent().detach() });

            /*
            var l = $('.ladda-button').ladda();


            l.click(function() {
                // Start loading
                l.ladda('start');

                // Do something in backend and then stop ladda
                // setTimeout() is only for demo purpose
                setTimeout(function() {
                    l.ladda('stop');
                    $('#addContact').modal('hide');
                    swal({
                        title: "Успешно!",
                        text: "Новый клиент успешно сохранен",
                        type: "success"
                    });

                }, 500);

            });
            */

        });
        
        function deleteContact(el) {
            $(el).parent().detach();
        }
        
       
        $('body').on('click','.popup_edit', function(e){
            e.preventDefault();
            id = $(this).attr('var_id');
            if( id ){
                $.ajax({
                    url: '/clients/selectContactForm/'+id,
                    type: 'post',
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    complete: function() {
                    },
                    success: function(json) {
                        //console.log( json );
                        $('#editClientForm input[type="checkbox"]').prop("checked", false);
                        Object.keys(json).forEach(function(key) {
                            let val = json[key];
                            if( $('#editClientForm input[name="'+ key +'"]') ){
                                $('#editClientForm input[name="'+ key +'"]').val(val);
                            }
                            if ( key == 'type_work_array' ) {
                                //$('#editClientForm input[name="type_work[]"]');
                                for (let index = 0; index < val.length; index++) {
                                    //console.log( val[index] );
                                    $('#editClientForm input[value="'+val[index]['name_work']+'"]').prop("checked", true);
                                }
                                //type_work[]
                                //'name_work'
                            }
                        })
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            }
            
        });

        /*
        $('.typeahead').typeahead({
            source: function(query, process) {
                return $.get('/server.php?action=new_contact_add_partners_list', { query: query }, function(data) {
                    return process(data.stuff);
                });
            }
        });
        */

        $('.contactAdd').click(function() {
            cls = $(this).find('i.fa').attr('class');
            mask = $(this).data('inputmask');
            if (mask === undefined || '' == mask) {
                mask = '';
            } else {
                mask = ' data-mask="' + mask + '"';
            }

            ph = $(this).data('placeholder');
            if (ph === undefined || '' == ph) {
                ph = '';
            } else {
                ph = ' placeholder="' + ph + '"';
            }
            $(this).parent().find('.inputHolder').append('<div class="input-group m-b"><div class="input-group-prepend"><span class="input-group-addon"><i class="' + cls + '"></i></span></div><input type="text" name="social[]['+cls.replace('fa fa-','')+']" class="form-control"' + mask + ph + ' value=""><a onClick="javascript:deleteContact(this)" class="input-group-addon text-danger deleteContact"><span class="fa fa-times"></span></a></div>');
        });
        
        $('#addContact').on('show.bs.modal', function(e) {
            if (null == stuff) {
                $.get('/server.php?action=new_contact_add_stuff_list', function(result) {
                    stuff = result;
                    $("select#account_manager").select2({
                        placeholder: "Выберите ответственного",
                        allowClear: true,
                        dropdownParent: $('#addContact div.modal-body'),
                        width: '100%',
                        'data': stuff
                    });
                }, "json");

            }
            if (null == partners) {
                $.get('/server.php?action=new_contact_add_partners_list', function(result) {
                    partners = result;
                    $("select#partner").select2({
                        placeholder: "Выберите партнера",
                        allowClear: true,
                        dropdownParent: $('#addContact div.modal-body'),
                        'data': partners,
                        width: '100%',

                    });
                }, "json");

            }
        })
        </script>
        
        <script>
        $(document).ready(function() {
            $('.calendar-palnning').on('click','button',function() {
                if($(this).hasClass('btn-info')) {
                    thisStepObj = $(this).parent().parent().parent().parent();
                    newStep = thisStepObj.clone();
                    newStep.find()
                    newStep.find('input').val('');
                    newStep.insertAfter(thisStepObj);
                }
                else if($(this).hasClass('btn-danger'))
                {
                     thisStepObj = $(this).parent().parent().parent().parent();
                     thisStepObj.remove();
                }

                $('.calendar-palnning-step').each(function(index){
                    $(this).removeClass('first');
                    $(this).removeClass('last');
                    if(index == 0){
                        $(this).find('button.btn-danger').prop('disabled',true);
                        $(this).addClass('first');
                    }
                    $(this).attr('id','step_'+(index+1));
                    $(this).find('label').html('Этап №'+(index+1));
                    $('#step_'+(index+1)).datepicker({language: "ru", inputs: $('.date-range input')});
                    
                });
            });
            //$.fn.datepicker.defaults.format = "dd.mm.yyyy";

            $('.date input').datepicker({language: "ru",});
            $('.calendar-palnning-step').each(function(){
                $(this).datepicker({
                    language: "ru",
                    inputs: $('.date-range input')
                });
            });
            //var action = getUrlParameter('action');
            //if ('addNew' == action) $('#addContract').modal('show');

            //$('.footable').footable();

        });

        $('#saveContact').click(function () { 
            $('.error').removeClass('error');

            var input = $('#addClientForm').find('input,textarea,select');
            var error = false;
            for (let index = 0; index < input.length; index++) {
                const element = input[index];
                //console.log( element );

                if( $(element).hasClass('required') && !$(element).val() ){
                    $(element).addClass('error');
                    error = true;
                }
            }
            if ( error ) {
                return false;
            }
            var l = $(this).ladda();
            l.ladda('start');

                var data = $('#addClientForm').find('input,textarea,select').serialize();
                $.ajax({
                    url: '/post/saveContact',
                    type: 'post',
                    data: data,
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    complete: function() {
                        l.ladda('stop');
                        $('#addContact').modal('hide');
                        /*
                        swal({
                            title: "Успешно!",
                            text: "Новый клиент успешно сохранен",
                            type: "success"
                        });
                        */
                    },
                    success: function(json) {
                        contactsTable.ajax.reload(null, false);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            
            /*
                // Start loading
                l.ladda('start');

                // Do something in backend and then stop ladda
                // setTimeout() is only for demo purpose
                setTimeout(function() {
                    l.ladda('stop');
                    $('#addContact').modal('hide');
                    swal({
                        title: "Успешно!",
                        text: "Новый клиент успешно сохранен",
                        type: "success"
                    });

                }, 500);
            */
         })

         
        $('#updateContact').click(function () { 
            $('.error').removeClass('error');

            var input = $('#editClientForm').find('input');
            var error = false;
            for (let index = 0; index < input.length; index++) {
                const element = input[index];

                if( $(element).hasClass('required') && !$(element).val() ){
                    $(element).addClass('error');
                    error = true;
                }
            }

            if (error) {
                return false;
            }
            var l = $(this).ladda();
            l.ladda('start');

            var data = $('#editClientForm').find('input').serialize();
            $.ajax({
                url: '/home/updateContact',
                type: 'post',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                },
                complete: function() {
                    contactsTable.ajax.reload(null, false);
                    l.ladda('stop');
                    $('#editContact').modal('hide');
                    /*
                    swal({
                        title: "Успешно!",
                        text: "Клиент обновлен",
                        type: "success"
                    });
                    */
                },
                success: function(json) {
                },
                error: function(xhr, ajaxOptions, thrownError) {

                }
            });
            
         })

         $('#deleteContactBtn').click(function () { 
            var l = $('.ladda-button').ladda();
            l.ladda('start');

            var id = $('#deleteContact').find('input[name="id"]').val();
                
                $.ajax({
                    url: '/home/deleteContact/'+id,
                    type: 'post',
                    data: [],
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    complete: function() {
                        l.ladda('stop');
                        $('#deleteContact').modal('hide');
                        /*
                        swal({
                            title: "Успешно!",
                            text: "Запись удалена",
                            type: "success"
                        });
                        */
                    },
                    success: function(json) {
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            
         })

         

         /*
        $('body').on('click','.popup_edit', function (e) {
            e.preventDefault();
            $('#addContract').modal('show');
        })
        */

        $('#saveContactСlientF').click(function (e) {
            e.preventDefault()
            
            $('.error').removeClass('error');

            var input = $('#addClientForm').find('input,textarea,select');
            var error = false;
            for (let index = 0; index < input.length; index++) {
                const element = input[index];
                //console.log( element );

                if( $(element).hasClass('required') && !$(element).val() ){
                    $(element).addClass('error');
                    error = true;
                }
            }
            if ( error ) {
                return false;
            }
            var l = $(this).ladda();
            l.ladda('start');

            var data = $('#addClientForm').find('input,textarea,select').serialize();
            $.ajax({
                url: '/post/saveContact',
                type: 'post',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                },
                complete: function() {
                    l.ladda('stop');
                    $('#addContact').modal('hide');
                    $('#addClientF').modal('show');
                    /*
                    swal({
                        title: "Успешно!",
                        text: "Новый клиент успешно сохранен",
                        type: "success"
                    });
                    */
                },
                success: function(json) {

                    $('#addClientFormF').find('input[name="name"]').val(json.info.name);
                    var social = $('#social_form').find('input[social]');

                    $('.social_form').html( json.social );
                    
                    $('#addClientFormF').find('input[name="contact"]').val(json.info.contact);

                    
                    $('#addClientFormF').find('input[name="firstname"]').val(json.name[0]);
                    $('#addClientFormF').find('input[name="lastname"]').val(json.name[1]);
                    $('#addClientFormF').find('input[name="middlename"]').val(json.name[2]);
                    
                    //console.log( json.info.social );
                    contactsTable.ajax.reload(null, false);
                },
                error: function(xhr, ajaxOptions, thrownError) {

                }
            });
        })
        $('#saveContactСlientU').click(function (e) {
            e.preventDefault()
            $('.error').removeClass('error');

            var input = $('#addClientForm').find('input,textarea,select');
            var error = false;
            for (let index = 0; index < input.length; index++) {
                const element = input[index];
                //console.log( element );

                if( $(element).hasClass('required') && !$(element).val() ){
                    $(element).addClass('error');
                    error = true;
                }
            }
            if ( error ) {
                return false;
            }
            var l = $(this).ladda();
            l.ladda('start');

            var data = $('#addClientForm').find('input,textarea,select').serialize();
            $.ajax({
                url: '/post/saveContact',
                type: 'post',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                },
                complete: function() {
                    l.ladda('stop');
                    $('#addContact').modal('hide');
                    $('#addClientU').modal('show');
                    /*
                    swal({
                        title: "Успешно!",
                        text: "Новый клиент успешно сохранен",
                        type: "success"
                    });
                    */
                },
                success: function(json) {

                    $('#addClientFormU').find('input[name="name"]').val(json.info.name);
                    var social = $('#social_form').find('input[social]');

                    $('.social_form').html( json.social );
                    
                    $('#addClientFormU').find('input[name="contact"]').val(json.info.contact);
                    
                    //console.log( json.info.social );
                    contactsTable.ajax.reload(null, false);
                },
                error: function(xhr, ajaxOptions, thrownError) {

                }
            });
        })

        
    $('#saveContactF').click(function () { 
        $('.error').removeClass('error');

        var input = $('#addClientForm').find('input,textarea,select');
        var error = false;
        for (let index = 0; index < input.length; index++) {
            const element = input[index];
            //console.log( element );

            if( $(element).hasClass('required') && !$(element).val() ){
                $(element).addClass('error');
                error = true;
            }
        }
        if ( error ) {
            return false;
        }
        var l = $(this).ladda();
        l.ladda('start');

        var data = $('#addClientForm').find('input,textarea,select').serialize();
        $.ajax({
            url: '/post/saveCustomer',
            type: 'post',
            data: data,
            dataType: 'json',
            beforeSend: function() {
            },
            complete: function() {
                l.ladda('stop');
                $('#addContact').modal('hide');
                /*
                swal({
                    title: "Успешно!",
                    text: "Новый клиент успешно сохранен",
                    type: "success"
                });
                */
            },
            success: function(json) {
                clientsTable.ajax.reload(null, false);
                l.ladda('stop');
                $('#addClientF').modal('hide');
                /*
                swal({
                    title: "Успешно!",
                    text: "Клиент обновлен",
                    type: "success"
                });
                */
            },
            error: function(xhr, ajaxOptions, thrownError) {

            }
        });
        
    })

    $('#saveContactU').click(function () { 
        $('.error').removeClass('error');

        var input = $('#addClientFormU').find('input,textarea,select');
        var error = false;
        for (let index = 0; index < input.length; index++) {
            const element = input[index];
            //console.log( element );

            if( $(element).hasClass('required') && !$(element).val() ){
                $(element).addClass('error');
                error = true;
            }
        }
        if ( error ) {
            return false;
        }
        var l = $(this).ladda();
        l.ladda('start');

        var data = $('#addClientFormU').find('input,textarea,select').serialize();
        $.ajax({
            url: '/post/saveCustomer',
            type: 'post',
            data: data,
            dataType: 'json',
            beforeSend: function() {
            },
            complete: function() {
                l.ladda('stop');
                $('#addClientF').modal('hide');
                /*
                swal({
                    title: "Успешно!",
                    text: "Клиент обновлен",
                    type: "success"
                });
                */
            },
            success: function(json) {
                clientsTable.ajax.reload(null, false);
            },
            error: function(xhr, ajaxOptions, thrownError) {

            }
        });
        
    })

    
    $('#saveContactF_next').click(function () { 
        $('.error').removeClass('error');

        var input = $('#addClientForm').find('input,textarea,select');
        var error = false;
        for (let index = 0; index < input.length; index++) {
            const element = input[index];
            //console.log( element );

            if( $(element).hasClass('required') && !$(element).val() ){
                $(element).addClass('error');
                error = true;
            }
        }
        if ( error ) {
            return false;
        }
        var l = $(this).ladda();
        l.ladda('start');

        var data = $('#addClientForm').find('input,textarea,select').serialize();
        $.ajax({
            url: '/post/saveCustomer',
            type: 'post',
            data: data,
            dataType: 'json',
            beforeSend: function() {
            },
            complete: function() {
                
            },
            success: function(json) {
                l.ladda('stop');
                $('#addClientF').modal('hide');
                $('#addObject').modal('show');

                $('#addObjectForm').find('select[name="customer_id"]').html(json.customer);
                $('#clientSelect').html(json.customer);
                /*
                swal({
                    title: "Успешно!",
                    text: "Заказчик добавлен",
                    type: "success"
                });
                */
            },
            error: function(xhr, ajaxOptions, thrownError) {

            }
        });
        
    })

    $('#saveContactU_next').click(function () { 
        $('.error').removeClass('error');

        var input = $('#addClientFormU').find('input,textarea,select');
        var error = false;
        for (let index = 0; index < input.length; index++) {
            const element = input[index];
            //console.log( element );

            if( $(element).hasClass('required') && !$(element).val() ){
                $(element).addClass('error');
                error = true;
            }
        }
        if ( error ) {
            return false;
        }
        var l = $(this).ladda();
        l.ladda('start');

        var data = $('#addClientFormU').find('input,textarea,select').serialize();
        $.ajax({
            url: '/post/saveCustomer',
            type: 'post',
            data: data,
            dataType: 'json',
            beforeSend: function() {
            },
            complete: function(json) {
                l.ladda('stop');
                $('#addClientU').modal('hide');
                $('#addObject').modal('show');
                
                $('#addObjectForm').find('select[name="customer_id"]').html(json.customer);
                $('#clientSelect').html(json.customer);
                /*
                swal({
                    title: "Успешно!",
                    text: "Заказчик добавлен",
                    type: "success"
                });
                */
            },
            success: function(json) {
            },
            error: function(xhr, ajaxOptions, thrownError) {

            }
        });
        
    })
    
    $('#saveObject').click(function () { 
            $('.error').removeClass('error');

            var input = $('#addObjectForm').find('input,textarea,select');
            var error = false;
            for (let index = 0; index < input.length; index++) {
                const element = input[index];
                //console.log( element );

                if( $(element).hasClass('required') && !$(element).val() ){
                    $(element).addClass('error');
                    error = true;
                }
            }
            if ( error ) {
                return false;
            }
            var l = $(this).ladda();
            l.ladda('start');

                var data = $('#addObjectForm').find('input,textarea,select').serialize();
                $.ajax({
                    url: '/post/saveObject',
                    type: 'post',
                    data: data,
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    complete: function() {
                        l.ladda('stop');
                        $('#addObject').modal('hide');
                        /*
                        swal({
                            title: "Успешно!",
                            text: "Новый объект успешно сохранен",
                            type: "success"
                        });
                        */
                    },
                    success: function(json) {
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            
         })
        
        $('#saveObject_next').click(function () { 
            $('.error').removeClass('error');

            var input = $('#addObjectForm').find('input,textarea,select');
            var error = false;
            for (let index = 0; index < input.length; index++) {
                const element = input[index];
                //console.log( element );

                if( $(element).hasClass('required') && !$(element).val() ){
                    $(element).addClass('error');
                    error = true;
                }
            }
            if ( error ) {
                return false;
            }
            var l = $(this).ladda();
            l.ladda('start');

                var data = $('#addObjectForm').find('input,textarea,select').serialize();
                $.ajax({
                    url: '/post/saveObject',
                    type: 'post',
                    data: data,
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    complete: function(json) {
                        l.ladda('stop');
                        $('#addObject').modal('hide');


                        $('#addContract').modal('show');
                        /*
                        swal({
                            title: "Успешно!",
                            text: "Новый объект успешно сохранен",
                            type: "success"
                        });
                        */
                    },
                    success: function(json) {
                        $('#clientSelect').html(json.customer);
                        $('#objectSelect').html(json.object);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            
         })

         
        $('#saveContract').click(function () { 
            $('.error').removeClass('error');

            var input = $('#addContractForm').find('input,textarea,select');
            var error = false;
            for (let index = 0; index < input.length; index++) {
                const element = input[index];
                //console.log( element );

                if( $(element).hasClass('required') && !$(element).val() ){
                    $(element).addClass('error');
                    error = true;
                }
            }
            if ( error ) {
                return false;
            }
            var l = $(this).ladda();
            l.ladda('start');

                var data = $('#addContractForm').find('input,textarea,select').serialize();
                $.ajax({
                    url: '/post/saveContract',
                    type: 'post',
                    data: data,
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    complete: function() {
                        l.ladda('stop');
                        $('#addContract').modal('hide');
                        /*
                        swal({
                            title: "Успешно!",
                            text: "Новый контракт успешно сохранен",
                            type: "success"
                        });
                        */
                    },
                    success: function(json) {
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            
        })

        
        $('#saveContract_next').click(function () { 
            $('.error').removeClass('error');

            var input = $('#addContractForm').find('input,textarea,select');
            var error = false;
            for (let index = 0; index < input.length; index++) {
                const element = input[index];
                //console.log( element );

                if( $(element).hasClass('required') && !$(element).val() ){
                    $(element).addClass('error');
                    error = true;
                }
            }
            if ( error ) {
                return false;
            }
            var l = $(this).ladda();
            l.ladda('start');

                var data = $('#addContractForm').find('input,textarea,select').serialize();
                $.ajax({
                    url: '/post/saveContract',
                    type: 'post',
                    data: data,
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    complete: function() {
                        l.ladda('stop');
                        $('#addContract').modal('hide');
                        /*
                        swal({
                            title: "Успешно!",
                            text: "Новый контракт успешно сохранен",
                            type: "success"
                        });
                        */
                    },
                    success: function(json) {

                        window.location.href = '/project/added';
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            
        })
        </script>
<?=$footer;?>