<?=$header;?>
<?=$left_menu;?>
        <div id="page-wrapper" class="gray-bg">
            <?=$head;?>
            <div class="wrapper wrapper-content  animated fadeInRight">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h4>Заказчики <small class="m-l-sm"> это люди или компании с кем заключен договор</small></h4>
                                <div class="ibox-tools">
                                    <button class="btn btn-success btn-sm" type="button" data-toggle="modal" data-target="#addClientF"><i class="fa fa-plus"></i>&nbsp;Заказчик ФЛ&nbsp;<i class="fa fa-user"></i></button>
                                    <button class="btn btn-info btn-sm" type="button" data-toggle="modal" data-target="#addClientU"><i class="fa fa-plus"></i>&nbsp;Заказчик ЮЛ&nbsp;<i class="fa fa-briefcase"></i></button>
                                    <a class="fullscreen-link">
                                        <i class="fa fa-expand"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <table class="table table-striped table-hover border-bottom mt-3" id="clientsTable" style="width:100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
    <div class="modal inmodal" id="addClientF" tabindex="-1" role="dialog" aria-modal="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                    <h4 class="modal-title">Новый ЗАКАЗЧИК - ФИЗИЧЕСКОЕ ЛИЦО</h4>
                    <small>Заказчик физическое лицо, идентифицируйтся в системе по паспорту</small>
                </div>
                <div class="modal-body">
                    <form method="get" id="addClientForm">
                        <input type="hidden" name="client_id" value="0">
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Фамилия</label>
                            <div class="col-sm-10"><input type="text" name="lastname" class="form-control"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Имя</label>
                            <div class="col-sm-10"><input type="text" name="firstname" class="form-control"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Отчество</label>
                            <div class="col-sm-10"><input type="text" name="middlename" class="form-control"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Паспорт</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="passport_series" placeholder="серия">
                            </div>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="passport_number" placeholder="номер">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Контакт</label>
                            <div class="col-sm-10">
                                <div class="inputHolder">
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                        </div>
                                        <input type="text" class="form-control" name="contact" data-mask="(999) 999-9999" placeholder="Телефон" value="(916)345-6789">
                                    </div>
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend"><span class="input-group-addon"><i class="fa fa-skype"></i></span></div>
                                        <input type="text" class="form-control" name="social[]" placeholder="Телефон" value="skype_login">
                                        <a class="input-group-addon text-danger deleteContact">
                                            <span class="fa fa-times"></span>
                                        </a>
                                    </div>
                                </div>
                                <button class="btn btn-primary btn-sm contactAdd" data-inputmask="(999) 999-9999" data-placeholder="Телефон" type="button">+&nbsp;<i class="fa fa-phone"></i></button>
                                <button class="btn btn-warning btn-sm contactAdd" data-placeholder="Почта" type="button">+&nbsp;<i class="fa fa-envelope"></i></button>
                                <button class="btn btn-success btn-sm contactAdd" data-placeholder="Skype" type="button">+&nbsp;<i class="fa fa-skype"></i></button>
                                <button class="btn btn-info btn-sm contactAdd" data-placeholder="Мессенджер" type="button">+&nbsp;<i class="fa fa-telegram"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer" style="display:block !important">
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                        </div>
                        <div class="col">
                            <button type="button" id="saveContactF_next" class="ladda-button btn btn-success pull-right" data-style="zoom-in">Сохранить и новый договор</button>
                            <button type="button" id="saveContactF" class="ladda-button btn btn-primary pull-right mr-1" data-style="zoom-in">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal inmodal" id="addClientU" tabindex="-1" role="dialog" aria-modal="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                    <h4 class="modal-title">Новый ЗАКАЗЧИК - ЮРИДИЧЕСКОЕ ЛИЦО</h4>
                    <small>Заказчики - это сущности в CRM, с которыми можно заключить договор</small>
                </div>
                <div class="modal-body">
                    <form method="get" id="addClientFormU">
                        <div class="form-group row"><label class="col-sm-2 col-form-label">ИНН</label>
                            <div class="col-sm-10"><input name="itn" type="text" class="form-control" placeholder="Начините вводить цифры">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">ОПФ</label>
                            <div class="col-sm-10"> <select class="form-control m-b" name="BPA">
                                    <option>ООО</option>
                                    <option>ИП</option>
                                    <option>АО</option>
                                    <option>ГУП</option>
                                </select></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Название</label>
                            <div class="col-sm-10"><input type="text" name="company_name" class="form-control typeahead" placeholder="Название организации">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Имя (ФИО)</label>
                            <div class="col-sm-10"><input type="text" name="name" class="form-control" placeholder="Контаткного лица">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Контакт</label>
                            <div class="col-sm-10">
                                <div class="inputHolder">
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                        </div>
                                        <input type="text" class="form-control" data-mask="(999) 999-9999" name="contact" placeholder="Телефон" value="(916)345-6789">
                                    </div>
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend"><span class="input-group-addon"><i class="fa fa-skype"></i></span></div>
                                        <input type="text" class="form-control" name="social[][skype]" placeholder="Телефон" value="skype_login">
                                        <a class="input-group-addon text-danger deleteContact">
                                            <span class="fa fa-times"></span>
                                        </a>
                                    </div>
                                </div>
                                <button class="btn btn-primary btn-sm contactAdd" data-inputmask="(999) 999-9999" data-placeholder="Телефон" type="button">+&nbsp;<i class="fa fa-phone"></i></button>
                                <button class="btn btn-warning btn-sm contactAdd" data-placeholder="Почта" type="button">+&nbsp;<i class="fa fa-envelope"></i></button>
                                <button class="btn btn-success btn-sm contactAdd" data-placeholder="Skype" type="button">+&nbsp;<i class="fa fa-skype"></i></button>
                                <button class="btn btn-info btn-sm contactAdd" data-placeholder="Мессенджер" type="button">+&nbsp;<i class="fa fa-telegram"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer" style="display:block !important">
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                        </div>
                        <div class="col">
                            <button type="button" id="saveContactU_next" class="ladda-button btn btn-success pull-right" data-style="zoom-in">Сохранить и новый договор</button>
                            <button type="button" id="saveContactU" class="ladda-button btn btn-primary pull-right mr-1" data-style="zoom-in">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
<?=$footer_scripts;?>

<script>
    var fio_random = ['Сергей', 'Максим', 'Дмитрий', 'Вячеслав ГЮ', 'Руслан', 'М.Женя', 'Эльдар замер', 'София Перелыгина', 'РОман', 'Энди', 'Виктор', 'Руслан', 'Елена Мирославовна', 'Ольга', 'Армен', 'Елена Викторовна от К', 'Андрей', 'Рафаэль от К', 'Юрий от К', 'Алла', 'Ольга Исаева', 'Лакоба', 'Лакоба', 'Илья', 'Оксана'];
    var contact_random = ['Сергей', 'Максим', 'Дмитрий', 'Вячеслав ГЮ', 'Руслан', 'М.Женя', 'Эльдар замер', 'София Перелыгина', 'РОман', 'Энди', 'Виктор', 'Руслан', 'Елена Мирославовна', 'Ольга', 'Армен', 'Елена Викторовна от К', 'Андрей', 'Рафаэль от К', 'Юрий от К', 'Алла', 'Ольга Исаева', 'Лакоба', 'Лакоба', 'Илья', 'Оксана'];
    $(document).ready(function() {
        var s_actionButtons = $('#buttonHolder').html();
        $.fn.DataTable.ext.classes.sFilterInput = "form-control form-control-lg";


        clientsTable = $('table#clientsTable').DataTable({
            language: { url: '/js/plugins/dataTables/Russian.json' },
            pageLength: 100,
            processing: true,
            responsive: true,
            paging: true,
            searching: true,
            serverSide: true, // пока у нас не много партнеров, нет смысла перегружать сервер пагинацией, сортировкой и фильтрацией
            ajax: {
                url: '<?=$ajax;?>',
                type: 'POST',
                dataSrc: 'aaData',
            },
            columnDefs: [
                { className: "align-middle", targets: "_all" },

            ],
            columns: [
                { name: 'id', data: 0, title: '№', width: "15px" },
                //{ name: 'isCompany', data: 1, "visible": false },
                {
                    name: 'name',
                    data: 1,
                    title: 'Название',
                    //width: '70%',
                },
                // В наборе тестовых данных от сервера не приходит ФИО, потому просто создаем его случайно на клиенте
                {
                    name: 'who',
                    data: 2,
                    title: 'Контактное лицо',
                },
                // В наборе тестовых данных от сервера не приходит ФИО, потому просто создаем его случайно на клиенте
                // по хорошему сюда нужно отдавать JSON объект содержащий все контактные данные этого контакта, с указанием типа и маски ровно так как это хранится в форме ввода этих контаткных данных.
                {
                    name: 'contact',
                    data: 3,
                    title: 'Связь',
                    width: '150px',
                },
                { name: "isActive", data: 4, title: '<i class="fa fa-check"></i>',
                    render: function(data, type, row, meta){
                        return type === 'display' ? '<i class="fa fa-'+((data>0)?'check text-info':'minus text-warning')+'"></i>' : '';
                    } 
                }, 
                { name: "sum", data: 5, title: "Сумма по договорам", className: "align-middle text-right", 
                    render: function(data, type, row, meta){
                        return type === 'display' ? formatMoney(data) : data;
                    } 
                },
                { name: "contracts", data: 6, title: "Кол-во" },
                {
                    name: "action",
                    data: 7,
                    title: "Действие"
                },
            ],
            order: [
                [1, 'desc']
            ],
            dom: "<'row'<'col-sm-12 col-md-6'f><'col-sm-12 col-md-6 text-right'i>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row mt-2'<'col-sm-12 col-md-6'p><'col-sm-12 col-md-6 text-right pull-right'l>>",

        });


        $('.typeahead').typeahead({
            source: [
                { "name": "Муравлев Николай", "id": 1 },
                { "name": "Левина Анастасия", "id": 2 },
                { "name": "Федоров Иван", "id": 3 },
                { "name": "Евгения Артамонова", "id": 4 }
            ]
        });

        $('.contactAdd').click(function() {
            cls = $(this).find('i.fa').attr('class');
            mask = $(this).data('inputmask');
            if (mask === undefined || '' == mask) {
                mask = '';
            } else {
                mask = ' data-mask="' + mask + '"';
            }

            ph = $(this).data('placeholder');
            if (ph === undefined || '' == ph) {
                ph = '';
            } else {
                ph = ' placeholder="' + ph + '"';
            }
            $(this).parent().find('.inputHolder').append('<div class="input-group m-b"><div class="input-group-prepend"><span class="input-group-addon"><i class="' + cls + '"></i></span></div><input type="text" name="social[]['+cls.replace('fa fa-','')+']" class="form-control"' + mask + ph + ' value=""><a onClick="javascript:deleteContact(this)" class="input-group-addon text-danger deleteContact"><span class="fa fa-times"></span></a></div>');
        });

        $('.deleteContact').click(function() { $(this).parent().detach() });

        var l = $('.ladda-button').ladda();

        /*
        l.click(function() {
            // Start loading
            l.ladda('start');

            // Do something in backend and then stop ladda
            // setTimeout() is only for demo purpose
            setTimeout(function() {
                l.ladda('stop');
                $('#addContact').modal('hide');
                swal({
                    title: "Успешно!",
                    text: "Новый клиент успешно сохранен",
                    type: "success"
                });

            }, 500);

        });
        */

    });

    function deleteContact(el) {
        $(el).parent().detach();
    }

    
    $('#saveContactF').click(function () { 
        $('.error').removeClass('error');

        var input = $('#addClientForm').find('input,textarea,select');
        var error = false;
        for (let index = 0; index < input.length; index++) {
            const element = input[index];
            console.log( element );

            if( $(element).hasClass('required') && !$(element).val() ){
                $(element).addClass('error');
                error = true;
            }
        }
        if ( error ) {
            return false;
        }
        var l = $(this).ladda();
        l.ladda('start');

        var data = $('#addClientForm').find('input,textarea,select').serialize();
        $.ajax({
            url: '/post/saveCustomer',
            type: 'post',
            data: data,
            dataType: 'json',
            beforeSend: function() {
            },
            complete: function() {
                l.ladda('stop');
                $('#addContact').modal('hide');
                swal({
                    title: "Успешно!",
                    text: "Новый клиент успешно сохранен",
                    type: "success"
                });
            },
            success: function(json) {
                clientsTable.ajax.reload(null, false);
                l.ladda('stop');
                $('#addClientF').modal('hide');
                swal({
                    title: "Успешно!",
                    text: "Клиент обновлен",
                    type: "success"
                });
            },
            error: function(xhr, ajaxOptions, thrownError) {

            }
        });
        
    })

    $('#saveContactU').click(function () { 
        $('.error').removeClass('error');

        var input = $('#addClientFormU').find('input,textarea,select');
        var error = false;
        for (let index = 0; index < input.length; index++) {
            const element = input[index];
            console.log( element );

            if( $(element).hasClass('required') && !$(element).val() ){
                $(element).addClass('error');
                error = true;
            }
        }
        if ( error ) {
            return false;
        }
        var l = $(this).ladda();
        l.ladda('start');

        var data = $('#addClientFormU').find('input,textarea,select').serialize();
        $.ajax({
            url: '/post/saveCustomer',
            type: 'post',
            data: data,
            dataType: 'json',
            beforeSend: function() {
            },
            complete: function() {
                l.ladda('stop');
                $('#addClientF').modal('hide');
                swal({
                    title: "Успешно!",
                    text: "Клиент обновлен",
                    type: "success"
                });
            },
            success: function(json) {
                clientsTable.ajax.reload(null, false);
            },
            error: function(xhr, ajaxOptions, thrownError) {

            }
        });
        
    })

         
        $('#updateContact').click(function () { 
            $('.error').removeClass('error');

            var input = $('#editClientForm').find('input');
            var error = false;
            for (let index = 0; index < input.length; index++) {
                const element = input[index];

                if( $(element).hasClass('required') && !$(element).val() ){
                    $(element).addClass('error');
                    error = true;
                }
            }

            if (error) {
                return false;
            }
            var l = $(this).ladda();
            l.ladda('start');

            var data = $('#editClientForm').find('input').serialize();
            $.ajax({
                url: '/home/updateCustomer',
                type: 'post',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                },
                complete: function() {
                    contactsTable.ajax.reload(null, false);
                    l.ladda('stop');
                    $('#editContact').modal('hide');
                    swal({
                        title: "Успешно!",
                        text: "Клиент обновлен",
                        type: "success"
                    });
                },
                success: function(json) {
                },
                error: function(xhr, ajaxOptions, thrownError) {

                }
            });
            
         })

         $('#deleteContactBtn').click(function () { 
            var l = $('.ladda-button').ladda();
            l.ladda('start');

            var id = $('#deleteContact').find('input[name="id"]').val();
                
                $.ajax({
                    url: '/home/deleteContact/'+id,
                    type: 'post',
                    data: [],
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    complete: function() {
                        l.ladda('stop');
                        $('#deleteContact').modal('hide');
                        swal({
                            title: "Успешно!",
                            text: "Запись удалена",
                            type: "success"
                        });
                    },
                    success: function(json) {
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            
         })

         
    </script>
<?=$footer;?>