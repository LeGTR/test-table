<?=$header;?>
<?=$left_menu;?>
        <div id="page-wrapper" class="gray-bg">
            <?=$head;?>
            <div class="wrapper wrapper-content  animated fadeInRight">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h4>Контакты <small class="m-l-sm"> могут стать заказчиками если заключить с ними договор</small></h4>
                                <div class="ibox-tools">
                                
                                    <button class="btn btn-primary btn-sm" type="button" data-target="#addContact"><i class="fa fa-plus"></i>&nbsp;Добавить контакт</button>

                                    <button class="btn btn-primary btn-sm" type="button" data-target="#addContract"><i class="fa fa-plus"></i>&nbsp;Добавить контракт</button>
                                    <button class="btn btn-primary btn-sm" type="button" data-target="#addObject"><i class="fa fa-plus"></i>&nbsp;Добавить объект</button>
                                    <button class="btn btn-primary btn-sm" type="button" data-target="#addPartner"><i class="fa fa-plus"></i>&nbsp;Добавить партнера</button>

                                    <a class="fullscreen-link">
                                        <i class="fa fa-expand"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <table class="table table-striped table-hover border-bottom mt-3 display" data-page-size="15" id="contactsTable">
                                
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
<?=$footer_scripts;?>

<script src="/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="/js/plugins/datapicker/bootstrap-datepicker.ru.min.js"></script>

<script src="/js/plugins/select2/select2.full.min.js"></script>
<script>
        var stuff = null;
        var partners = null;
        var fio_random = ['Сергей', 'Максим', 'Дмитрий', 'Вячеслав ГЮ', 'Руслан', 'М.Женя', 'Эльдар замер', 'София Перелыгина', 'РОман', 'Энди', 'Виктор', 'Руслан', 'Елена Мирославовна', 'Ольга', 'Армен', 'Елена Викторовна от К', 'Андрей', 'Рафаэль от К', 'Юрий от К', 'Алла', 'Ольга Исаева', 'Лакоба', 'Лакоба', 'Илья', 'Оксана'];
        var contact_random = ['Сергей', 'Максим', 'Дмитрий', 'Вячеслав ГЮ', 'Руслан', 'М.Женя', 'Эльдар замер', 'София Перелыгина', 'РОман', 'Энди', 'Виктор', 'Руслан', 'Елена Мирославовна', 'Ольга', 'Армен', 'Елена Викторовна от К', 'Андрей', 'Рафаэль от К', 'Юрий от К', 'Алла', 'Ольга Исаева', 'Лакоба', 'Лакоба', 'Илья', 'Оксана'];
        var s_actionButtons = $('#buttonHolder').html();
        $(document).ready(function() {
            $(document).on('preInit.dt', function(e, settings) {
                //console.log('Init');
            });
            //$.fn.modal.Constructor.prototype.enforceFocus = function() {};
            //$.fn.modal.Constructor.prototype._enforceFocus = function() {};
            $.fn.DataTable.ext.classes.sFilterInput = "form-control form-control-lg";

            //var dataJson = { [csrfName]: csrfHash };
            contactsTable = $('table#contactsTable').DataTable({
                language: { url: '/js/plugins/dataTables/Russian.json' },
                pageLength: 10,
                processing: true,
                responsive: true,
                paging: true,
                searching: true,
                serverSide: true, // пока у нас не много партнеров, нет смысла перегружать сервер пагинацией, сортировкой и фильтрацией
                ajax: {
                    url: '<?=$ajax;?>',
                    type: 'POST',
                    dataSrc: 'aaData',
                    //data:dataJson
                },
                columnDefs: [
                    { className: "align-middle", targets: "_all" },
                ],
                columns: [
                    { name: 'id', data: 0, title: '№', width: "15px",
                        render: function(data, type, row, meta) {
                            //console.log(data, type, row, meta);
                            return type === 'display' ? '<a data-target="#editContact" data-id="' + row[0] + '" href="#">' + data + '</a>' : data;
                        } 
                    },
                    // Дата приходит в виде UNIX-метки в миллисекундах
                    {
                        name: 'date',
                        data: 1,
                        title: 'Дата',
                        width: "60px",
                    },
                    {
                        name: 'address',
                        data: 2,
                        title: 'Адрес объекта',
                        //width: '70%',
                    },
                    // В наборе тестовых данных от сервера не приходит ФИО, потому просто создаем его случайно на клиенте
                    {
                        name: 'who',
                        data: 3,
                        title: 'ФИО',
                    },
                    // В наборе тестовых данных от сервера не приходит ФИО, потому просто создаем его случайно на клиенте
                    // по хорошему сюда нужно отдавать JSON объект содержащий все контактные данные этого контакта, с указанием типа и маски ровно так как это хранится в форме ввода этих контаткных данных.
                    {
                        name: 'contact',
                        data: 4,
                        title: 'Связь',
                        width: '150px',
                    },
                    { name: "task", data: 5, title: "Суть запроса" },
                    { name: "referrer", data: 6, title: "От кого" },
                    { name: "manager", data: 7, title: "Поручен" }, 
                    {
                        name: "action",
                        data: 8,
                        orderable: false,
                        title: "Действие",
                    },
                ],
                order: [
                    [0, 'asc']
                ],
                dom: "<'row'<'col-sm-12 col-md-3'f><'col-sm-12 col-md-6 yearsSelector'><'col-sm-12 col-md-3 text-right'i>>" + 
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row mt-2'<'col-sm-12 col-md-6'p><'col-sm-12 col-md-6 text-right pull-right'l>>",

            });
            
            $('body').on('click','.type_work_btn', function () {
                setTimeout( function () {
                    var type = $('.yearsSelector').find('.active');
                    var arr = [];
                    for (let index = 0; index < type.length; index++) {
                        const element = type[index];
                        arr[index] = $(element).attr('var_type');
                    }
                    contactsTable
                        .columns( 6 )
                        .search( String(arr) )
                        .draw();
                }, 100 )
            })

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('body').on('click','.deleteContact', function () {
                $(this).parent().detach() 
            });

            /*
            var l = $('.ladda-button').ladda();


            l.click(function() {
                // Start loading
                l.ladda('start');

                // Do something in backend and then stop ladda
                // setTimeout() is only for demo purpose
                setTimeout(function() {
                    l.ladda('stop');
                    $('#addContact').modal('hide');
                    swal({
                        title: "Успешно!",
                        text: "Новый клиент успешно сохранен",
                        type: "success"
                    });

                }, 500);

            });
            */

        });
        
        function deleteContact(el) {
            $(el).parent().detach();
        }
        
       
        $('body').on('click','.popup_edit', function(e){
            e.preventDefault();
            id = $(this).attr('var_id');
            if( id ){
                $.ajax({
                    url: '/clients/selectContactForm/'+id,
                    type: 'post',
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    complete: function() {
                    },
                    success: function(json) {
                        //console.log( json );
                        $('#editClientForm input[type="checkbox"]').prop("checked", false);
                        Object.keys(json).forEach(function(key) {
                            let val = json[key];
                            if( $('#editClientForm input[name="'+ key +'"]') ){
                                $('#editClientForm input[name="'+ key +'"]').val(val);
                            }
                            if ( key == 'type_work_array' ) {
                                //$('#editClientForm input[name="type_work[]"]');
                                for (let index = 0; index < val.length; index++) {
                                    //console.log( val[index] );
                                    $('#editClientForm input[value="'+val[index]['name_work']+'"]').prop("checked", true);
                                }
                                //type_work[]
                                //'name_work'
                            }
                        })
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            }
            
        });

        /*
        $('.typeahead').typeahead({
            source: function(query, process) {
                return $.get('/server.php?action=new_contact_add_partners_list', { query: query }, function(data) {
                    return process(data.stuff);
                });
            }
        });
        */

        $('.contactAdd').click(function() {
            cls = $(this).find('i.fa').attr('class');
            mask = $(this).data('inputmask');
            if (mask === undefined || '' == mask) {
                mask = '';
            } else {
                mask = ' data-mask="' + mask + '"';
            }

            ph = $(this).data('placeholder');
            if (ph === undefined || '' == ph) {
                ph = '';
            } else {
                ph = ' placeholder="' + ph + '"';
            }
            $(this).parent().find('.inputHolder').append('<div class="input-group m-b"><div class="input-group-prepend"><span class="input-group-addon"><i class="' + cls + '"></i></span></div><input type="text" name="social[]['+cls.replace('fa fa-','')+']" class="form-control"' + mask + ph + ' value=""><a onClick="javascript:deleteContact(this)" class="input-group-addon text-danger deleteContact"><span class="fa fa-times"></span></a></div>');
        });
        
        $('#addContact').on('show.bs.modal', function(e) {
            if (null == stuff) {
                $.get('/server.php?action=new_contact_add_stuff_list', function(result) {
                    stuff = result;
                    $("select#account_manager").select2({
                        placeholder: "Выберите ответственного",
                        allowClear: true,
                        dropdownParent: $('#addContact div.modal-body'),
                        width: '100%',
                        'data': stuff
                    });
                }, "json");

            }
            if (null == partners) {
                $.get('/server.php?action=new_contact_add_partners_list', function(result) {
                    partners = result;
                    $("select#partner").select2({
                        placeholder: "Выберите партнера",
                        allowClear: true,
                        dropdownParent: $('#addContact div.modal-body'),
                        'data': partners,
                        width: '100%',

                    });
                }, "json");

            }
        })
        </script>
        
        <script>
        $(document).ready(function() {
            $('.calendar-palnning').on('click','button',function() {
                if($(this).hasClass('btn-info')) {
                    thisStepObj = $(this).parent().parent().parent().parent();
                    newStep = thisStepObj.clone();
                    newStep.find()
                    newStep.find('input').val('');
                    newStep.insertAfter(thisStepObj);
                }
                else if($(this).hasClass('btn-danger'))
                {
                     thisStepObj = $(this).parent().parent().parent().parent();
                     thisStepObj.remove();
                }

                $('.calendar-palnning-step').each(function(index){
                    $(this).removeClass('first');
                    $(this).removeClass('last');
                    if(index == 0){
                        $(this).find('button.btn-danger').prop('disabled',true);
                        $(this).addClass('first');
                    }
                    $(this).attr('id','step_'+(index+1));
                    $(this).find('label').html('Этап №'+(index+1));
                    $('#step_'+(index+1)).datepicker({language: "ru", inputs: $('.date-range input')});
                    
                });
            });
            //$.fn.datepicker.defaults.format = "dd.mm.yyyy";

            $('.date input').datepicker({language: "ru",});
            $('.calendar-palnning-step').each(function(){
                $(this).datepicker({
                    language: "ru",
                    inputs: $('.date-range input')
                });
            });
            //var action = getUrlParameter('action');
            //if ('addNew' == action) $('#addContract').modal('show');

            //$('.footable').footable();

        });

        $('#saveContact').click(function () { 
            $('.error').removeClass('error');

            var input = $('#addClientForm').find('input,textarea,select');
            var error = false;
            for (let index = 0; index < input.length; index++) {
                const element = input[index];
                //console.log( element );

                if( $(element).hasClass('required') && !$(element).val() ){
                    $(element).addClass('error');
                    error = true;
                }
            }
            if ( error ) {
                return false;
            }
            var l = $(this).ladda();
            l.ladda('start');

                var data = $('#addClientForm').find('input,textarea,select').serialize();
                $.ajax({
                    url: '/post/saveContact',
                    type: 'post',
                    data: data,
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    complete: function() {
                        l.ladda('stop');
                        $('#addContact').modal('hide');
                        swal({
                            title: "Успешно!",
                            text: "Новый клиент успешно сохранен",
                            type: "success"
                        });
                    },
                    success: function(json) {
                        contactsTable.ajax.reload(null, false);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            
            /*
                // Start loading
                l.ladda('start');

                // Do something in backend and then stop ladda
                // setTimeout() is only for demo purpose
                setTimeout(function() {
                    l.ladda('stop');
                    $('#addContact').modal('hide');
                    swal({
                        title: "Успешно!",
                        text: "Новый клиент успешно сохранен",
                        type: "success"
                    });

                }, 500);
            */
         })

         
        $('#updateContact').click(function () { 
            $('.error').removeClass('error');

            var input = $('#editClientForm').find('input');
            var error = false;
            for (let index = 0; index < input.length; index++) {
                const element = input[index];

                if( $(element).hasClass('required') && !$(element).val() ){
                    $(element).addClass('error');
                    error = true;
                }
            }

            if (error) {
                return false;
            }
            var l = $(this).ladda();
            l.ladda('start');

            var data = $('#editClientForm').find('input').serialize();
            $.ajax({
                url: '/home/updateContact',
                type: 'post',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                },
                complete: function() {
                    contactsTable.ajax.reload(null, false);
                    l.ladda('stop');
                    $('#editContact').modal('hide');
                    swal({
                        title: "Успешно!",
                        text: "Клиент обновлен",
                        type: "success"
                    });
                },
                success: function(json) {
                },
                error: function(xhr, ajaxOptions, thrownError) {

                }
            });
            
         })

         $('#deleteContactBtn').click(function () { 
            var l = $('.ladda-button').ladda();
            l.ladda('start');

            var id = $('#deleteContact').find('input[name="id"]').val();
                
                $.ajax({
                    url: '/home/deleteContact/'+id,
                    type: 'post',
                    data: [],
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    complete: function() {
                        l.ladda('stop');
                        $('#deleteContact').modal('hide');
                        swal({
                            title: "Успешно!",
                            text: "Запись удалена",
                            type: "success"
                        });
                    },
                    success: function(json) {
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            
         })

         

         /*
        $('body').on('click','.popup_edit', function (e) {
            e.preventDefault();
            $('#addContract').modal('show');
        })
        */

        $('body').on('click','[data-target]', function name(e) {
            e.preventDefault();
            var target = $(this).data('target');
            var id = $(this).data('id');

            if( $(target).length > 0 ){
                // $(target).modal('show');
                $(target).remove();
            }
            $.ajax({
                url: '/popup/view',
                type: 'post',
                data: {
                    'target': target,
                    'id': id
                },
                beforeSend: function() {
                },
                complete: function() {
                },
                success: function(json) {
                    //console.log( json );
                    $('body').append( json );
                },
                error: function(xhr, ajaxOptions, thrownError) {

                }
            });
            
            
            
        })
        </script>
<?=$footer;?>