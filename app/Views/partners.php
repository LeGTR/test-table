<?=$header;?>
<?=$left_menu;?>

<div id="page-wrapper" class="gray-bg">
            <?=$head;?>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h3>Партнеры <small class="m-l-sm"> участвуют в формировании контактов</small></h3>
                                <div class="ibox-tools">
                                    <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#addPartner"><i class="fa fa-plus"></i>&nbsp;Новый партнер&nbsp;<i class="fa fa-smile-o"></i></button>
                                    <a class="fullscreen-link">
                                        <i class="fa fa-expand"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover border-bottom" id="partnersTable">
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="float-right">
                    10Гб из <strong>250Гб</strong> свободно.
                </div>
                <div>
                    <strong>Все права защищены</strong> ООО &laquo;Варди&raquo; &copy; 2020-2021
                </div>
            </div>
        </div>
    <div class="modal inmodal" id="addPartner" tabindex="-1" role="dialog" aria-modal="true">
        <div class="modal-dialog">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                    <h4 class="modal-title">Новый партнер</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="addNewPartnerFail" style="display:none">
                        Добавление новой должности не удалось.
                    </div>
                    <form method="get" id="addPartnerForm">
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Имя</label>
                            <div class="col-sm-10"><input type="text" name="partner_name" class="form-control" placeholder="Имя или название партнера"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer" style="display:block !important">
                    <div class="row">
                        <div class="col">
                         <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                        </div>
                        <div class="col">
                            <button type="button" class="ladda-button btn btn-primary pull-right" id="saveNewPartner" data-style="zoom-in"><span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span></button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?=$footer_scripts;?>

<script>
    var partnersTable = null;
    $(document).ready(function() {
        $.fn.DataTable.ext.classes.sFilterInput = "form-control form-control-lg";


        partnersTable = $('table#partnersTable').DataTable({
            language: { url: 'js/plugins/dataTables/Russian.json' },
            pageLength: 25,
            responsive: true,
            paging: true,
            searching: true,
            serverSide: false, // пока у нас не много партнеров, нет смысла перегружать сервер пагинацией, сортировкой и фильтрацией
            ajax: {
                url: '<?=$ajax;?>',
                type: 'POST',
                dataSrc: 'aaData',
            },
            columns: [
                { name: 'id', data: 0, title: '№', width: "30px" },
                {
                    name: 'name',
                    data: 1,
                    title: 'Имя (название) парнера',
                    width: '70%',
                    render: function(data, type, row, meta) {
                        //console.log(data, type, row, meta);
                        return type === 'display' ? '<a href="?action=edit&id=' + row[0] + '">' + data + '</a>' : data;
                    }
                },
                { name: 'count', data: 2, title: 'Кол-во контактов' },
            ],
            order: [
                [1, 'asc']
            ],
            dom: "<'row'<'col-sm-12 col-md-6'f><'col-sm-12 col-md-6 text-right'i>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row mt-2'<'col-sm-12 col-md-6'p><'col-sm-12 col-md-6 text-right pull-right'l>>",
        });

        o_addNewPartnerFail = $('#addNewPartnerFail');
        $('#addPartner').on('hidden.bs.modal', function(e) {
            o_addNewPartnerFail.hide();
        });

        $('button#saveNewPartner').click(function() {
            t = $(this).html();
            b = $(this);
            $(this).prop("disabled", true).html('Обрабатываю...');
            $.post('server.php?action=add_new_partner', $("#addPartnerForm").serialize(), function() {}, "json")
                .done(function(data) {
                    console.log(data);
                    partnersTable.row.add(data).draw();
                    $('#addPartner').modal('hide');
                })
                .fail(function() {
                    o_addNewPartnerFail.show();
                })
                .always(function() {
                    b.prop("disabled", false).html(t);
                });
        });
    });
    </script>
<?=$footer;?>