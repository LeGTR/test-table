<?=$header;?>
<?=$left_menu;?>

<div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                        <form role="search" class="navbar-form-custom" action="search_results.html">
                            <div class="form-group">
                                <input type="text" placeholder="Поиск по всей CRM..." class="form-control" name="top-search" id="top-search">
                            </div>
                        </form>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <span class="m-r-sm text-muted welcome-message">Вт, 28 ноября 2020 11:13</span>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-bell"></i> <span class="label label-primary">8</span>
                            </a>
                            <ul class="dropdown-menu dropdown-alerts">
                                <li>
                                    <a href="activity_stream.html" class="dropdown-item">
                                        <div>
                                            <i class="fa fa-envelope fa-fw"></i> Новых 16 сообщений
                                            <span class="float-right text-muted small">4 минуты назад</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="dropdown-divider"></li>
                                <li>
                                    <a href="activity_stream.html" class="dropdown-item">
                                        <div>
                                            <i class="fa fa-twitter fa-fw"></i> 3 Просроченных задачи
                                            <span class="float-right text-muted small">12 дней назад</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="dropdown-divider"></li>
                                <li>
                                    <a href="activity_stream.html" class="dropdown-item">
                                        <div>
                                            <i class="fa fa-upload fa-fw"></i> 5 проектов завершены
                                            <span class="float-right text-muted small">4 недели назад</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="dropdown-divider"></li>
                                <li>
                                    <div class="text-center link-block">
                                        <a href="activity_stream.html" class="dropdown-item">
                                            <strong>Все уведомления</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="login.html">
                                <i class="fa fa-sign-out"></i> Выход
                            </a>
                        </li>
                        <!--li>
                            <a class="right-sidebar-toggle">
                                <i class="fa fa-tasks"></i>
                            </a>
                            </li-->
                    </ul>
                </nav>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox ">
                            <div class="ibox-title">
                                <h3>Новый проект <small id="heading_text"> Попробуйте выбрать разделы...</small></h3>
                                <div class="ibox-tools">
                                    <i class="fa fa-clock-o"></i>&nbsp;&laquo;13&raquo; ноября 2020
                                </div>
                            </div>
                            <div class="ibox-content">
                                <form method="get">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <h5>Заказчик</h5>
                                            <!--div class="input-group"></div-->
                                            <select class="clients form-control">
                                                <option></option>
                                                <option value="new">Новый заказчик</option>
                                                <option value="22">ЗАО &laquo;АРКС&raquo;</option>
                                                <option value="12">ИП Бахруничев Игорь Петрович</option>
                                                <option value="2">Сидоренко Анна Павловна</option>
                                                <option value="7">Игнатов Ахмет аль Саалах Оглы</option>
                                                <option value="4">ООО &laquo;Застройщик&raquo;</option>
                                                <option value="8">Паша (сосед по даче)</option>
                                                <option value="17">ГУП &laquo;Водоканал&raquo;</option>
                                                <option value="32">Министрество промышленности и торговли Российской Федерации</option>
                                            </select>
                                            <!--span class="input-group-append">
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addClient"><i class="fa fa-plus"></i></button>
                                                </span-->
                                        </div>
                                        <div class="col-sm-2">
                                            <h5>Договор</h5>
                                            <select class="contracts form-control">
                                                <option></option>
                                                <option value="new">Новый договор</option>
                                                <option value="2020100034">34/10 от 01.12.2019</option>
                                                <option value="2020050058">58/05 от 17.05.2020</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <h5>Объект</h5>
                                            <select class="objects form-control">
                                                <option></option>
                                                <option value="new">Новый объект</option>
                                                <option value="22">ЖК &laquo;Нева&raquo;: 1-й Красногвардейский пр-д, 22, Москва, 123317</option>
                                                <option value="12">Люимая парковка: Москва, 8-я ул Спасоглинищевского пер., д14-12, стр8 , к6, соор. 38</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group row"><label class="col-sm-2 col-form-label">Ответственный по проекту</label>
                                        <div class="col-sm-10">
                                            <div class="team-members">
                                                <a href="#"><img title="Иванов Сергей" alt="Иванов Сергей" class="rounded-circle" src="/img/a1.jpg"></a>
                                                <a href="#"><img title="Петров Сергей" class="rounded-circle" src="/img/a2.jpg"></a>
                                                <a href="#"><img title="Сидоров Сергей" class="rounded-circle" src="/img/a3.jpg"></a>
                                                <a href="#"><img title="Иванов Петр" class="rounded-circle" src="/img/a5.jpg"></a>
                                                <a href="#"><img title="Сидоров Петр" class="rounded-circle" src="/img/a6.jpg"></a>
                                            </div>
                                            <p id="project_manager"></p>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <p>
                                        Укажите этапы (разделы) в текущем проекте. Выберите столько раздеов, сколько нужно в рамках этого договора, используйте фильтры для отбора нужных разделов из общего перечня.
                                    </p>
                                    <select class="form-control dual_select" multiple>
                                        <option value="АР">АР</option>
                                        <option value="ТЗК">ТЗК</option>
                                        <option value="ТЗК по факту">ТЗК по факту</option>
                                        <option value="АП ТЗК">АП ТЗК</option>
                                        <option value="Колористический паспорт">Колористический паспорт</option>
                                        <option value="ФАСАД">ФАСАД</option>
                                        <option value="ВК">ВК</option>
                                        <option value="ЭОМ">ЭОМ</option>
                                        <option value="ПС">ПС</option>
                                        <option value="О">О</option>
                                        <option value="ОВИК">ОВИК</option>
                                        <option value="КР">КР</option>
                                        <option value="Акустика">Акустика</option>
                                        <option value="Фотомонтаж">Фотомонтаж</option>
                                        <option value="3Д визуализация">3Д визуализация</option>
                                        <option value="Эскиз">Эскиз</option>
                                        <option value="Обмеры">Обмеры</option>
                                        <option value="ППР">ППР</option>
                                        <option value="ПОС">ПОС</option>
                                        <option value="Дизайн">Дизайн</option>
                                        <option value="АГР">АГР</option>
                                        <option value="Макулатура">Макулатура</option>
                                        <option value="технология">технология</option>
                                        <option value="кислород">кислород</option>
                                        <option value="под ЭЦ">под ЭЦ</option>
                                    </select>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group row"><label class="col-sm-2 col-form-label">Краткое описание</label>
                                        <div class="col-sm-10"><input type="text" class="form-control" name="project_description" id="project_description"></div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group row">
                                        <div class="col text-center">
                                            <a role="button" class="btn btn-primary btn-sm" type="submit" id="makePlanning" style="color: white"><i class="fa fa-arrow-down"></i>&nbsp;Сформировать этапы&nbsp;<i class="fa fa-arrow-down"></i></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox ">
                            <div class="ibox-title">
                                <h5>Планирование этапов проекта </h5>
                            </div>
                            <div class="ibox-content">
                                <p>Некоторые этапы могут формироваться на основе выбранных разделов, если это нужно</p>
                                <div class="row mb-4">
                                    <div class="col-lg-6">
                                        <dl class="row mb-0">
                                            <div class="col-sm-4 text-sm-right"><dt>Вид работ:</dt> </div>
                                            <div class="col-sm-8 text-sm-left">
                                                <dd class="mb-1"><span class="label label-primary">ПРОЕКТИРОВАНИЕ</span></dd>
                                            </div>
                                        </dl>
                                        <dl class="row mb-0">
                                            <div class="col-sm-4 text-sm-right"><dt>Объект:</dt> </div>
                                            <div class="col-sm-8 text-sm-left">
                                                <dd class="mb-1" id="summary_object">ЖК Нева</dd>
                                            </div>
                                        </dl>
                                        <dl class="row mb-0">
                                            <div class="col-sm-4 text-sm-right"><dt>Ответственный:</dt> </div>
                                            <div class="col-sm-8 text-sm-left">
                                                <dd class="mb-1" id="summary_project_manager">Комаров Анна</dd>
                                            </div>
                                        </dl>
                                        <dl class="row mb-0">
                                            <div class="col-sm-4 text-sm-right"><dt>Клиент:</dt> </div>
                                            <div class="col-sm-8 text-sm-left">
                                                <dd class="mb-1" id="summary_client">ООО «Квартал»</dd>
                                            </div>
                                        </dl>
                                        <dl class="row mb-0">
                                            <div class="col-sm-4 text-sm-right"><dt>Сумма проекта:</dt> </div>
                                            <div class="col-sm-8 text-sm-left">
                                                <dd class="mb-1" id="summary_project_budget">100`000,00 ₽ (приходит из договора)</dd>
                                            </div>
                                        </dl>
                                    </div>
                                    <div class="col-lg-6" id="cluster_info">
                                        <dl class="row mb-0">
                                            <div class="col-sm-4 text-sm-right">
                                                <dt>Договор:</dt>
                                            </div>
                                            <div class="col-sm-8 text-sm-left">
                                                <dd class="mb-1" id="summary_contract">34|10 от 5.11.2020</dd>
                                            </div>
                                        </dl>
                                        <dl class="row mb-0">
                                            <div class="col-sm-4 text-sm-right">
                                                <dt>Дата начала:</dt>
                                            </div>
                                            <div class="col-sm-8 text-sm-left">
                                                <dd class="mb-1" id="summary_date_start">16.10.2020 (приходит из договора)</dd>
                                            </div>
                                        </dl>
                                        <dl class="row mb-0">
                                            <div class="col-sm-4 text-sm-right">
                                                <dt>Дата окончания:</dt>
                                            </div>
                                            <div class="col-sm-8 text-sm-left">
                                                <dd class="mb-1" id="summary_date_end">28.12.2020 (приходит из договора)</dd>
                                            </div>
                                        </dl>
                                        <dl class="row mb-0">
                                            <div class="col-sm-4 text-sm-right">
                                                <dt>Участники:</dt>
                                            </div>
                                            <div class="col-sm-8 text-sm-left">
                                                <dd class="project-people mb-1" id="summary_participants">
                                                    <a href=""><img alt="image" class="rounded-circle" src="/img/a3.jpg"></a>
                                                    <a href=""><img alt="image" class="rounded-circle" src="/img/a1.jpg"></a>
                                                    <a href=""><img alt="image" class="rounded-circle" src="/img/a2.jpg"></a>
                                                    <a href=""><img alt="image" class="rounded-circle" src="/img/a4.jpg"></a>
                                                    <a href=""><img alt="image" class="rounded-circle" src="/img/a5.jpg"></a>
                                                </dd>
                                            </div>
                                        </dl>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col text-right">
                                        <button type="button" class="btn btn-success" id="addNewTask"><i class="fa fa-plus"></i>&nbsp;Добавить этап</button>
                                    </div>
                                </div>
                                <table class="table table-bordered table-hover" id="projectTasksTable" data-reorderable-rows="true">
                                    <thead>
                                        <tr>
                                            <th data-field="id">№</th>
                                            <th data-field="task">Этап</th>
                                            <th data-field="date_start" class="text-center">Начало</th>
                                            <th data-field="date_end" class="text-center">Окончание</th>
                                            <th data-field="manager">Ответственный</th>
                                            <th data-field="needFile" class="text-center"><i title="Требовать загрузки файла для завершения этапа" class="fa fa-save"></i></th>
                                            <th data-field="needPreviousFinish" class="text-center"><i title="Этап не может быть завершен пока не завершены все предыдущие" class="fa fa-history"></i></th>
                                            <th data-field="comment">Примечание</th>
                                            <th data-field="action" class="text-center"><i class="fa fa-trash"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody id="projectTasksBody">
                                        <tr id="task3">
                                            <td>1</td>
                                            <td>Колористический план</td>
                                            <td class="date-start">17.05.2020</td>
                                            <td class="date-end">05.06.2020</td>
                                            <td>Полякова Ирина</td>
                                            <td><input type="checkbox" value=""></td>
                                            <td><input type="checkbox" value=""></td>
                                            <td>Этот этап выполняется параллельно другим</td>
                                            <td class="text-nowrap">
                                                <button class="btn btn-danger btn-sm" type="button" data-action="delete"><i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr id="task2">
                                            <td>2</td>
                                            <td>ТЗК по факту</td>
                                            <td class="date-start">17.05.2020</td>
                                            <td class="date-end">05.06.2020</td>
                                            <td>Полякова Ирина</td>
                                            <td><input type="checkbox" value=""></td>
                                            <td><input type="checkbox" value=""></td>
                                            <td>Попробуйте поиграться кнопками добавления этапов или изменением состава разделов в проекте</td>
                                            <td>
                                                <button class="btn btn-danger btn-sm" type="button" data-action="delete"><i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr id="task3">
                                            <td>3</td>
                                            <td>ФАСАД</td>
                                            <td class="date-start">20.05.2020</td>
                                            <td class="date-end">05.06.2020</td>
                                            <td>Иван Мещеряков</td>
                                            <td><input type="checkbox" value=""></td>
                                            <td><input type="checkbox" value=""></td>
                                            <td>Краткое описание этапа</td>
                                            <td>
                                                <button class="btn btn-danger btn-sm" type="button" data-action="delete"><i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="hr-line-dashed"></div>
                                <div class="row"> <div class="col">
                                    <p>
                                        Для каждого этапа можно указать "Требовать прикрепление файла при завершении этапа"?
                                    </p>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row">
                                    <div class="col">
                                        <button class="btn btn-white btn-sm" type="submit">Отмена</button>
                                    </div>
                                    <div class="col">
                                        <button class="btn btn-primary btn-sm pull-right" type="submit">Сохранить проект</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="float-right">
                    10Гб из <strong>250Гб</strong> свободно.
                </div>
                <div>
                    <strong>Все права защищены</strong> ООО &laquo;Варди&raquo; &copy; 2020-2021
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="addContact" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                        <h4 class="modal-title">Новый клиент</h4>
                        <small>Клиенты - это свободный контакт с человеком, потенциальным Заказчиком</small>
                    </div>
                    <div class="modal-body">
                        <form method="get">
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Имя (ФИО)</label>
                                <div class="col-sm-10"><input type="text" class="form-control"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">От кого</label>
                                <div class="col-sm-10"><input type="text" class="form-control typeahead" placeholder="Начните вводить Имя сотрудника"> <span class="form-text m-b-none small">Оставьте пустым, если не знаете от кого пришел клиент</span>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Адрес</label>
                                <div class="col-sm-10"><input type="text" class="form-control" placeholder="Используется autocomplete по API Яндекс.Карт">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Запрос</label>
                                <div class="col-sm-10"><textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Контакт</label>
                                <div class="col-sm-10">
                                    <div class="inputHolder">
                                        <div class="input-group m-b">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Телефон" value="(916)345-6789">
                                        </div>
                                        <div class="input-group m-b">
                                            <div class="input-group-prepend"><span class="input-group-addon"><i class="fa fa-skype"></i></span></div>
                                            <input type="text" class="form-control" data-mask="(999) 999-9999" placeholder="Телефон" value="skype_login">
                                            <a class="input-group-addon text-danger deleteContact">
                                                <span class="fa fa-times"></span>
                                            </a>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary btn-sm contactAdd" data-inputMask="(999) 999-9999" data-placeholder="Телефон" type="button">+&nbsp;<i class="fa fa-phone"></i></button>
                                    <button class="btn btn-warning btn-sm contactAdd" data-placeholder="Почта" type="button">+&nbsp;<i class="fa fa-envelope"></i></button>
                                    <button class="btn btn-success btn-sm contactAdd" data-placeholder="Skype" type="button">+&nbsp;<i class="fa fa-skype"></i></button>
                                    <button class="btn btn-info btn-sm contactAdd" data-placeholder="Мессенджер" type="button">+&nbsp;<i class="fa fa-telegram"></i></button>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Ответственный</label>
                                <div class="col-sm-10">
                                    <select class="form-control m-b" name="account">
                                        <option>Сотрудник 1</option>
                                        <option>Сотрудник 2</option>
                                        <option>Сотрудник 3</option>
                                        <option>Сотрудник 4</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer" style="display:block !important">
                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                            </div>
                            <div class="col">
                                <button type="button" class="ladda-button btn btn-primary pull-right" data-style="zoom-in">Сохранить</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="addClientFizik" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                        <h4 class="modal-title">Новое ФИЗИЧЕСКОЕ лицо</h4>
                        <small>Заказчики - это сущности в CRM, с которыми можно заключить договор</small>
                    </div>
                    <div class="modal-body">
                        <form method="get">
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Имя</label>
                                <div class="col-sm-10"><input type="text" class="form-control"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">От кого</label>
                                <div class="col-sm-10"><input type="text" class="form-control typeahead" placeholder="Начните вводить Имя сотрудника"> <span class="form-text m-b-none small">Оставьте пустым, если не знаете от кого пришел клиент</span>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Адрес</label>
                                <div class="col-sm-10"><input type="text" class="form-control" placeholder="Используется autocomplete по API Яндекс.Карт">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Запрос</label>
                                <div class="col-sm-10"><textarea class="form-control" id="exampleFormControlTextarea11" rows="3"></textarea></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Контакт</label>
                                <div class="col-sm-10">
                                    <div class="inputHolder">
                                        <div class="input-group m-b">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Телефон" value="(916)345-6789">
                                        </div>
                                        <div class="input-group m-b">
                                            <div class="input-group-prepend"><span class="input-group-addon"><i class="fa fa-skype"></i></span></div>
                                            <input type="text" class="form-control" data-mask="(999) 999-9999" placeholder="Телефон" value="skype_login">
                                            <a class="input-group-addon text-danger deleteContact">
                                                <span class="fa fa-times"></span>
                                            </a>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary btn-sm contactAdd" data-inputMask="(999) 999-9999" data-placeholder="Телефон" type="button">+&nbsp;<i class="fa fa-phone"></i></button>
                                    <button class="btn btn-warning btn-sm contactAdd" data-placeholder="Почта" type="button">+&nbsp;<i class="fa fa-envelope"></i></button>
                                    <button class="btn btn-success btn-sm contactAdd" data-placeholder="Skype" type="button">+&nbsp;<i class="fa fa-skype"></i></button>
                                    <button class="btn btn-info btn-sm contactAdd" data-placeholder="Мессенджер" type="button">+&nbsp;<i class="fa fa-telegram"></i></button>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Ответственный</label>
                                <div class="col-sm-10">
                                    <select class="form-control m-b" name="account">
                                        <option>Сотрудник 1</option>
                                        <option>Сотрудник 2</option>
                                        <option>Сотрудник 3</option>
                                        <option>Сотрудник 4</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer" style="display:block !important">
                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                            </div>
                            <div class="col">
                                <button type="button" class="ladda-button btn btn-primary pull-right" data-style="zoom-in">Сохранить</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="addClientUrik" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                        <h4 class="modal-title">Новое ЮРИДИЧЕСКОЕ лицо</h4>
                        <small>Заказчики - это сущности в CRM, с которыми можно заключить договор</small>
                    </div>
                    <div class="modal-body">
                        <form method="get">
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Имя</label>
                                <div class="col-sm-10"><input type="text" class="form-control"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">От кого</label>
                                <div class="col-sm-10"><input type="text" class="form-control typeahead" placeholder="Начните вводить Имя сотрудника"> <span class="form-text m-b-none small">Оставьте пустым, если не знаете от кого пришел клиент</span>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Адрес</label>
                                <div class="col-sm-10"><input type="text" class="form-control" placeholder="Используется autocomplete по API Яндекс.Карт">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Запрос</label>
                                <div class="col-sm-10"><textarea class="form-control" id="FormControlTextarea" rows="3"></textarea></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Контакт</label>
                                <div class="col-sm-10">
                                    <div class="inputHolder">
                                        <div class="input-group m-b">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Телефон" value="(916)345-6789">
                                        </div>
                                        <div class="input-group m-b">
                                            <div class="input-group-prepend"><span class="input-group-addon"><i class="fa fa-skype"></i></span></div>
                                            <input type="text" class="form-control" data-mask="(999) 999-9999" placeholder="Телефон" value="skype_login">
                                            <a class="input-group-addon text-danger deleteContact">
                                                <span class="fa fa-times"></span>
                                            </a>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary btn-sm contactAdd" data-inputMask="(999) 999-9999" data-placeholder="Телефон" type="button">+&nbsp;<i class="fa fa-phone"></i></button>
                                    <button class="btn btn-warning btn-sm contactAdd" data-placeholder="Почта" type="button">+&nbsp;<i class="fa fa-envelope"></i></button>
                                    <button class="btn btn-success btn-sm contactAdd" data-placeholder="Skype" type="button">+&nbsp;<i class="fa fa-skype"></i></button>
                                    <button class="btn btn-info btn-sm contactAdd" data-placeholder="Мессенджер" type="button">+&nbsp;<i class="fa fa-telegram"></i></button>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Ответственный</label>
                                <div class="col-sm-10">
                                    <select class="form-control m-b" name="account">
                                        <option>Сотрудник 1</option>
                                        <option>Сотрудник 2</option>
                                        <option>Сотрудник 3</option>
                                        <option>Сотрудник 4</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer" style="display:block !important">
                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                            </div>
                            <div class="col">
                                <button type="button" class="ladda-button btn btn-primary pull-right" data-style="zoom-in">Сохранить</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="addContract" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                        <h4 class="modal-title">Новый договор</h4>
                        <small>С заказчиком ЗАО "АРКС"</small>
                    </div>
                    <div class="modal-body">
                        <form method="get">
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Номер</label>
                                <div class="col-sm-4"><input type="text" class="form-control"></div>
                                <div class="col-sm-6"> <select class="clientsInModal form-control">
                                        <option value="">Выберите тип оплаты</option>
                                        <option value="new">Наличка</option>
                                        <option value="22">Безналичка</option>
                                    </select></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Сумма договора</label>
                                <div class="col-sm-10"><div class="input-group m-b">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-addon">руб.</span>
                                                </div>
                                                <input type="text" placeholder="" class="form-control">
                                            </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Дата заключения</label>
                                <div class="col-sm-10"><input type="text" class="form-control typeahead" placeholder="17.10.2020">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Наименование этапа
                                </label>
                                <div class="col-sm-10"><input type="text" class="form-control typeahead" placeholder="например разработка концепта">
                                </div>
                            </div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Дата начала
                                </label>
                                <div class="col-sm-10"><input type="text" class="form-control typeahead" placeholder="10.12.2020">
                                </div>
                            </div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Дата завершения
                                </label>
                                <div class="col-sm-10"><input type="text" class="form-control typeahead" placeholder="20.12.2020">
                                </div>
                                <div class="col-sm-10"><button class="btn btn-primary " type="button"><i class="fa fa-plus"></i> &nbsp;Добавить этап</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer" style="display:block !important">
                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                            </div>
                            <div class="col">
                                <button type="button" class="ladda-button btn btn-primary pull-right" data-style="zoom-in"><span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span></button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="addObject" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                        <h4 class="modal-title">Новый Объект</h4>
                        <small>ОБъекты это адреса по которым ведется работа. Связаны с Заказчиками</small>
                    </div>
                    <div class="modal-body">
                        <form method="get">
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Заказчик</label>
                                <div class="col-sm-10">
                                    <select class="clientsInModal form-control">
                                        <option></option>
                                        <option value="new">Новый заказчик</option>
                                        <option value="22">ЗАО &laquo;АРКС&raquo;</option>
                                        <option value="12">ИП Бахруничев Игорь Петрович</option>
                                        <option value="2">Сидоренко Анна Павловна</option>
                                        <option value="7" selected>Игнатов Ахмет аль Саалах Оглы</option>
                                        <option value="4">ООО &laquo;Застройщик&raquo;</option>
                                        <option value="8">Паша (сосед по даче)</option>
                                        <option value="17">ГУП &laquo;Водоканал&raquo;</option>
                                        <option value="32">Министрество промышленности и торговли Российской Федерации</option>
                                    </select>
                                    <span class="form-text m-b-none small">Если на предыдущей форме заказчик был выбран, то и в этом списке будет выбран тот же заказчик</span>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Краткое название</label>
                                <div class="col-sm-10"><input type="text" class="form-control typeahead" placeholder="Обязательное поле">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Адрес</label>
                                <div class="col-sm-10"><input type="text" class="form-control" placeholder="Используется autocomplete по API Яндекс.Карт">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Площадь</label>
                                <div class="col-sm-10">
                                    <div class="input-group m-b">
                                        <input type="text" class="form-control" placeholder="Только число">
                                        <div class="input-group-append">
                                            <span class="input-group-addon">м<sup>2</sup></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer" style="display:block !important">
                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                            </div>
                            <div class="col">
                                <button type="button" class="ladda-button btn btn-primary pull-right" data-style="zoom-in">Сохранить</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Mainly scripts -->
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
        
        <!-- Custom and plugin javascript -->
        <script src="js/inspinia.js"></script>
        <script src="js/plugins/pace/pace.min.js"></script>
        <!-- Ladda -->
        <script src="js/plugins/ladda/spin.min.js"></script>
        <script src="js/plugins/ladda/ladda.min.js"></script>
        <script src="js/plugins/ladda/ladda.jquery.min.js"></script>
        <!-- Typehead -->
        <script src="js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
        <!-- Input Mask-->
        <script src="js/plugins/jasny/jasny-bootstrap.min.js"></script>
        <!-- Sweet alert -->
        <script src="js/plugins/sweetalert/sweetalert.min.js"></script>
        <!-- Select2 -->
        <script src="js/plugins/select2/select2.full.min.js"></script>
        <!-- Dual Listbox -->
        <script src="js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
        <script type="text/javascript" src="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1567487539/jquery.tabledit.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.1/dist/bootstrap-table.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/TableDnD/1.0.3/jquery.tablednd.min.js"></script>
        <script src="https://unpkg.com/bootstrap-table@1.18.1/dist/extensions/reorder-rows/bootstrap-table-reorder-rows.min.js"></script>
            <!-- iCheck -->
    <script src="js/plugins/iCheck/icheck.min.js"></script>
        <script>
        var FIOs = ['Бебчук Тарас', 'Кубланов Рифат', 'Щедров Клавдий', 'Мухортов Аким', 'Силин Витольд', 'Семичаевский Мадлен', 'Механтьев Арно', 'Чмыхов Вячеслав', 'Голубев Ефим', 'Перфильев Наиль', 'Саввин Святослав', 'Крылов Наум', 'Шарапов Никодим', 'Кондратьев Раис', 'Ханцев Стакрат', 'Оборин Мстислав', 'Лачинов Мечеслав', 'Кудрявцев Леонард', 'Крысов Иван', 'Черкашин Генрих', 'Сабанцев Пимен'];
        var alreadyEditingPlan = true;
        var bTable = null
        var newTaskID = 10;
        $(document).ready(function() {
            bTable = $('#projectTasksTable');
            var FizikType = getUrlParameter('type');
            if ('fizik' == FizikType) $('#addClientFizik').modal('show');
            if ('urik' == FizikType) $('#addClientUrik').modal('show');
            
            
            
            $('div.team-members a').click(function() {
                $('div.team-members a img').css('border', 'none');
                oImg = $(this).find('img');
                oImg.css('border', 'medium solid #1ab394');
                $('p#project_manager').text(oImg.attr('title'));

            });

            var oHeadingText = $('div.wrapper small#heading_text');
            var oProjectDescInput = $('input[type=text][name=project_description');
            var data = [{
                id: 0,
                text: 'Фролова Елена',
                html: '<a href="#" class="client-avatar" ><img src="/img/a5.jpg"><span> Фролова Елена</span></a>',
                title: 'enchancement'
            }, {
                id: 1,
                text: 'Муравлев Николай',
                html: '<a href="#" class="client-avatar"><img src="/img/a4.jpg"><span> Муравлев Николай</span></a>',
                title: 'enchancement'
            }];
            $("select#project_manager").select2({
                data: data,
                escapeMarkup: function(markup) {
                    return markup;
                },
                templateResult: function(data) {
                    return data.html;
                },
                templateSelection: function(data) {
                    return data.text;
                }
            });


            $('.dual_select').bootstrapDualListbox({
                selectorMinimalHeight: 160,
                filterTextClear: 'Показать все',
                filterPlaceHolder: 'Фильтр по названию',
                moveSelectedLabel: 'Отобрать выбранные',
                moveAllLabel: 'Выбрать все',
                removeSelectedLabel: 'Удалить выбранные',
                removeAllLabel: 'Удалить все',
                infoText: 'Показать все {0}',
                infoTextFiltered: '<span class="badge badge-warning">Найдено</span> {0} из {1}',
                infoTextEmpty: 'Пустой список',
            });
            $('.dual_select').on('change', function() {
                val = '';
                heading_text = [];
                if ($('select.dual_select option:selected').length == 0) {
                    oHeadingText.html('');
                    return;
                }

                $('select.dual_select option:selected').each(function() {
                    heading_text.push($(this).text());
                });
                last = heading_text.pop();
                if (heading_text.length > 0) {
                    val = heading_text.join(', ') + ' и ' + last;

                } else {
                    val = last;
                }
                oHeadingText.html(val);
                oProjectDescInput.val(val)
            });

            $("select.clients").select2({
                placeholder: "Выберите заказчика, или создайте нового",
                allowClear: true,

            });
            $("select.contracts").select2({
                placeholder: "Выберите договор, или создайте новый",
                allowClear: true,

            });

            $("select.objects").select2({
                placeholder: "Выберите объект, или создайте новый",
                allowClear: true,

            });
            $('select.clients').on('select2:select', function(e) {
                var data = e.params.data;
                if (data.id == 'new') {
                    swal({
                            title: "Выберите тип",
                            text: "Выберите тип создаваемого клиента, от этого будет зависеть способ его создания",
                            type: "info",
                            showCancelButton: true,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Юридическое лицо",
                            cancelButtonText: "Физическое лицо",
                            closeOnConfirm: true,
                            closeOnCancel: true
                        },
                        function(isConfirm) {
                            if (isConfirm) {
                                $('#addClientUrik').modal('show');
                            } else {
                                $('#addClientFizik').modal('show');
                            }
                        });
                } else {
                    $('#summary_client').html(data.text);
                }
            });
            $('select.contracts').on('select2:select', function(e) {
                var data = e.params.data;

                if (data.id == 'new') {
                    $('#addContract').modal('show');
                } else {
                    $('#summary_contract').html(data.text);
                }
            });
            $('select.objects').on('select2:select', function(e) {
                var data = e.params.data;
                if (data.id == 'new') {
                    $('#addObject').modal('show');
                    $('#clientsInModal').select2({
                        placeholder: "Выберите заказчика, или создайте нового",
                        allowClear: true,
                    });
                } else {
                    $('#summary_object').html(data.text);
                }
            });
            $('.typeahead').typeahead({
                source: [
                    { "name": "Муравлев Николай", "id": 1 },
                    { "name": "Левина Анастасия", "id": 2 },
                    { "name": "Федоров Иван", "id": 3 },
                    { "name": "Евгения Артамонова", "id": 4 }
                ]
            });

            $('.contactAdd').click(function() {
                cls = $(this).find('i.fa').attr('class');
                mask = $(this).data('inputMask');
                if (mask === undefined || '' == mask) {
                    mask = '';
                } else {
                    mask = ' data-mask="' + mask + '"';
                }

                ph = $(this).data('placeholder');
                if (ph === undefined || '' == ph) {
                    ph = '';
                } else {
                    ph = ' placeholder="' + ph + '"';
                }
                $(this).parent().find('.inputHolder').append('<div class="input-group m-b"><div class="input-group-prepend"><span class="input-group-addon"><i class="' + cls + '"></i></span></div><input type="text" class="form-control"' + mask + ph + ' value=""><a onClick="javascript:deleteContact(this)" class="input-group-addon text-danger deleteContact"><span class="fa fa-times"></span></a></div>');
            });

            $('.deleteContact').click(function() { $(this).parent().detach() });

            var l = $('.ladda-button').ladda();
            makeEditable();
            //console.log(l);
            l.click(function() {
                // Start loading
                l.ladda('start');

                // Do something in backend and then stop ladda
                // setTimeout() is only for demo purpose
                setTimeout(function() {
                    l.ladda('stop');

                    $('#addContact').modal('hide');
                    $('#addObject').modal('hide');
                    $('#addClientFizik').modal('hide');
                    $('#addClientUrik').modal('hide');
                    swal({
                        title: "Успешно!",
                        text: "Новый клиент успешно сохранен",
                        type: "success"
                    });

                }, 500);

            });

            $('#makePlanning').click(function(e) {
                if (alreadyEditingPlan) {
                    swal({
                            title: "Переделать план?",
                            text: "Вы уверены что хотите стереть текущий план работ по проект и сгенерировать новый?",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Да, все заново",
                            //closeOnConfirm: false,
                            cancelButtonText: "Нет, передумал",
                        },
                        function() {
                            makePlan();
                        });
                } else {
                    makePlan();
                }
                e.preventDefault();
                return false;
            });

            $('button#addNewTask').click(function(e){
                bTable.find('tr').last().clone().appendTo(bTable);
                bTable.find('tr:last-child td').not(':last-child, :first-child, :nth-child(6), :nth-child(7)').text('');
                bTable.find('tr td:first-child').each(function(i){
                    $(this).text(i+1);
                });
            });

            $(document).on('click', 'button[data-action=delete]', function(e) {
                $(this).closest('tr').detach();
                bTable.find('tr td:first-child').each(function(i){
                    $(this).text(i+1);
                });                
            });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
            
        });

        function setPicker(element) {
            $(element).daterangepicker({
                "locale": {
                    "format": "DD.MM.YYYY",
                    "separator": " - ",
                    "applyLabel": "Выбрал",
                    "cancelLabel": "Отмена",
                    "fromLabel": "С",
                    "toLabel": "По",
                    "customRangeLabel": "Свой",
                    "weekLabel": "Н",
                    "daysOfWeek": [
                        "Вс",
                        "Пн",
                        "Вт",
                        "Ср",
                        "Чт",
                        "Пт",
                        "Сб"
                    ],
                    "monthNames": [
                        "Январь",
                        "Февраль",
                        "Март",
                        "Апрель",
                        "Май",
                        "Июнь",
                        "Июль",
                        "Август",
                        "Сентябрь",
                        "Октябрь",
                        "Ноябрь",
                        "Декабрь"
                    ],
                    "firstDay": 1
                },
                autoApply: true,
                "autoUpdateInput": false
            });
            $(element).on('apply.daterangepicker',function(ev, picker){
                td = $(element).closest('td');
                
                if(2 == td.closest('tr').find('td').index(td))
                {
                    val = picker.startDate;
                    other_val = picker.endDate;
                    sibling = 3;
                }else if(3 == td.closest('tr').find('td').index(td))
                {
                    val = picker.endDate;
                    other_val = picker.startDate;
                    sibling = 2;
                }
                other_td = $(td.closest('tr').find('td').get(sibling));
                other_td.find('input').val(other_val.format('DD.MM.YYYY'));
                other_td.find('span.tabledit-span').text(other_val.format('DD.MM.YYYY'));
                $(element).val(val.format('DD.MM.YYYY'));
                td.find('span.tabledit-span').text(val.format('DD.MM.YYYY'));
                e = jQuery.Event( "keyup", { keyCode:13, which: 13 } );
                $(element).trigger(e);
            });
        }

        function makeEditable() {

            //bTable.bootstrapTable();
            //console.log('AFTER BT');
            /*bTable.Tabledit({
                url: 'server.php',
                editButton: false,
                deleteButton: false,
                hideIdentifier: false,
                columns: {
                    identifier: [0, 'id'],
                    editable: [
                        [1, 'name'],
                        [2, 'date_start'],
                        [3, 'date_end'],
                        [4, 'manager', getFIOasJSON(6)],
                        //[5, 'needFile'],
                        //[6, 'needWait'],
                        [7, 'description']
                    ]
                },
                onDraw: function() {
                    $('table#projectTasksTable tr td:nth-child(3) input, table#projectTasksTable tr td:nth-child(4) input').each(function() {
                        setPicker(this);
                        
                    });
                }
            });
            */
        }

        function makePlan() {
            tasks = $('select.dual_select').val();
            if (tasks.length == 0) {
                swal({
                    title: "Не могу создать план",
                    text: "Необходимо выбрать хотя бы один раздел проектирования",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonClass: "btn-success",
                    confirmButtonText: "Понял, исправлю",
                    closeOnConfirm: true
                });
                return false;
            }
            $('tbody#projectTasksBody').html('');
            for (var i = 0; i < tasks.length; i++) {
                $('tbody#projectTasksBody').append('<tr id="task' + (i + 1) + '"><td>' + (i + 1) + '</td><td>' + tasks[i] + '</td><td>16.10.2020</td><td>C</td><td>' + getFIO() + '</td><td><input type="checkbox" value=""></td><td><input type="checkbox" value=""></td><td>E</td><td>                                                <button class="btn btn-success btn-sm" type="button"><i class="fa fa-plus"></i>                                                </button>                                                <button class="btn btn-danger btn-sm" type="button"><i class="fa fa-trash"></i>                                                </button></td></tr>');
            }
            //makeEditable();
            return false;
        }

        function deleteContact(el) {
            $(el).parent().detach();
        }

        function truncate(str, n) {
            return (str.length > n) ? str.substr(0, n - 1) + '&hellip;' : str;
        };

        function getFIO() {
            return FIOs[Math.floor(Math.random() * FIOs.length)];
        }

        function getFIOasJSON(limit) {
            var thisFIOs = FIOs;
            var out = {};
            for (var i = 0;
                (i < limit) && (i < thisFIOs.length); i++) {
                var r = Math.floor(Math.random() * (thisFIOs.length - i)) + i;
                var fio = thisFIOs[r];
                thisFIOs[r] = thisFIOs[i];
                thisFIOs[i] = fio;
                out[i + 1] = fio;
            }
            //console.log(JSON.stringify(out));
            return JSON.stringify(out);
        }
        function addNewTask(rowIndex){
            //console.log(rowIndex, rowData);
            let rowData = {
                    id:getNewID(), 
                    task: '',
                    date_start: '',
                    date_end: '',
                    manager: '',
                    needFile: '<input type="checkbox" value="">',
                    needPreviousFinish: '<input type="checkbox" value="">',
                    comment: '',
                    action: '<div class="btn-group">'+
                            '   <button class="btn btn-success btn-sm" type="button"><i class="fa fa-plus"></i></button>'+
                            '   <button class="btn btn-danger btn-sm" type="button"><i class="fa fa-trash"></i></button>'+
                            '</div>'
                };
            
            bTable.bootstrapTable('insertRow', {index: rowIndex, row: rowData});

            makeEditable();
        }
        function getNewID(){
            return newTaskID++;
        }
        //new s
        </script>
<?=$footer;?>