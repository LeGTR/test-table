<?=$header;?>
<?=$left_menu;?>

<div id="page-wrapper" class="gray-bg">
            <?=$head;?>
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h3>Объекты <small class="m-l-sm"> адреса по которым ведется работа</small></h3>
                                <div class="ibox-tools">
                                    <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#addObject"><i class="fa fa-plus"></i>&nbsp;Новый объект&nbsp;<i class="fa fa-building"></i></button>
                                    <a class="fullscreen-link">
                                        <i class="fa fa-expand"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <table class="table table-striped table-hover border-bottom mt-3 display" id="objectsTable" style="width:100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="float-right">
                    10Гб из <strong>250Гб</strong> свободно.
                </div>
                <div>
                    <strong>Все права защищены</strong> ООО &laquo;Варди&raquo; &copy; 2020-2021
                </div>
            </div>
        </div>
    <div class="modal inmodal" id="addPartner" tabindex="-1" role="dialog" aria-modal="true">
        <div class="modal-dialog">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                    <h4 class="modal-title">Новый партнер</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="addNewPartnerFail" style="display:none">
                        Добавление новой должности не удалось.
                    </div>
                    <form method="get" id="addPartnerForm">
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Имя</label>
                            <div class="col-sm-10"><input type="text" name="partner_name" class="form-control" placeholder="Имя или название партнера"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer" style="display:block !important">
                    <div class="row">
                        <div class="col">
                         <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                        </div>
                        <div class="col">
                            <button type="button" class="ladda-button btn btn-primary pull-right" id="saveNewPartner" data-style="zoom-in"><span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span></button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?=$footer_scripts;?>

<div id="buttonHolder" class="d-none">
        <div class="btn-group" role="group" aria-label="Редактировать/удалить объект">
            <button type="button" class="btn btn-sm btn-info" data-id="{ID}"><i class="fa fa-pencil"></i></button>
            <button type="button" class="btn btn-sm btn-danger" data-id="{ID}"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <div id="districtHolder" class="d-none">
        <div class="btn-group">
            <button data-toggle="dropdown" class="btn btn-outline btn-default dropdown-toggle">Округ: <span class="value">Показать все</span></button>
            <ul class="dropdown-menu">
                <?php
                            include('../server.php');
                            foreach($object_disticts as $k=>$district)
                            {
                                echo '<li><a class="dropdown-item" href="#">'.$district.'</a></li>';
                            }
                ?>                
                <li class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="#">Показать все <i class="fa fa-check text-success"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="modal inmodal" id="addObject" tabindex="-1" role="dialog" aria-modal="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                    <h4 class="modal-title">Новый Объект</h4>
                    <small>ОБъекты это адреса по которым ведется работа. Связаны с Заказчиками</small>
                </div>
                <div class="modal-body">
                    <form method="get" id="addObjectForm">
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Заказчик</label>
                            <div class="col-sm-10">
                                <select class="clientsInModal form-control" name="customer_id">
                                    <option></option>
                                    <option value="new">Новый заказчик</option>
                                    <option value="22">ЗАО &laquo;АРКС&raquo;</option>
                                    <option value="12">ИП Бахруничев Игорь Петрович</option>
                                    <option value="2">Сидоренко Анна Павловна</option>
                                    <option value="7" selected>Игнатов Ахмет аль Саалах Оглы</option>
                                    <option value="4">ООО &laquo;Застройщик&raquo;</option>
                                    <option value="8">Паша (сосед по даче)</option>
                                    <option value="17">ГУП &laquo;Водоканал&raquo;</option>
                                    <option value="32">Министрество промышленности и торговли Российской Федерации</option>
                                </select>
                                <span class="form-text m-b-none small">Если на предыдущей форме заказчик был выбран, то и в этом списке будет выбран тот же заказчик</span>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Краткое название</label>
                            <div class="col-sm-10"><input type="text" name="object_name" class="form-control typeahead" placeholder="Обязательное поле">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Адрес</label>
                            <div class="col-sm-10"><input type="text" name="object_address" class="form-control" placeholder="Используется autocomplete по API Яндекс.Карт">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Площадь</label>
                            <div class="col-sm-10">
                                <div class="input-group m-b">
                                    <input type="text" name="object_area" class="form-control" placeholder="Только число">
                                    <div class="input-group-append" >
                                        <span class="input-group-addon">м<sup>2</sup></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer" style="display:block !important">
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                        </div>
                        <div class="col">
                            <button type="button" id="saveObject" class="ladda-button btn btn-primary pull-right" data-style="zoom-in">Сохранить</button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>

var objectsTable = null;
var s_actionButtons = null;
var works_types = { primary: 'Проектирование', warning: 'Кадастрирование', danger: 'Перепланировка' };
var works_types_k = Object.keys(works_types);
var districts = <?php
                    echo var_export54($object_disticts).';';
                ?>

$(document).ready(function() {
    s_actionButtons = $('#buttonHolder').html();
    
    $(document).on('preInit.dt', function(e, settings) {
        
        $('.districtSelector').html($('#districtHolder').html());
    });

    objectsTable = $('table#objectsTable').DataTable({
        language: { url: 'js/plugins/dataTables/Russian.json' },
        pageLength: 50,
        responsive: true,
        paging: true,
        searching: true,
        serverSide: true, // ТОЛЬКО ДЛЯ ТЕСТА, В ПРОДАКШ ОБРАБОТКА ДОЛЖНА БЫТЬ СЕРВЕРНАЯ
        ajax: {
            url: '<?=$ajax;?>',
            type: 'POST',
            dataSrc: 'aaData',
        },
        columnDefs: [
            { className: "align-middle", targets: "_all" },

        ],
        columns: [
            { name: 'id', data: 0, title: '№' },
            {
                name: 'date',
                data: 1,
                title: 'Дата',
                width: "60px",
                
            },
            { name: 'name', data: 2, title: 'Название' },
            {
                name: 'distict',
                data: 3,
                title: 'Округ',
                
            },
            {
                name: 'address',
                data: 4,
                title: 'Адрес',
                //width: '70%',
                
            },
            { name: 'type', data: 5, title: 'Тип' },
            {
                name: 'work_type',
                data: 6,
                title: 'Вид работ',
                render: function(data, type, row, meta) {
                    //console.log(data, type, row, meta);
                    if (type === 'display') {
                        data = ''+data;
                        if (data != '0') {
                            out = '';
                            //console.log(typeof data, data, data.split(',').forEach(function(element){console.log(element)})); 
                            //    return data;
                            data.split(',').forEach((element) => {

                                out = out + ' <span class="badge badge-'+works_types_k[element-1]+'">' + works_types[works_types_k[element-1]] + '</span>';
                            });
                            return out.trim();
                        }
                        return '';
                    }
                    return data;
                }
            },
            { name: 'target', data: 7, title: 'Назначение' },
            { name: 'square', data: 8, title: 'Площадь' },
            {
                name: 'progress',
                searchable: false,
                data: 9,
                title: '<i class="fa fa-check text-info">&nbsp;</i>',
                render: function(data, type, row, meta) {
                    out = '';
                    if (type === 'display') {
                        if (0 == data)
                            return '<i class="fa fa-minus text-warning"></i>';
                        else
                            return '<i class="fa fa-check text-info"></i>';
                    }
                    return data;
                }
            },
            {
                name: "action",
                data: 10,
                searchable: false,
                sortable: false,
                title: "",
                className: "align-middle text-right",
                
            },
        ],
        order: [
            [4, 'desc']
        ],
        dom: "<'row'<'col-md-4'f><'col-md-4 districtSelector'><'col-md-4 text-right'i>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row mt-2'<'col-sm-12 col-md-6'p><'col-sm-12 col-md-6 text-right pull-right'l>>",
    });
    $(document).on('click', '.districtSelector ul li a',function() {
        
        $('.districtSelector button span.value').text($(this).text().trim());
        $('.districtSelector ul li a').each(function()
            {
                $(this).html($(this).text().trim())
            }
        );
        $(this).html($(this).text()+' <i class="fa fa-check text-success"></i>');
    });            
    
    $('#saveObject').click(function () { 
            $('.error').removeClass('error');

            var input = $('#addObjectForm').find('input,textarea,select');
            var error = false;
            for (let index = 0; index < input.length; index++) {
                const element = input[index];
                console.log( element );

                if( $(element).hasClass('required') && !$(element).val() ){
                    $(element).addClass('error');
                    error = true;
                }
            }
            if ( error ) {
                return false;
            }
            var l = $(this).ladda();
            l.ladda('start');

                var data = $('#addObjectForm').find('input,textarea,select').serialize();
                $.ajax({
                    url: '/post/saveObject',
                    type: 'post',
                    data: data,
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    complete: function() {
                        l.ladda('stop');
                        $('#addObject').modal('hide');
                        swal({
                            title: "Успешно!",
                            text: "Новый клиент успешно сохранен",
                            type: "success"
                        });
                    },
                    success: function(json) {
                        objectsTable.ajax.reload(null, false);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            
         })

         
        $('#updateContact').click(function () { 
            $('.error').removeClass('error');

            var input = $('#editClientForm').find('input');
            var error = false;
            for (let index = 0; index < input.length; index++) {
                const element = input[index];

                if( $(element).hasClass('required') && !$(element).val() ){
                    $(element).addClass('error');
                    error = true;
                }
            }

            if (error) {
                return false;
            }
            var l = $(this).ladda();
            l.ladda('start');

            var data = $('#editClientForm').find('input').serialize();
            $.ajax({
                url: '/home/updateContact',
                type: 'post',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                },
                complete: function() {
                    contactsTable.ajax.reload(null, false);
                    l.ladda('stop');
                    $('#editContact').modal('hide');
                    swal({
                        title: "Успешно!",
                        text: "Клиент обновлен",
                        type: "success"
                    });
                },
                success: function(json) {
                },
                error: function(xhr, ajaxOptions, thrownError) {

                }
            });
            
         })

         $('#deleteContactBtn').click(function () { 
            var l = $('.ladda-button').ladda();
            l.ladda('start');

            var id = $('#deleteContact').find('input[name="id"]').val();
                
                $.ajax({
                    url: '/home/deleteContact/'+id,
                    type: 'post',
                    data: [],
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    complete: function() {
                        l.ladda('stop');
                        $('#deleteContact').modal('hide');
                        swal({
                            title: "Успешно!",
                            text: "Запись удалена",
                            type: "success"
                        });
                    },
                    success: function(json) {
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            
         })
});
</script>
<?=$footer;?>