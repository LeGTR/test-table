<?=$header;?>
<?=$left_menu;?>

<div id="page-wrapper" class="gray-bg">
            <?=$head;?>
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h3>Договоры <small class="m-l-sm"> заключаются с конкретным заказчиком и имеют финансовый календарный план</small></h3>
                                <div class="ibox-tools">
                                    <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#addContract"><i class="fa fa-plus"></i>&nbsp;Новый договор&nbsp;<i class="fa fa-handshake-o"></i></button>
                                    <a class="fullscreen-link">
                                        <i class="fa fa-expand"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <table class="table table-striped table-hover border-bottom mt-3 display" id="contractsTable" style="width:100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="float-right">
                    10Гб из <strong>250Гб</strong> свободно.
                </div>
                <div>
                    <strong>Все права защищены</strong> ООО &laquo;Варди&raquo; &copy; 2020-2021
                </div>
            </div>
        </div>
        
        <div class="modal inmodal" id="addContract" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                        <h4 class="modal-title">Новый договор</h4>
                    </div>
                    <div class="modal-body">
                        <form method="get" id="addContractForm">
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Заказчик</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="customer_id" id="clientSelect"></select>
                                </div>
                                <div class="col-sm-2">
                                    <div class="btn-group">
                                        <button data-toggle="dropdown" class="btn btn-primary btn-lg btn-block btn-outline dropdown-toggle"><i class="fa fa-plus"></i>&nbsp;Новый</button>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="#">Юридическое лицо</a></li>
                                            <li><a class="dropdown-item" href="#" class="font-bold">Физическое лицо</a></li>
                                        </ul>
                                    </div>
                                    <!--button class="btn btn-primary btn-lg btn-block btn-outline" type="button"><i class="fa fa-plus"></i>&nbsp;Заказчик</button-->
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Объект</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="object_id" id="objectSelect"></select>
                                </div>
                                <div class="col-sm-2">
                                    <button class="btn btn-primary btn-lg btn-block btn-outline" type="button"><i class="fa fa-plus"></i>&nbsp;Объект</button>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group  row">
                                <label class="col-sm-2 col-form-label">Номер</label>
                                <div class="col-sm-4">
                                    <input name="number" type="text" class="form-control" placeholder="Автоподстановка инкрементного номера">
                                </div>
                                <div class="col-sm-6"> 
                                    <select name="type_payment" class="contactsInModal form-control">
                                        <option value="" selected="yes">Выберите тип оплаты</option>
                                        <option value="0">Наличка</option>
                                        <option value="1">Безналичка</option>
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Сумма</label>
                                <div class="col-sm-5">
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon"><i class="fa fa-rouble"></i></span>
                                        </div>
                                        <input type="text" name="payment" placeholder="" class="form-control">
                                        <span class="form-text m-b-none small">Вы должны распределить всю сумму по этапам</span>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="input-group date">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="contract_date" class="form-control" value="<?php echo date('d.m.Y'); ?>" placeholder="Дата договора">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="calendar-palnning">
                                <div class="form-group row">
                                    <div class="col-sm-2">
                                    </div>
                                    <div class="col-sm-10">
                                        <h4>Календарный план договора</h4>
                                    </div>
                                </div>
                                <div id="step_1" class="calendar-palnning-step first">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label step">Этап №1</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="plan[stage_name][]" class="form-control" placeholder="Название этапа">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-2">
                                            <div class="btn-group" role="group" aria-label="actions">
                                                <button class="btn btn-info" type="button"><i class="fa fa-plus"></i></button>
                                                <button class="btn btn-danger" type="button" disabled=""><i class="fa fa-trash"></i></button>
                                            </div>
                                        </div>
                                        <div class="input-group date-range col-sm-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" name="plan[stage_start][]" class="form-control" placeholder="Начало этапа">
                                        </div>
                                        <div class="input-group date-range col-sm-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" name="plan[stage_end][]" class="form-control" placeholder="Окончание этапа">
                                        </div>
                                        <div class="input-group col-sm-4">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon"><i class="fa fa-rouble"></i></span>
                                            </div>
                                            <input type="text" name="plan[stage_summ][]" placeholder="сумма этапа" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Комментарий</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="plan[comment][]" class="form-control" placeholder="Будет виден только вам">
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer" style="display:block !important">
                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                            </div>
                            <div class="col">
                                <button type="button" id="saveContract" class="ladda-button btn btn-primary pull-right" data-style="zoom-in"><span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span></button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="buttonHolder" class="d-none">
            <div class="btn-group" role="group" aria-label="Редактировать/удалить договор">
                <button type="button" class="btn btn-sm btn-info" data-id="{ID}"><i class="fa fa-pencil"></i></button>
                <button type="button" class="btn btn-sm btn-danger" data-id="{ID}"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <div id="yearHolder" class="d-none">
            <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-outline btn-default dropdown-toggle">Год: 2020</button>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="#" class="font-bold">2020 <i class="fa fa-check text-success"></i></a></li>
                    <li><a class="dropdown-item" href="#">2019</a></li>
                    <li><a class="dropdown-item" href="#">2018</a></li>
                    <li class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="#">Показать все</a></li>
                </ul>
            </div>
        </div>
<?=$footer_scripts;?>
<script src="/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="/js/plugins/datapicker/bootstrap-datepicker.ru.min.js"></script>
<script src="/js/plugins/select2/select2.full.min.js"></script>

<div id="yearHolder" class="d-none">
            <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-outline btn-default dropdown-toggle">Год: 2020</button>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="#" class="font-bold">2020 <i class="fa fa-check text-success"></i></a></li>
                    <li><a class="dropdown-item" href="#">2019</a></li>
                    <li><a class="dropdown-item" href="#">2018</a></li>
                    <li class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="#">Показать все</a></li>
                </ul>
            </div>
        </div>
<script>
        var contactsTable = null;
        var s_actionButtons = null;
        $(document).ready(function() {
            $(document).on('preInit.dt', function(e, settings) {
                $('.yearsSelector').html($('#yearHolder').html());
                console.log('Init');
            });
            s_actionButtons = $('#buttonHolder').html();
            contactsTable = $('table#contractsTable').DataTable({
                language: { url: 'js/plugins/dataTables/Russian.json' },
                pageLength: 100,
                responsive: true,
                paging: true,
                searching: true,
                serverSide: true, // ТОЛЬКО ДЛЯ ТЕСТА, В ПРОДАКШ ОБРАБОТКА ДОЛЖНА БЫТЬ СЕРВЕРНАЯ
                ajax: {
                    url: '<?=$ajax;?>',
                    type: 'POST',
                    dataSrc: 'aaData',
                },
                columnDefs: [
                    { className: "align-middle", targets: "_all" },

                ],
                columns: [
                    { name: 'id', data: 0, title: '№', "visible": false },
                    {
                        name: 'date',
                        data: 1,
                        title: 'Заключён',
                        width: "60px",
                        render: function(data, type, row, meta) {
                            return type === 'display' || type === 'filter' ? getDateFromUNIX(data) : data;
                        }
                    },
                    { name: 'num', data: 0, title: '№' },
                    {
                        name: 'client',
                        data: 2,
                        title: 'Клиент',
                        //width: '70%',
                        render: function(data, type, row, meta) {
                            //console.log(data, type, row, meta);
                            return type === 'display' ? '<a href="?action=edit&id=' + row[0] + '">' + data + '</a>' : data;
                        }
                    },
                    // В наборе тестовых данных от сервера не приходит ФИО, потому просто создаем его случайно на клиенте
                    {
                        name: 'date_start',
                        data: 3,
                        title: 'Начало',
                        render: function(data, type, row, meta) {
                            return type === 'display' || type === 'filter' ? getDateFromUNIX(data) : data;
                        }
                    },
                    // В наборе тестовых данных от сервера не приходит ФИО, потому просто создаем его случайно на клиенте
                    // по хорошему сюда нужно отдавать JSON объект содержащий все контактные данные этого контакта, с указанием типа и маски ровно так как это хранится в форме ввода этих контаткных данных.
                    {
                        name: 'progress',
                        data: 4,
                        title: '<i class="fa fa-check text-info">&nbsp;</i>',
                        render: function(data, type, row, meta) {
                            out = '';
                            if (type === 'display') {
                                if ('' == data)
                                    return '<i class="fa fa-minus text-warning"></i>';
                                else
                                    return '<i class="fa fa-check text-info"></i>';
                            }
                            return data;
                        }
                    },
                    {
                        name: 'date_end',
                        data: 5,
                        title: 'Окончание',
                        render: function(data, type, row, meta) {
                            return type === 'display' || type === 'filter' ? getDateFromUNIX(data) : data;
                        }
                    },
                    {
                        name: "sum",
                        data: 6,
                        title: "Сумма, руб",
                        className: "align-middle text-right",
                        render: function(data, type, row, meta) {
                            return type === 'display' ? formatMoney(data, 0) : data;
                        }
                    },
                    {
                        name: "sum",
                        data: 7,
                        title: "Остаток, руб",
                        className: "align-middle text-right",
                        render: function(data, type, row, meta) {
                            min = 0;
                            max = row[4];

                            data = data - Math.random() * (max - min) + min;
                            return type === 'display' ? formatMoney(data, 0) : data;
                        }
                    },
                    {
                        name: "action",
                        data: 8,
                        title: "Действие",
                        width: '100px',
                        className: "align-middle text-right",
                        
                    },
                ],
                order: [
                    [1, 'desc']
                ],
                dom: "<'row'<'col-md-4'f><'col-md-4 yearsSelector'><'col-md-4 text-right'i>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row mt-2'<'col-sm-12 col-md-6'p><'col-sm-12 col-md-6 text-right pull-right'l>>",
            });

            
            $('.calendar-palnning').on('click', 'button', function() {
                if ($(this).hasClass('btn-info')) {
                    thisStepObj = $(this).parent().parent().parent().parent();
                    newStep = thisStepObj.clone();
                    newStep.find()
                    newStep.find('input').val('');
                    newStep.insertAfter(thisStepObj);
                    newStep.find('button').prop('disabled', false);
                } else if ($(this).hasClass('btn-danger')) {
                    thisStepObj = $(this).parent().parent().parent().parent();
                    thisStepObj.remove();
                }

                $('.calendar-palnning-step').each(function(index) {
                    $(this).removeClass('first');
                    $(this).removeClass('last');
                    $(this).attr('id', 'step_' + (index + 1));
                    $(this).find('label.step').html('Этап №' + (index + 1));
                    $('#step_' + (index + 1)).datepicker({ language: "ru", inputs: $('.date-range input') });
                    $(this).find('button').prop('disabled', false);

                });
                if (1 >= $('.calendar-palnning-step').length) {
                    $('.calendar-palnning-step button.btn-danger').prop('disabled', true);
                }
            });
            //$.fn.datepicker.defaults.format = "dd.mm.yyyy";

            
            $('#addContract').on('show.bs.modal', function(e) {
                $(this).find("#clientSelect").select2({
                    //language: "ru",
                    //theme: 'bootstrap4',
                    placeholder: "Выберите заказчика (или создайте нового)",
                    allowClear: true,
                    minimumInputLength: 2,
                    width: '100%',
                    dropdownParent: $('#addContract div.modal-body'),
                    ajax: {
                        url: 'server.php?action=get_clients_list_clear',
                        delay: 300, // wait 250 milliseconds before triggering the request
                        dataType: 'json',
                    }
                });
                $(this).find("#objectSelect").select2({
                    //language: "ru",
                    //theme: 'bootstrap4',
                    placeholder: { address: "Выберите объект", text: 'или создайте новый', id: 0 },
                    allowClear: true,
                    minimumInputLength: 2,
                    width: '100%',
                    dropdownParent: $('#addContract div.modal-body'),
                    ajax: {
                        url: 'server.php?action=get_objects_list_clear',
                        delay: 300, // wait 250 milliseconds before triggering the request
                        dataType: 'json',
                    },
                    templateResult: function(d) {
                        return $('<div><b>' + d.text + '</b><br>' + d.address + '</div>');
                    },
                    templateSelection: function(d) {
                        console.log(d);
                        return $('<span>' + d.address + ' (' + d.text + ')</span>');
                    }
                });
            });
            $.fn.datepicker.defaults.format = "dd.mm.yyyy";
            $('#addContract .date input').datepicker({ language: "ru", weekStart: 1 });
            $('.calendar-palnning-step').each(function() {
                $(this).datepicker({
                    language: "ru",
                    inputs: $('.date-range input')
                });
            });
            var action = getUrlParameter('action');
            if ('addNew' == action) $('#addContract').modal('show');
        });

        
        $('#saveContract').click(function () { 
            $('.error').removeClass('error');

            var input = $('#addContractForm').find('input,textarea,select');
            var error = false;
            for (let index = 0; index < input.length; index++) {
                const element = input[index];
                console.log( element );

                if( $(element).hasClass('required') && !$(element).val() ){
                    $(element).addClass('error');
                    error = true;
                }
            }
            if ( error ) {
                return false;
            }
            var l = $(this).ladda();
            l.ladda('start');

                var data = $('#addContractForm').find('input,textarea,select').serialize();
                $.ajax({
                    url: '/post/saveContract',
                    type: 'post',
                    data: data,
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    complete: function() {
                        l.ladda('stop');
                        $('#addContract').modal('hide');
                        swal({
                            title: "Успешно!",
                            text: "Новый контракт успешно сохранен",
                            type: "success"
                        });
                    },
                    success: function(json) {
                        contactsTable.ajax.reload(null, false);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            
        })
        </script>
<?=$footer;?>