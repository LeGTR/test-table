<?=$header;?>
<?=$left_menu;?>
        <div id="page-wrapper" class="gray-bg">
            <?=$head;?>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h3>Сотрудники <small class="m-l-sm"> участвуют в формировании контактов, проектов</small></h3>
                                <div class="ibox-tools">
                                    <a class="btn btn-primary btn-sm" type="button" href="contacts_add.html"><i class="fa fa-plus"></i>&nbsp;Новый сотрудник&nbsp;<i class="fa fa-address-card"></i></a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-2">
                                        <div class="btn-group">
                                            <button data-toggle="dropdown" class="btn btn-block btn-outline btn-success dropdown-toggle" id="positionFilterButton">Фильтр до должности</button>
                                            <ul class="dropdown-menu" id="postionFilterList">
                                                <?php
                                                    $positions = file_get_contents('../positions.txt');
                                                    $positions = unserialize($positions);
                                                    foreach($positions as $position)
                                                    {
                                                        echo '<li data-filter="'.$position['id'].'"><a class="dropdown-item" data-position-id="'.$position['id'].'" href="#">'.$position['position_name'].'</a></li>';
                                                    } 
                                                ?>
                                                <li class="dropdown-divider"></li>
                                                <li data-filter="all"><a class="dropdown-item showAll"  href="#">Показать всех</a></li>
                                            </ul>
                                        </div>
                                        
                                       
                                    </div>
                                    <div class="col text-center">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="positionSearchInput" placeholder="Поиск по сотрудникам">
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-outline btn-primary"  id="positionSearchButton">Поиск <i class="fa fa-chevron-right"></i></button> 
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-2 text-right">
                                        <div class="btn-group">
                                            <button data-toggle="dropdown" class="btn btn-block btn-outline btn-success dropdown-toggle" id="positionSortingButton">Cортировка: <b>А-Я</b></button>
                                            <ul class="dropdown-menu" id="postionSortingList" data-sortOrder>
                                                <li><a class="dropdown-item"  href="#" data-sort-order="asc">Cортировка: <b>А-Я</b></a></li>
                                                <li class="dropdown-divider"></li>
                                                <li><a class="dropdown-item"  href="#" data-sort-order="desc">Cортировка: <b>Я-А</b></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row stuffList">
                <?php

                //$positions = file_get_contents('positions.txt');
                //$positions = unserialize($positions);
                include('../server.php');
                /*
                    <div class="col-lg-4 p{$positions[0]['id']}" data-category="{$positions[0]['id']}" data-sort="{$stuff[0]['text']} {$positions[0]['position_name']}">
                        <div class="contact-box">
                            <a class="row" href="profile.html">
                                <div class="col-4">
                                    <div class="text-center">
                                        <img alt="image" class="rounded-circle m-t-xs img-fluid" src="img/a$image.jpg">
                                        <div class="m-t-xs font-bold position">{$positions[0]['position_name']}</div>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <h3><strong>{$stuff[0]['text']}</strong></h3>
                                    <p><i class="fa fa-map-marker"></i> Riviera State 32/106</p>
                                    <address>
                                        <strong>Twitter, Inc.</strong><br>
                                        795 Folsom Ave, Suite 600<br>
                                        San Francisco, CA 94107<br>
                                        <abbr title="Phone">P:</abbr> (123) 456-7890
                                    </address>
                                </div>
                            </a>
                        </div>
                    </div>
                */
                for($i=1; $i<40; $i++)
                {
                    shuffle($stuff);
                    shuffle($positions);
                    $image = mt_rand(1,8);
                    $phone = getRandPhones();
                    echo <<<"STUFF"
                    <div class="col-lg-3 p{$positions[0]['id']}">
                        <div class="contact-box center-version">
                            <a href="profile.html" class="data">
                                <img alt="image" class="rounded-circle" src="img/a$image.jpg">
                                <h3 class="m-b-xs"><strong>{$stuff[0]['text']}</strong></h3>
                                <div class="font-bold">{$positions[0]['position_name']}</div>
                                <address class="m-t-md">
                                    <strong>Twitter, Inc.</strong><br>
                                    795 Folsom Ave, Suite 600<br>
                                    San Francisco, CA 94107<br>
                                    <abbr title="Phone">P:</abbr> {$phone}
                                </address>
                            </a>
                            <div class="contact-box-footer">
                                <div class="m-t-xs btn-group">
                                    <a href="" class="btn btn-xs btn-white"><i class="fa fa-pencil text-info"></i> Правка </a>
                                    <a href="" class="btn btn-xs btn-white"><i class="fa fa-eye-slash text-warning"></i> Уволить</a>
                                    <a href="" class="btn btn-xs btn-white"><i class="fa fa-times text-danger"></i> Удалить</a>
                                </div>
                            </div>
                        </div>
                    </div>
STUFF;
                    }
                    ?>
                </div>
            </div>
            <div class="footer">
                <div class="float-right">
                    10Гб из <strong>250Гб</strong> свободно.
                </div>
                <div>
                    <strong>Все права защищены</strong> ООО &laquo;Варди&raquo; &copy; 2020-2021
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="addStuff" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                        <h4 class="modal-title">Новый сотрудник</h4>
                        <small>Сотрудники &mdash; это люди, которые могут быть выбраны в качестве исполнителей проекта</small>
                    </div>
                    <div class="modal-body">
                        <form method="get">
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Фамилия</label>
                                <div class="col-sm-10"><input type="text" class="form-control"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Имя</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Отчество</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Должность</label>
                                <div class="col-sm-10">
                                    <select id="position">
                                        <option value="0">Выберите должность</option>
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Контакт</label>
                                <div class="col-sm-10">
                                    <div class="inputHolder">
                                        <div class="input-group m-b">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Телефон" value="(916)345-6789">
                                        </div>
                                        <div class="input-group m-b">
                                            <div class="input-group-prepend"><span class="input-group-addon"><i class="fa fa-skype"></i></span></div>
                                            <input type="text" class="form-control" data-mask="(999) 999-9999" placeholder="Телефон" value="skype_login">
                                            <a class="input-group-addon text-danger deleteContact">
                                                <span class="fa fa-times"></span>
                                            </a>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary btn-sm contactAdd" data-inputMask="(999) 999-9999" data-placeholder="Телефон" type="button">+&nbsp;<i class="fa fa-phone"></i></button>
                                    <button class="btn btn-warning btn-sm contactAdd" data-placeholder="Почта" type="button">+&nbsp;<i class="fa fa-envelope"></i></button>
                                    <button class="btn btn-success btn-sm contactAdd" data-placeholder="Skype" type="button">+&nbsp;<i class="fa fa-skype"></i></button>
                                    <button class="btn btn-info btn-sm contactAdd" data-placeholder="Мессенджер" type="button">+&nbsp;<i class="fa fa-telegram"></i></button>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Ответственный</label>
                                <div class="col-sm-10">
                                    <select class="form-control col-sm-10 m-b" name="account" id="account_manager">
                                        <option value="0" selected="selected">Выберите ответственного сорудника</option>
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Комментарий</label>
                                <div class="col-sm-10"><textarea rows="3" class="form-control" placeholder="Резюме ответственного  сотрудника по теккущему запросу, дата первого изменения этого поля будет отдельно сохранена в БД"></textarea>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer" style="display:block !important">
                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                            </div>
                            <div class="col">
                                <button type="button" class="ladda-button btn btn-primary pull-right" data-style="zoom-in">Сохранить</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?=$footer_scripts;?>

        <script src="/js/plugins/select2/select2.full.min.js"></script>

        <script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
        <script>
        var positionFilterButtonText = '';
        var stuffListGrid = null;
        var positionSearchInput = null;
        var positionSearchButton = null;
        $(document).ready(function() {
            positionFilterButtonText = $('#positionFilterButton').text();
            positionSearchInput = $('#positionSearchInput');
            positionSearchButton = $('#positionSearchButton');

            $("select#position").select2({
                placeholder: "Выберите должность",
                allowClear: false,
                dropdownParent: $('#addStuff div.modal-body'),
                width: '100%' ,
                data:[
                <?php
                    foreach($positions as $position)
                    {
                        echo '{id:"'.$position['id'].'",text:"'.$position['position_name'].'"},';
                    } 
                ?>
                ]
            });
            

            stuffListGrid = $('.stuffList').isotope({
                itemSelector: '.col-lg-3',
                getSortData: {
                        stuffName: 'h3'
                },
            });
            // Searching by positions dictionary
            $('#postionFilterList a').click(function(e){
                //console.log($(this).text(), $(this).hasClass('showAll'));
                if($(this).hasClass('showAll'))
                {
                    $('#positionFilterButton').text(positionFilterButtonText);
                    stuffListGrid.isotope({ filter: '*' });
                }else
                {
                    $('#positionFilterButton').text($(this).text());
                    stuffListGrid.isotope({ filter: '.p'+$(this).data('positionId') });
                }
                e.preventDefault();
            });
            // Sorting order
            $('#postionSortingList a').click(function(e){
                
                $('#positionSortingButton').text($(this).text());
                stuffListGrid.isotope({
                    sortBy: 'stuffName',
                    sortAscending: $(this).data('sort-order')=='asc'
                });
                e.preventDefault();
            });
          
            positionSearchInput.keyup(function(e){
                if(e.keyCode == 13)
                {
                    positionSearchButton.trigger("click");
                }
            });
            positionSearchButton.click(function() {
                stuffListGrid.isotope({filter: function() {
                    return $(this).find('a.data').text().trim().indexOf(positionSearchInput.val()) !== -1;
              }
            })});

        });
        </script>
        
<?=$footer;?>