<?=$header;?>
<?=$left_menu;?>

        <div id="page-wrapper" class="gray-bg">
            <?=$head;?>
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h3>Договоры <small class="m-l-sm"> заключаются с конкретным заказчиком и имеют финансовый календарный план</small></h3>
                                <div class="ibox-tools">
                                    <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#addContract"><i class="fa fa-plus"></i>&nbsp;Новый договор&nbsp;<i class="fa fa-handshake-o"></i></button>
                                    <a class="fullscreen-link">
                                        <i class="fa fa-expand"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <table class="table table-striped table-hover border-bottom mt-3 display" id="contractsTable" style="width:100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="float-right">
                    10Гб из <strong>250Гб</strong> свободно.
                </div>
                <div>
                    <strong>Все права защищены</strong> ООО &laquo;Варди&raquo; &copy; 2020-2021
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="addContract" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                        <h4 class="modal-title">Новый договор</h4>
                    </div>
                    <div class="modal-body">
                        <form method="get">
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Заказчик</label>
                                <div class="col-sm-10"><input type="text" class="form-control" placeholder="Подгружаемый список Заказчиков"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Номер</label>
                                <div class="col-sm-4"><input type="text" class="form-control" placeholder="Автоподстановка инкрементного номера"></div>
                                <div class="col-sm-6"> <select class="contactsInModal form-control">
                                        <option value="" selected="yes">Выберите тип оплаты</option>
                                        <option value="new">Наличка</option>
                                        <option value="22">Безналичка</option>
                                    </select></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Сумма</label>
                                <div class="col-sm-5">
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon">руб.</span>
                                        </div>
                                        <input type="text" placeholder="" class="form-control">
                                        <span class="form-text m-b-none small">Вы должны распределить всю сумму по этапам</span>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="input-group date">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Дата договора">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="calendar-palnning">
                                <div class="form-group row">
                                    <div class="col-sm-2">
                                    </div>
                                    <div class="col-sm-10">
                                        <h4>Календарный план договора</h4>
                                    </div>
                                </div>
                                <div id="step_1" class="calendar-palnning-step first">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Этап №1</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" placeholder="Название этапа">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-2">
                                            <div class="btn-group" role="group" aria-label="actions">
                                                <button class="btn btn-info" type="button"><i class="fa fa-plus"></i></button>
                                                <button class="btn btn-danger" type="button" disabled><i class="fa fa-trash"></i></button>
                                            </div>
                                        </div>
                                        <div class="input-group m-b  date-range col-sm-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Начало этапа">
                                        </div>
                                        <div class="input-group m-b  date-range col-sm-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Окончание этапа">
                                        </div>
                                        <div class="input-group m-b col-sm-4">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon">руб.</span>
                                            </div>
                                            <input type="text" placeholder="сумма этапа" class="form-control">
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                </div>
                                <div id="step_2" class="calendar-palnning-step">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Этап №2</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" placeholder="Название этапа">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-2">
                                            <div class="btn-group" role="group" aria-label="actions">
                                                <button class="btn btn-info" type="button"><i class="fa fa-plus"></i></button>
                                                <button class="btn btn-danger" type="button"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </div>
                                        <div class="input-group m-b  date-range col-sm-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Начало этапа">
                                        </div>
                                        <div class="input-group m-b  date-range col-sm-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Окончание этапа">
                                        </div>
                                        <div class="input-group m-b col-sm-4">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon">руб.</span>
                                            </div>
                                            <input type="text" placeholder="сумма этапа" class="form-control">
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                </div>
                                <div id="step_3" class="calendar-palnning-step last">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Этап №3</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" placeholder="Название этапа">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-2">
                                            <div class="btn-group" role="group" aria-label="actions">
                                                <button class="btn btn-info" type="button"><i class="fa fa-plus"></i></button>
                                                <button class="btn btn-danger" type="button"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </div>
                                        <div class="input-group m-b  date-range col-sm-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Начало этапа">
                                        </div>
                                        <div class="input-group m-b  date-range col-sm-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Окончание этапа">
                                        </div>
                                        <div class="input-group m-b col-sm-4">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon">руб.</span>
                                            </div>
                                            <input type="text" placeholder="сумма этапа" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer" style="display:block !important">
                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                            </div>
                            <div class="col">
                                <button type="button" class="ladda-button btn btn-primary pull-right" data-style="zoom-in"><span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span></button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="buttonHolder" class="d-none">
            <div class="btn-group" role="group" aria-label="Редактировать/удалить договор">
                <button type="button" class="btn btn-sm btn-info" data-id="{ID}"><i class="fa fa-pencil"></i></button>
                <button type="button" class="btn btn-sm btn-danger" data-id="{ID}"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <div id="yearHolder" class="d-none">
            <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-outline btn-default dropdown-toggle">Год: 2020</button>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="#" class="font-bold">2020 <i class="fa fa-check text-success"></i></a></li>
                    <li><a class="dropdown-item" href="#">2019</a></li>
                    <li><a class="dropdown-item" href="#">2018</a></li>
                    <li class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="#">Показать все</a></li>
                </ul>
            </div>
        </div>
<?=$footer_scripts;?>

<script>
        var contactsTable = null;
        var s_actionButtons = null;
        $(document).ready(function() {
            $(document).on('preInit.dt', function(e, settings) {
                $('.yearsSelector').html($('#yearHolder').html());
                console.log('Init');
            });
            s_actionButtons = $('#buttonHolder').html();
            contactsTable = $('table#contractsTable').DataTable({
                language: { url: 'js/plugins/dataTables/Russian.json' },
                pageLength: 100,
                responsive: true,
                paging: true,
                searching: true,
                serverSide: false, // ТОЛЬКО ДЛЯ ТЕСТА, В ПРОДАКШ ОБРАБОТКА ДОЛЖНА БЫТЬ СЕРВЕРНАЯ
                ajax: {
                    url: 'server.php?action=contracts_list',
                    type: 'POST',
                    dataSrc: ''
                },
                columnDefs: [
                    { className: "align-middle", targets: "_all" },

                ],
                columns: [
                    { name: 'id', data: 0, title: '№', "visible": false },
                    {
                        name: 'date',
                        data: 2,
                        title: 'Заключён',
                        width: "60px",
                        render: function(data, type, row, meta) {
                            return type === 'display' || type === 'filter' ? getDateFromUNIX(data) : data;
                        }
                    },
                    { name: 'num', data: 1, title: '№' },
                    {
                        name: 'client',
                        data: 3,
                        title: 'Клиент',
                        //width: '70%',
                        render: function(data, type, row, meta) {
                            //console.log(data, type, row, meta);
                            return type === 'display' ? '<a href="?action=edit&id=' + row[0] + '">' + data + '</a>' : data;
                        }
                    },
                    // В наборе тестовых данных от сервера не приходит ФИО, потому просто создаем его случайно на клиенте
                    {
                        name: 'date_start',
                        data: 4,
                        title: 'Начало',
                        render: function(data, type, row, meta) {
                            return type === 'display' || type === 'filter' ? getDateFromUNIX(data) : data;
                        }
                    },
                    // В наборе тестовых данных от сервера не приходит ФИО, потому просто создаем его случайно на клиенте
                    // по хорошему сюда нужно отдавать JSON объект содержащий все контактные данные этого контакта, с указанием типа и маски ровно так как это хранится в форме ввода этих контаткных данных.
                    {
                        name: 'progress',
                        data: 5,
                        title: '<i class="fa fa-check text-info">&nbsp;</i>',
                        render: function(data, type, row, meta) {
                            out = '';
                            if (type === 'display') {
                                if ('' == data)
                                    return '<i class="fa fa-minus text-warning"></i>';
                                else
                                    return '<i class="fa fa-check text-info"></i>';
                            }
                            return data;
                        }
                    },
                    {
                        name: 'date_end',
                        data: 6,
                        title: 'Окончание',
                        render: function(data, type, row, meta) {
                            return type === 'display' || type === 'filter' ? getDateFromUNIX(data) : data;
                        }
                    },
                    {
                        name: "sum",
                        data: 4,
                        title: "Сумма, руб",
                        className: "align-middle text-right",
                        render: function(data, type, row, meta) {
                            return type === 'display' ? formatMoney(data, 0) : data;
                        }
                    },
                    {
                        name: "sum",
                        data: 4,
                        title: "Остаток, руб",
                        className: "align-middle text-right",
                        render: function(data, type, row, meta) {
                            min = 0;
                            max = row[4];

                            data = data - Math.random() * (max - min) + min;
                            return type === 'display' ? formatMoney(data, 0) : data;
                        }
                    },
                    {
                        name: "action",
                        data: 0,
                        title: "Действие",
                        width: '100px',
                        className: "align-middle text-right",
                        render: function(data, type, row, meta) {
                            return type === 'display' ? s_actionButtons.replaceAll('{ID}', row[0]) : '';
                        }
                    },
                ],
                order: [
                    [1, 'desc']
                ],
                dom: "<'row'<'col-md-4'f><'col-md-4 yearsSelector'><'col-md-4 text-right'i>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row mt-2'<'col-sm-12 col-md-6'p><'col-sm-12 col-md-6 text-right pull-right'l>>",
            });

            $('.calendar-palnning').on('click', 'button', function() {
                if ($(this).hasClass('btn-info')) {
                    thisStepObj = $(this).parent().parent().parent().parent();
                    newStep = thisStepObj.clone();
                    newStep.find()
                    newStep.find('input').val('');
                    newStep.insertAfter(thisStepObj);
                } else if ($(this).hasClass('btn-danger')) {
                    thisStepObj = $(this).parent().parent().parent().parent();
                    thisStepObj.remove();
                }

                $('.calendar-palnning-step').each(function(index) {
                    $(this).removeClass('first');
                    $(this).removeClass('last');
                    if (index == 0) {
                        $(this).find('button.btn-danger').prop('disabled', true);
                        $(this).addClass('first');
                    }
                    $(this).attr('id', 'step_' + (index + 1));
                    $(this).find('label').html('Этап №' + (index + 1));
                    $('#step_' + (index + 1)).datepicker({ language: "ru", inputs: $('.date-range input') });

                });
            });
            $.fn.datepicker.defaults.format = "dd.mm.yyyy";

            $('#addContract .date input').datepicker({ language: "ru", });
            $('.calendar-palnning-step').each(function() {
                $(this).datepicker({
                    language: "ru",
                    inputs: $('.date-range input')
                });
            });
            var action = getUrlParameter('action');
            if ('addNew' == action) $('#addContract').modal('show');
        });
        </script>
<?=$footer;?>