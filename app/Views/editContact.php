
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                        <h4 class="modal-title">Новый контакт</h4>
                        <small>Контакт - это свободная запись контактных данных с человеком, потенциальным Заказчиком</small>
                    </div>
                    <div class="modal-body">
                        <form method="POST" id="addClientForm" class="form_addcontacts">
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Клиент</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control required" name="client">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Номер</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control typeahead required" name="number" placeholder=""> 
                                    <span class="form-text m-b-none small "></span>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Выручка	В работе</label>
                                <div class="col-sm-5">
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon">руб.</span>
                                        </div>
                                        <input type="text" placeholder="" name="profit" class="form-control required">
                                        <span class="form-text m-b-none small">Вы должны распределить всю сумму по этапам</span>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="input-group date">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" class="form-control required" name="date" placeholder="Дата договора">
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Вид работ</label>
                                <div class="col-sm-10">
                                <? foreach ($works as $key => $value) {?>
                                    <label> 
                                        <input type="checkbox" class="check" name="type_work[]" value="<?=$value['name_work'];?>" id="inlineCheckbox<?=$value['id'];?> "> <?=$value['name_work'];?> 
                                    </label> 
                                    <br>
                                <?} ?>
                                </div>
                                <?/*
                                <div class="col-sm-10">
                                    <input type="text" class="form-control typeahead" name="type_work" placeholder=""> 
                                    <span class="form-text m-b-none small"></span>
                                </div>
                                */?>
                            </div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Тип оплаты</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control typeahead required" name="type_pay" placeholder=""> 
                                    <span class="form-text m-b-none small"></span>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer" style="display:block !important">
                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                            </div>
                            <div class="col">
                                <button type="button" id="saveContact" class="ladda-button btn btn-primary pull-right" data-style="zoom-in">Сохранить</button></div>
                        </div>
                    </div>
                </div>
            </div>