
    <div class="modal inmodal" id="addPartner" tabindex="-1" role="dialog" aria-modal="true">
        <div class="modal-dialog">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                    <h4 class="modal-title">Новый партнер</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="addNewPartnerFail" style="display:none">
                        Добавление новой должности не удалось.
                    </div>
                    <form method="get" id="addPartnerForm">
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Имя</label>
                            <div class="col-sm-10"><input type="text" name="partner_name" class="form-control" placeholder="Имя или название партнера"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer" style="display:block !important">
                    <div class="row">
                        <div class="col">
                         <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                        </div>
                        <div class="col">
                            <button type="button" class="ladda-button btn btn-primary pull-right" id="saveNewPartner" data-style="zoom-in"><span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span></button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#addPartner').modal('show');
        $('button#saveNewPartner').click(function() {
            t = $(this).html();
            b = $(this);
            $(this).prop("disabled", true).html('Обрабатываю...');
            $.post('/post/updatePartner', $("#addPartnerForm").serialize(), function() {}, "json")
                .done(function(data) {
                    console.log(data);
                    //partnersTable.row.add(data).draw();
                    $('#addPartner').modal('hide');
                })
                .fail(function() {
                    //o_addNewPartnerFail.show();
                })
                .always(function() {
                    b.prop("disabled", false).html(t);
                });
        });
    </script>