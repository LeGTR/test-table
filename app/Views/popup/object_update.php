
    <div class="modal inmodal" id="addObject" tabindex="-1" role="dialog" aria-modal="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                    <h4 class="modal-title">Новый Объект</h4>
                    <small>ОБъекты это адреса по которым ведется работа. Связаны с Заказчиками</small>
                </div>
                <div class="modal-body">
                    <form method="get" id="addObjectForm">
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Заказчик</label>
                            <div class="col-sm-10">
                                <select class="clientsInModal form-control" name="customer_id">
                                    <option></option>
                                    <option value="new">Новый заказчик</option>
                                    <option value="22">ЗАО &laquo;АРКС&raquo;</option>
                                    <option value="12">ИП Бахруничев Игорь Петрович</option>
                                    <option value="2">Сидоренко Анна Павловна</option>
                                    <option value="7" selected>Игнатов Ахмет аль Саалах Оглы</option>
                                    <option value="4">ООО &laquo;Застройщик&raquo;</option>
                                    <option value="8">Паша (сосед по даче)</option>
                                    <option value="17">ГУП &laquo;Водоканал&raquo;</option>
                                    <option value="32">Министрество промышленности и торговли Российской Федерации</option>
                                </select>
                                <span class="form-text m-b-none small">Если на предыдущей форме заказчик был выбран, то и в этом списке будет выбран тот же заказчик</span>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Краткое название</label>
                            <div class="col-sm-10"><input type="text" name="object_name" class="form-control typeahead" placeholder="Обязательное поле">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Адрес</label>
                            <div class="col-sm-10"><input type="text" name="object_address" class="form-control" placeholder="Используется autocomplete по API Яндекс.Карт">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Площадь</label>
                            <div class="col-sm-10">
                                <div class="input-group m-b">
                                    <input type="text" name="object_area" class="form-control" placeholder="Только число">
                                    <div class="input-group-append" >
                                        <span class="input-group-addon">м<sup>2</sup></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer" style="display:block !important">
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                        </div>
                        <div class="col">
                            <button type="button" id="saveObject" class="ladda-button btn btn-primary pull-right" data-style="zoom-in">Сохранить</button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#addObject').modal('show');
        $('#saveObject').click(function () { 
            $('.error').removeClass('error');

            var input = $('#addObjectForm').find('input,textarea,select');
            var error = false;
            for (let index = 0; index < input.length; index++) {
                const element = input[index];
                console.log( element );

                if( $(element).hasClass('required') && !$(element).val() ){
                    $(element).addClass('error');
                    error = true;
                }
            }
            if ( error ) {
                return false;
            }
            var l = $(this).ladda();
            l.ladda('start');

            var data = $('#addObjectForm').find('input,textarea,select').serialize();
            $.ajax({
                url: '/post/updateObject',
                type: 'post',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                },
                complete: function() {
                    l.ladda('stop');
                    $('#addObject').modal('hide');
                },
                success: function(json) {
                    //objectsTable.ajax.reload(null, false);
                },
                error: function(xhr, ajaxOptions, thrownError) {

                }
            });
        
        })
    </script>