<div>
    <div class="modal inmodal" id="addContact" role="dialog" aria-modal="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                    <h4 class="modal-title">Новый контакт</h4>
                    <small>Контакт - это свободная запись контактных данных с человеком, потенциальным Заказчиком</small>
                </div>
                <div class="modal-body">
                    <form method="get" id="addClientForm">
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Имя (ФИО)</label>
                            <div class="col-sm-10"><input type="text" name="name" class="form-control"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">От кого</label>
                            <div class="col-sm-10"> <select class="form-control col-sm-10 m-b" name="referrer" id="partner">
                                    <option value="0" selected="selected">Выберите партнера, от кого пришел контакт</option>
                                    <? foreach ($partners as $key => $value) {?>
                                        <option value="<?=$value['partners_id'];?>" ><?=$value['name'];?></option>
                                    <?} ?>
                                </select> <span class="form-text m-b-none small">Оставьте пустым, если не знаете от кого пришел клиент</span>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Адрес</label>
                            <div class="col-sm-10"><input type="text" name="address" class="form-control" placeholder="Используется autocomplete по API Яндекс.Карт">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Запрос</label>
                            <div class="col-sm-10"><input type="text" name="task" class="form-control" id="exampleFormControlTextarea1" /></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row" id="social_form">
                            <label class="col-sm-2 col-form-label">Контакт</label>
                            <div class="col-sm-10">
                                <div class="inputHolder">
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                        </div>
                                        <input type="text" class="form-control" name="contact" data-mask="(999) 999-9999" placeholder="Телефон" value="(916)345-6789">
                                    </div>
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend"><span class="input-group-addon"><i class="fa fa-skype"></i></span></div>
                                        <input type="text" class="form-control" name="social[][skype]" placeholder="Телефон" value="skype_login">
                                        <a class="input-group-addon text-danger deleteContact">
                                            <span class="fa fa-times"></span>
                                        </a>
                                    </div>
                                </div>
                                <button class="btn btn-primary btn-sm contactAdd" data-inputMask="(999) 999-9999" data-placeholder="Телефон" type="button">+&nbsp;<i class="fa fa-phone"></i></button>
                                <button class="btn btn-warning btn-sm contactAdd" data-placeholder="Почта" type="button">+&nbsp;<i class="fa fa-envelope"></i></button>
                                <button class="btn btn-success btn-sm contactAdd" data-placeholder="Skype" type="button">+&nbsp;<i class="fa fa-skype"></i></button>
                                <button class="btn btn-info btn-sm contactAdd" data-placeholder="Мессенджер" type="button">+&nbsp;<i class="fa fa-telegram"></i></button>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Ответственный</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-10 m-b" name="manager" id="account_manager">
                                    <option value="0" selected="selected">Выберите ответственного сорудника</option>
                                    <? foreach ($users as $key => $value) {?>
                                        <option value="<?=$value['user_id'];?>" ><?=$value['name'];?> <?=$value['lastname'];?> <?=$value['middlename'];?></option>
                                    <?} ?>
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Комментарий</label>
                            <div class="col-sm-10"><textarea rows="3" class="form-control" name="comment" placeholder="Резюме ответственного  сотрудника по теккущему запросу, дата первого изменения этого поля будет отдельно сохранена в БД"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer" style="display:block !important">
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-primary pull-right" data-target="#addClientF" id="saveContactСlientF" data-style="zoom-in">Сохранить (Физ лицо)</button>
                            <button type="button" class="btn btn-primary pull-right" data-target="#addClientU" id="saveContactСlientU" data-style="zoom-in">Сохранить (Юр лицо)</button>
                            <button type="button" class="btn btn-primary pull-right" id="saveContact" data-style="zoom-in">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#addContact').modal('show');

        $('#saveContact').click(function () { 
            $('.error').removeClass('error');

            var input = $('#addClientForm').find('input,textarea,select');
            var error = false;
            for (let index = 0; index < input.length; index++) {
                const element = input[index];
                //console.log( element );

                if( $(element).hasClass('required') && !$(element).val() ){
                    $(element).addClass('error');
                    error = true;
                }
            }
            if ( error ) {
                return false;
            }
            var l = $(this).ladda();
            l.ladda('start');

                var data = $('#addClientForm').find('input,textarea,select').serialize();
                $.ajax({
                    url: '/post/saveContact',
                    type: 'post',
                    data: data,
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    complete: function() {
                        l.ladda('stop');
                        $('#addContact').modal('hide').parent().remove();
                        
                        /*
                        swal({
                            title: "Успешно!",
                            text: "Новый клиент успешно сохранен",
                            type: "success"
                        });
                        */
                    },
                    success: function(json) {
                        //contactsTable.ajax.reload(null, false);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            
            /*
                // Start loading
                l.ladda('start');

                // Do something in backend and then stop ladda
                // setTimeout() is only for demo purpose
                setTimeout(function() {
                    l.ladda('stop');
                    $('#addContact').modal('hide');
                    swal({
                        title: "Успешно!",
                        text: "Новый клиент успешно сохранен",
                        type: "success"
                    });

                }, 500);
            */
         })
    </script>
</div>