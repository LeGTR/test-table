
    <div class="modal inmodal" id="editContact" role="dialog" aria-modal="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                    <h4 class="modal-title">Редактировать контакт</h4>
                    <small>Контакт - это свободная запись контактных данных с человеком, потенциальным Заказчиком</small>
                </div>
                <div class="modal-body">
                    <form method="get" id="editClientForm">
                    

                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Клиент ID</label>
                            <div class="col-sm-10"><input type="text" readonly name="client_id" value="<?=$info->client_id;?>" class="form-control"></div>
                        </div>

                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Имя (ФИО)</label>
                            <div class="col-sm-10"><input type="text" name="name" value="<?=$info->name;?>" class="form-control"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">От кого</label>
                            <div class="col-sm-10"> 
                                <select class="form-control col-sm-10 m-b" name="referrer" id="partner">
                                    <option value="0">Выберите партнера, от кого пришел контакт</option>
                                    <? foreach ($partners as $key => $value) {?>
                                        <? if( $value['partners_id'] == $info->referrer ){?>
                                            <option value="<?=$value['partners_id'];?>" selected="selected"><?=$value['name'];?></option>
                                        <?}else{?>
                                            <option value="<?=$value['partners_id'];?>" ><?=$value['name'];?></option>
                                        <?} ?>
                                    <?} ?>
                                </select> 
                                <span class="form-text m-b-none small">Оставьте пустым, если не знаете от кого пришел клиент</span>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Адрес</label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="address" value="<?=$info->address;?>" placeholder="Используется autocomplete по API Яндекс.Карт">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Запрос</label>
                            <div class="col-sm-10"><input type="text" name="task" value="<?=$info->task;?>" class="form-control" id="exampleFormControlTextarea1" /></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Контакт</label>
                            <div class="col-sm-10">
                                <div class="inputHolder">
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Телефон" name="contact" value="<?=$info->contact;?>">
                                    </div>
                                    
                                    <? if( isset($info->social) && !empty($info->social) ){?>
                                        <? $json = json_decode( $info->social ); 
                                        foreach ($json as $key => $value) {
                                            foreach ($value as $key2 => $value2) {?>
                                                <div class="input-group m-b">
                                                    <div class="input-group-prepend"><span class="input-group-addon"><i class="fa fa-skype"></i></span></div>
                                                    <input type="text" class="form-control" name="social[][<?=$key2;?>]" placeholder="Телефон" value="<?=$value2;?>">
                                                    <a class="input-group-addon text-danger deleteContact">
                                                        <span class="fa fa-times"></span>
                                                    </a>
                                                </div>
                                            <?} ?>
                                        <?} ?>
                                    <?} ?>

                                </div>
                                <button class="btn btn-primary btn-sm contactAdd" data-inputMask="(999) 999-9999" data-placeholder="Телефон" type="button">+&nbsp;<i class="fa fa-phone"></i></button>
                                <button class="btn btn-warning btn-sm contactAdd" data-placeholder="Почта" type="button">+&nbsp;<i class="fa fa-envelope"></i></button>
                                <button class="btn btn-success btn-sm contactAdd" data-placeholder="Skype" type="button">+&nbsp;<i class="fa fa-skype"></i></button>
                                <button class="btn btn-info btn-sm contactAdd" data-placeholder="Мессенджер" type="button">+&nbsp;<i class="fa fa-telegram"></i></button>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Ответственный</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-10 m-b" name="account" id="account_manager">
                                    <option value="0">Выберите ответственного сорудника</option>
                                    <? foreach ($users as $key => $value) {?>
                                        <? if( $value['user_id'] == $info->manager ){?>
                                            <option value="<?=$value['user_id'];?>" selected="selected"><?=$value['name'];?> <?=$value['lastname'];?> <?=$value['middlename'];?></option>
                                        <?}else{?>
                                            <option value="<?=$value['user_id'];?>" ><?=$value['name'];?> <?=$value['lastname'];?> <?=$value['middlename'];?></option>
                                        <?} ?>
                                    <?} ?>
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Комментарий</label>
                            <div class="col-sm-10">
                            <textarea rows="3" class="form-control" name="comment" placeholder="Резюме ответственного  сотрудника по теккущему запросу, дата первого изменения этого поля будет отдельно сохранена в БД"><?=$info->comment;?></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer" style="display:block !important">
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                        </div>
                        <div class="col">
                            <button type="button" class="ladda-button btn btn-primary pull-right" data-style="zoom-in">Сохранить</button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    $('#editContact').modal('show');
    $('#updateContact').click(function () { 
            $('.error').removeClass('error');

            var input = $('#editClientForm').find('input');
            var error = false;
            for (let index = 0; index < input.length; index++) {
                const element = input[index];

                if( $(element).hasClass('required') && !$(element).val() ){
                    $(element).addClass('error');
                    error = true;
                }
            }

            if (error) {
                return false;
            }
            var l = $(this).ladda();
            l.ladda('start');

            var data = $('#editClientForm').find('input').serialize();
            $.ajax({
                url: '/home/updateContact',
                type: 'post',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                },
                complete: function() {
                    contactsTable.ajax.reload(null, false);
                    l.ladda('stop');
                    $('#editContact').modal('hide');
                    /*
                    swal({
                        title: "Успешно!",
                        text: "Клиент обновлен",
                        type: "success"
                    });
                    */
                },
                success: function(json) {
                },
                error: function(xhr, ajaxOptions, thrownError) {

                }
            });
            
         })
</script>