
        <div class="modal inmodal" id="addContract" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                        <h4 class="modal-title">Новый договор</h4>
                    </div>
                    <div class="modal-body">
                        <form method="get" id="addContractForm">
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Заказчик</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="customer_id" id="clientSelect"></select>
                                </div>
                                <div class="col-sm-2">
                                    <div class="btn-group">
                                        <button data-toggle="dropdown" class="btn btn-primary btn-lg btn-block btn-outline dropdown-toggle"><i class="fa fa-plus"></i>&nbsp;Новый</button>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="#">Юридическое лицо</a></li>
                                            <li><a class="dropdown-item" href="#" class="font-bold">Физическое лицо</a></li>
                                        </ul>
                                    </div>
                                    <!--button class="btn btn-primary btn-lg btn-block btn-outline" type="button"><i class="fa fa-plus"></i>&nbsp;Заказчик</button-->
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Объект</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="object_id" id="objectSelect"></select>
                                </div>
                                <div class="col-sm-2">
                                    <button class="btn btn-primary btn-lg btn-block btn-outline" type="button"><i class="fa fa-plus"></i>&nbsp;Объект</button>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group  row">
                                <label class="col-sm-2 col-form-label">Номер</label>
                                <div class="col-sm-4">
                                    <input name="number" type="text" class="form-control" placeholder="Автоподстановка инкрементного номера">
                                </div>
                                <div class="col-sm-6"> 
                                    <select name="type_payment" class="contactsInModal form-control">
                                        <option value="" selected="yes">Выберите тип оплаты</option>
                                        <option value="0">Наличка</option>
                                        <option value="1">Безналичка</option>
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Сумма</label>
                                <div class="col-sm-5">
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon"><i class="fa fa-rouble"></i></span>
                                        </div>
                                        <input type="text" name="payment" placeholder="" class="form-control">
                                        <span class="form-text m-b-none small">Вы должны распределить всю сумму по этапам</span>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="input-group date">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="contract_date" class="form-control" value="<?php echo date('d.m.Y'); ?>" placeholder="Дата договора">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="calendar-palnning">
                                <div class="form-group row">
                                    <div class="col-sm-2">
                                    </div>
                                    <div class="col-sm-10">
                                        <h4>Календарный план договора</h4>
                                    </div>
                                </div>
                                <div id="step_1" class="calendar-palnning-step first">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label step">Этап №1</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="plan[stage_name][]" class="form-control" placeholder="Название этапа">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-2">
                                            <div class="btn-group" role="group" aria-label="actions">
                                                <button class="btn btn-info" type="button"><i class="fa fa-plus"></i></button>
                                                <button class="btn btn-danger" type="button" disabled=""><i class="fa fa-trash"></i></button>
                                            </div>
                                        </div>
                                        <div class="input-group date-range col-sm-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" name="plan[stage_start][]" class="form-control" placeholder="Начало этапа">
                                        </div>
                                        <div class="input-group date-range col-sm-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" name="plan[stage_end][]" class="form-control" placeholder="Окончание этапа">
                                        </div>
                                        <div class="input-group col-sm-4">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon"><i class="fa fa-rouble"></i></span>
                                            </div>
                                            <input type="text" name="plan[stage_summ][]" placeholder="сумма этапа" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Комментарий</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="plan[comment][]" class="form-control" placeholder="Будет виден только вам">
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer" style="display:block !important">
                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                            </div>
                            <div class="col">
                                <button type="button" id="saveContract" class="ladda-button btn btn-primary pull-right" data-style="zoom-in"><span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span></button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $('#addContract').modal('show');
            $('#saveContract').click(function () { 
                $('.error').removeClass('error');

                var input = $('#addContractForm').find('input,textarea,select');
                var error = false;
                for (let index = 0; index < input.length; index++) {
                    const element = input[index];
                    console.log( element );

                    if( $(element).hasClass('required') && !$(element).val() ){
                        $(element).addClass('error');
                        error = true;
                    }
                }
                if ( error ) {
                    return false;
                }
                var l = $(this).ladda();
                l.ladda('start');

                var data = $('#addContractForm').find('input,textarea,select').serialize();
                $.ajax({
                    url: '/post/saveContract',
                    type: 'post',
                    data: data,
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    complete: function() {
                        l.ladda('stop');
                        $('#addContract').modal('hide');
                        /*
                        swal({
                            title: "Успешно!",
                            text: "Новый контракт успешно сохранен",
                            type: "success"
                        });
                        */
                    },
                    success: function(json) {
                        contactsTable.ajax.reload(null, false);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            
            })
        </script>