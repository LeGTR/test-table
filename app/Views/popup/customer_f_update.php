
    <div class="modal inmodal" id="updateClientF" tabindex="-1" role="dialog" aria-modal="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                    <h4 class="modal-title">Новый ЗАКАЗЧИК - ФИЗИЧЕСКОЕ ЛИЦО</h4>
                    <small>Заказчик физическое лицо, идентифицируйтся в системе по паспорту</small>
                </div>
                <div class="modal-body">
                    <form method="get" id="updateClientForm">
                        <input type="hidden" name="client_id" value="0">
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Фамилия</label>
                            <div class="col-sm-10"><input type="text" name="lastname" class="form-control"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Имя</label>
                            <div class="col-sm-10"><input type="text" name="firstname" class="form-control"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Отчество</label>
                            <div class="col-sm-10"><input type="text" name="middlename" class="form-control"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Паспорт</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="passport_series" placeholder="серия">
                            </div>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="passport_number" placeholder="номер">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Контакт</label>
                            <div class="col-sm-10">
                                <div class="inputHolder">
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                        </div>
                                        <input type="text" class="form-control" name="contact" data-mask="(999) 999-9999" placeholder="Телефон" value="(916)345-6789">
                                    </div>
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend"><span class="input-group-addon"><i class="fa fa-skype"></i></span></div>
                                        <input type="text" class="form-control" name="social[]" placeholder="Телефон" value="skype_login">
                                        <a class="input-group-addon text-danger deleteContact">
                                            <span class="fa fa-times"></span>
                                        </a>
                                    </div>
                                </div>
                                <button class="btn btn-primary btn-sm contactAdd" data-inputmask="(999) 999-9999" data-placeholder="Телефон" type="button">+&nbsp;<i class="fa fa-phone"></i></button>
                                <button class="btn btn-warning btn-sm contactAdd" data-placeholder="Почта" type="button">+&nbsp;<i class="fa fa-envelope"></i></button>
                                <button class="btn btn-success btn-sm contactAdd" data-placeholder="Skype" type="button">+&nbsp;<i class="fa fa-skype"></i></button>
                                <button class="btn btn-info btn-sm contactAdd" data-placeholder="Мессенджер" type="button">+&nbsp;<i class="fa fa-telegram"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer" style="display:block !important">
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-white pull-left" data-dismiss="modal">Отменить</button>
                        </div>
                        <div class="col">
                            <button type="button" id="saveContactF_next" class="ladda-button btn btn-success pull-right" data-style="zoom-in">Сохранить и новый договор</button>
                            <button type="button" id="saveContactF" class="ladda-button btn btn-primary pull-right mr-1" data-style="zoom-in">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <script>
        $('#addContact').modal('show');
        $('#saveContactF').click(function () { 
            $('.error').removeClass('error');

            var input = $('#addClientForm').find('input,textarea,select');
            var error = false;
            for (let index = 0; index < input.length; index++) {
                const element = input[index];
                console.log( element );

                if( $(element).hasClass('required') && !$(element).val() ){
                    $(element).addClass('error');
                    error = true;
                }
            }
            if ( error ) {
                return false;
            }
            var l = $(this).ladda();
            l.ladda('start');

            var data = $('#addClientForm').find('input,textarea,select').serialize();
            $.ajax({
                url: '/post/saveCustomer',
                type: 'post',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                },
                complete: function() {
                    l.ladda('stop');
                    $('#addContact').modal('hide');
                    /*
                    swal({
                        title: "Успешно!",
                        text: "Новый клиент успешно сохранен",
                        type: "success"
                    });
                    */
                },
                success: function(json) {
                    clientsTable.ajax.reload(null, false);
                    l.ladda('stop');
                    $('#addClientF').modal('hide');
                    /*
                    swal({
                        title: "Успешно!",
                        text: "Клиент обновлен",
                        type: "success"
                    });
                    */
                },
                error: function(xhr, ajaxOptions, thrownError) {

                }
            });
            
        })
    </script>