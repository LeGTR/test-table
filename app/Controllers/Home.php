<?php 
namespace App\Controllers;
//namespace App\Controllers\Helloworld;

use App\Models\NewsModel;
use CodeIgniter\Controller;
use App\Controllers\Common;

use Config\Email;
use Config\Services;

use App\Http\Controllers\Base\MasterController;

class Home extends BaseController{
	/**
	 * Access to current session.
	 *
	 * @var \CodeIgniter\Session\Session
	 */
	protected $session;

	/**
	 * Authentication settings.
	 */
	protected $config;
	/**
	 * 
	 */
	protected $security;

	public $model = '';
	public $request = '';
	public $pager = '';
	public $limit = 20;
	public $common;
	public $_data;
	
	public function __construct(){
		// start session
		$this->session = Services::session();

		// load auth settings
		$this->config = config('Auth');

		$this->model = new NewsModel();
		$this->request = \Config\Services::request();
		$this->pager = \Config\Services::pager();
		$this->common = new Common();
		$this->_data['header'] = $this->common->header();
		$this->_data['head'] = $this->common->head();
		$this->_data['footer'] = $this->common->footer();
		$this->_data['footer_scripts'] = $this->common->footer_scripts();
		$this->_data['left_menu'] = $this->common->left_menu();
	}

	public function index(){

		$data = $this->_data;
		return view('home', $data);
	}


	//--------------------------------------------------------------------

}
