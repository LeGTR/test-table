<?php 
namespace App\Controllers;
//namespace App\Controllers\Helloworld;

use App\Models\NewsModel;
use CodeIgniter\Controller;
use App\Controllers\Common;

use Config\Email;
use Config\Services;

use App\Http\Controllers\Base\MasterController;

class Contacts extends BaseController{
	/**
	 * Access to current session.
	 *
	 * @var \CodeIgniter\Session\Session
	 */
	protected $session;

	/**
	 * Authentication settings.
	 */
	protected $config;
	/**
	 * 
	 */
	protected $security;

	public $model = '';
	public $request = '';
	public $pager = '';
	public $limit = 20;
	public $common;
	public $_data;
	
	public function __construct(){
		// start session
		$this->session = Services::session();

		// load auth settings
		$this->config = config('Auth');

		$this->model = new NewsModel();
		$this->request = \Config\Services::request();
		$this->pager = \Config\Services::pager();
		$this->common = new Common();
		$this->_data['header'] = $this->common->header();
		$this->_data['head'] = $this->common->head();
		$this->_data['footer'] = $this->common->footer();
		$this->_data['footer_scripts'] = $this->common->footer_scripts();
		$this->_data['left_menu'] = $this->common->left_menu();
	}

	public function index(){
		
		$data = $this->_data;
		return view('contacts', $data);
	}
	

	public function MyDataTables(){
		/*
		$reponse = array(
			'csrfName' => $this->security->get_csrf_token_name(),
			'csrfHash' => $this->security->get_csrf_hash()
		);
		*/

		$aColumns = array('user_id','username','address');
		$sTable = 'user';
		$data = array();

		$iDisplayStart = $this->request->getPost('start');
		$iDisplayLength = $this->request->getPost('length');
		$iSortCol_0 = $this->request->getPost('iSortCol_0');
		$iSortingCols = $this->request->getPost('iSortingCols');
		$sSearch = $this->request->getPost('search');
		$sEcho = $this->request->getPost('sEcho');

		$columns = $this->request->getPost('columns');
		if( isset( $columns[6]['search']['value'] ) && !empty( $columns[6]['search']['value'] ) ){
			$data['type_work'] = explode(',',$columns[6]['search']['value']);
		}

		//order[0][column]: 0
		//order[0][dir]: asc
		$order = $this->request->getPost('order');
		$order_name[] = 'id';
		$order_name[] = 'client';
		$order_name[] = 'number';
		$order_name[] = 'date';
		$order_name[] = 'profit';
		$order_name[] = 'work_status';
		$order_name[] = 'type_work';
		$order_name[] = 'type_pay';
		$order_name[] = 'date_added';
		$order_name[] = 'status';
		$order_name[] = 'action';

		if( $order ){
			$data['sort']['column'] = $order_name[ $order[0]['column'] ];
			$data['sort']['dir'] = $order[0]['dir'];
		}


		$data['search'] = $sSearch['value'];

		// Paging
		if(isset($iDisplayStart) && $iDisplayLength != '-1'){
			//$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
		}

		// Ordering
		if(isset($iSortCol_0))
		{
			for($i=0; $i<intval($iSortingCols); $i++)
			{
				$iSortCol = $this->request->getPost('iSortCol_'.$i, true);
				$bSortable = $this->request->getPost('bSortable_'.intval($iSortCol), true);
				$sSortDir = $this->request->getPost('sSortDir_'.$i, true);

				if($bSortable == 'true')
				{
					//$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
				}
			}
		}

		/* 
			* Filtering
			* NOTE this does not match the built-in DataTables filtering which does it
			* word by word on any field. It's possible to do here, but concerned about efficiency
			* on very large tables, and MySQL's regex functionality is very limited
			*/
		if(isset($sSearch) && !empty($sSearch)){
			for($i=0; $i<count($aColumns); $i++){
				$bSearchable = $this->request->getPost('bSearchable_'.$i, true);

				// Individual column filtering
				if(isset($bSearchable) && $bSearchable == 'true'){
					//$this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
				}
			}
		}

		// Select Data


		//$this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);               
		//$rResult = $this->db->get($sTable);

		// Data set length after filtering
		//$this->db->select('FOUND_ROWS() AS found_rows');
		//$iFilteredTotal = $this->db->get()->row()->found_rows;

		// Total data set length
		$iTotal = $this->model->getItemsTotal($data);
		$iTotalAll = $this->model->getItemsTotal();

		// Output
		$output = array(
			'sEcho' => intval($sEcho),
			'iTotalRecords' => $iTotalAll,
			'iTotalDisplayRecords' => $iTotal,
			'aaData' => array()
		);

		/*
		foreach($rResult->result_array() as $aRow){
			$row = array();

			foreach($aColumns as $col)
			{
					$row[] = $aRow['user_id'];
					$row[] = $aRow['username'];
					$row[] = $aRow['address'];
			}

			$output['aaData'][] = $row;
		}
		*/
		
		$_json = $this->model->getItems($data, $iDisplayLength, $iDisplayStart);
		if( $_json ){
			foreach ($_json as $key => $value) {
				$row = array();
				foreach ($value as $key2 => $value2) {
					if( $key2 == 'work_status' ){
						if( $value2 == 1 ){
							$row[] = '<i class="fa fa-check text-info"></i>';
						}else{
							$row[] = '<i class="fa fa-minus text-warning"></i>';
						}
					}else{
						$row[] = $value2;
					}
					
					
				}
				
				$row[] = '<button class="btn btn-primary btn-sm popup_edit" type="button" data-toggle="modal" data-target="#editContact" var_id="'. $value['id'] .'"><i class="fa fa-pencil"></i></button>
				<button class="btn btn-danger btn-sm popup_delete" type="button" data-toggle="modal" data-target="#deleteContact" var_id="'. $value['id'] .'"><i class="fa fa-times"></i></button>';
				$output['aaData'][] = $row;
			}
		}

		echo json_encode($output);

    }

	
	public function not_tablejs(){
		$data = $this->request->getGet();

		$url = array();
		$url['type_1'] = site_url('contacts/not_tablejs?type_work=1');
		$url['type_2'] = site_url('contacts/not_tablejs?type_work=2');
		$url['type_3'] = site_url('contacts/not_tablejs?type_work=3');

		$json['works'] = $this->model->getWorks();
		$json['json'] = array();
		$json['url'] = $url;
		$json['get'] = '';
		$json['contacts_url'] = site_url('contacts/not_tablejs');
		$_url = '';
		
		if( isset( $data['type_work'] ) ){
			$json['get'] = $data['type_work'];
			$json['s'] = '&';
			if( is_array( $data['type_work'] ) ){
				
				$type_work = array();
				foreach ($data['type_work'] as $key => $value) {
					$type_work[] = 'type_work[]='.$value;
				}
				$_url = '?' . implode('&',$type_work);
			}else{
				$_url = '?type_work='.$data['type_work'];
			}
			
		}else{
			$json['s'] = '?';
		}
		


		$json['search'] = '';
		if( isset( $data['search'] ) ){
			$json['search'] = $data['search'];
			if( $_url ){
				$_url .= '&search='.$data['search'];
			}else{
				$_url .= '?search='.$data['search'];
			}
		}

		$json['_url'] = $_url;
		
		if( isset( $data['page'] ) ){
			$page = $data['page'];
			if( $_url ){
				$_url .= '&page='.$data['page'];
			}else{
				$_url .= '?page='.$data['page'];
			}
		}else{
			$page = 0;
		}

		if( isset( $data['sort'] )  ){
			$_sort = $data['sort'];
			$json['order'] = $data['sort'];
			$json['_url'] .= '&sort='.$data['sort'];
			$json['_url'] .= '&order='.$data['order'];
		}else{
			$_sort = '';
		}

		if( isset( $data['order'] ) && $data['order'] == 'ASC' ){
			$_order = 'DESC';
		}else{
			$_order = 'ASC';
		}

		$sort = array(
			'id' => $_sort == 'id' ? $_order : 'ASC', 
			'client' => $_sort == 'client' ? $_order : 'ASC', 
			'number' => $_sort == 'number' ? $_order : 'ASC', 
			'date' => $_sort == 'date' ? $_order : 'ASC', 
			'profit' => $_sort == 'profit' ? $_order : 'ASC', 
			'work_status' => $_sort == 'work_status' ? $_order : 'ASC', 
			'type_work' => $_sort == 'type_work' ? $_order : 'ASC', 
			'type_pay' => $_sort == 'type_pay' ? $_order : 'ASC', 
			'date_added' => $_sort == 'date_added' ? $_order : 'ASC', 
			'status' => $_sort == 'status' ? $_order : 'ASC'
		);

		$json['sort'] = array();

		foreach ($sort as $key => $value) {
			if( $_url ){
				$json['sort'][$key] = array(
					'url' =>  $json['contacts_url'] . $_url . '&sort='.$key.'&order='.$value,
					'order' => $value
				);
			}else{
				$json['sort'][$key] = array(
					'url' => $json['contacts_url'] . '?sort='.$key.'&order='.$value,
					'order' => $value
				);
			}
		}


		if( $page > 0 ){
			$start = ( $page - 1 ) * $this->limit;
			$next = $page + 1;
		}else{
			$start = 0;
			$next = 2;
		}
		

		$_json = $this->model->getItems($data, $this->limit, $start );
		$_json_total = $this->model->getItemsTotal($data);
		$json['total'] = $_json_total;
		if( $start == 0 ){
			$json['start'] = 1;
		}else{
			$json['start'] = $start + 1;
		}
		
		$json['end'] = $start + $this->limit;
		$json['pagination'] = $this->pager->makeLinks($page, $this->limit, $_json_total);

		
		if( $json['_url'] ){
			$json['next'] = site_url('contacts/load_table') . $json['_url'] . '&page='.$next;
		}else{
			$json['next'] = site_url('contacts/load_table') . '?page='.$next;
		}
		
		if( $_json ){
			foreach ($_json as $key => $value) {
				$json['json'][$key] = $value;
				$json['json'][$key]['action'] = '<button class="btn btn-primary btn-sm popup_edit" type="button" data-toggle="modal" data-target="#editContact" var_id="'. $value['id'] .'">Редактировать</button>
				<button class="btn btn-danger btn-sm popup_delete" type="button" data-toggle="modal" data-target="#deleteContact" var_id="'. $value['id'] .'">Удалить</button>';
			}
		}
		return view('table_not_js', $json);
	}

	public function load_table(Type $var = null){
		$data = $this->request->getGet();
		
		if( isset( $data['page'] ) ){
			$page = $data['page'];
		}else{
			$page = 0;
		}
		if( $page > 0 ){
			$start = ( $page - 1 ) * $this->limit;
		}else{
			$start = 0;
		}
		

		$json['contacts_url'] = site_url('contacts/load_table');
		$_url = '';
		
		if( isset( $data['type_work'] ) ){
			$json['get'] = $data['type_work'];
			$json['s'] = '&';
			if( is_array( $data['type_work'] ) ){
				
				$type_work = array();
				foreach ($data['type_work'] as $key => $value) {
					$type_work[] = 'type_work[]='.$value;
				}
				$_url = '?' . implode('&',$type_work);
			}else{
				$_url = '?type_work='.$data['type_work'];
			}
			
		}else{
			$json['s'] = '?';
		}
		
		$json['search'] = '';
		
		if( isset( $data['search'] ) ){
			$json['search'] = $data['search'];
			if( $_url ){
				$_url .= '&search='.$data['search'];
			}else{
				$_url .= '?search='.$data['search'];
			}
		}
		$json['_url'] = $_url;

		if( isset( $data['sort'] )  ){
			$json['_url'] .= '&sort='.$data['sort'];
			$json['_url'] .= '&order='.$data['order'];
		}

		if( isset( $data['order'] ) && $data['order'] == 'ASC' ){
			$_order = 'DESC';
		}else{
			$_order = 'ASC';
		}


		if( $json['_url'] ){
			$json['next'] = $json['contacts_url'] . $json['_url'] . '&page='.( $page + 1 );
		}else{
			$json['next'] = $json['contacts_url'] . '?page='.( $page + 1 );
		}

		$_json = $this->model->getItems($data, $this->limit, $start );
		$_json_total = $this->model->getItemsTotal($data);
		$pagination = $this->pager->makeLinks($page, $this->limit, $_json_total);
		if( $_json ){
			foreach ($_json as $key => $value) {
				$json['json'][$key] = $value;
				$json['json'][$key]['action'] = '<button class="btn btn-primary btn-sm popup_edit" type="button" data-toggle="modal" data-target="#editContact" var_id="'. $value['id'] .'">Редактировать</button>
				<button class="btn btn-danger btn-sm popup_delete" type="button" data-toggle="modal" data-target="#deleteContact" var_id="'. $value['id'] .'">Удалить</button>';
			}
		}
		$html = view('table_load', $json);
		$next_url = $json['next'];

		echo json_encode(
			array(
				'html' => $html,
				'next_url' => $next_url,
				'pagination' => $pagination
			)
		);
	}

	public function selectContactForm($id){
		$_json = $this->model->getItem($id);
		//17.12.2020
		$_json->date = date('d.m.Y', strtotime($_json->date) );
		$_json->type_work_array = $this->model->getTypeWork( $id );
		echo json_encode( $_json );
	}

	public function saveContact(){
		$_post = $this->request->getPost();
		if( $_post ){
			//Array ( [client] => test [number] => test [profit] => test [date] => 16.12.2020 [type_work] => Array ( [0] => option1 ) [type_pay] => test )
			$data = $_post;
			$data['date'] = date('Y-m-d', strtotime( $_post['date'] ) );
			$data['date_added'] = date( 'Y-m-d H:i:s' );
			$data['status'] = 1;
			if ( isset($_post['type_work']) ) {
				$data['type_work'] = implode(' ', $_post['type_work']);
			}
			$id = $this->model->saveContact($data);
			if ( isset($_post['type_work']) ) {
				foreach ($_post['type_work'] as $key => $value) {
					$this->model->saveTypeWork($id, $value);
				}
			}
			echo $id;
		}
	}

	public function updateContact(Type $var = null){
		$_post = $this->request->getPost();
		if( $_post ){
			//Array ( [client] => test [number] => test [profit] => test [date] => 16.12.2020 [type_work] => Array ( [0] => option1 ) [type_pay] => test )
			$data = $_post;
			$id = $data['id'];
			unset($data['id']);
			$data['status'] = 1;
			$data['date'] = date('Y-m-d', strtotime( $data['date'] ) );
			$data['date_added'] = date( 'Y-m-d H:i:s' );
			if ( isset($data['type_work']) ) {
				$data['type_work'] = implode(' ', $data['type_work']);
			}
			$this->model->updateContact($id,$data);
			if ( isset($_post['type_work']) ) {
				foreach ($_post['type_work'] as $key => $value) {
					$this->model->saveTypeWork($id, $value);
				}
			}
			
		}
	}

	public function deleteContact($id = ''){
		if( $id ){
			$this->model->deleteContact($id);
		}
		
	}
	

	//--------------------------------------------------------------------

}
