<?php 
namespace App\Controllers;
//namespace App\Controllers\Helloworld;

use App\Models\NewsModel;
use CodeIgniter\Controller;
use App\Controllers\Common;

use Config\Email;
use Config\Services;

use App\Http\Controllers\Base\MasterController;

class Popup extends BaseController{
	/**
	 * Access to current session.
	 *
	 * @var \CodeIgniter\Session\Session
	 */
	protected $session;

	/**
	 * Authentication settings.
	 */
	protected $config;
	/**
	 * 
	 */
	protected $security;

	public $model = '';
	public $request = '';
	public $pager = '';
	public $limit = 20;
	
	public function __construct(){
		// start session
		$this->session = Services::session();

		// load auth settings
		$this->config = config('Auth');

		$this->model = new NewsModel();
		$this->request = \Config\Services::request();
        $this->pager = \Config\Services::pager();
        
	}

	public function index(){
        
    }
    
    public function view(){
		$post = $this->request->getPost('target');
		$id = 0;
		$id = $this->request->getPost('id');
		switch ($post) {
			case '#addContact':
				$this->addContact();
			break;

			case '#addClientF':
				$this->addClientF($id);
			break;

			case '#addClientU':
				$this->addClientU($id);
			break;

			case '#editContact':
				$this->editContact($id);
			break;

			case '#addContract':
				$this->addContract();
			break;

			case '#addObject':
				$this->addObject();
			break;

			case '#addPartner':
				$this->addPartner();
			break;

			default:
				# code...
			break;
		}
	}

	public function addContact(){
		$data = array();
		$data['partners'] = $this->model->getPartners();
		$data['users'] = $this->model->selectUsers();
		echo view('popup/contacts_add', $data);
	}

	#editContact
	public function editContact($id){
		$data = array();
		if( !empty( $id ) ){
			$data['info'] = $this->model->getItemClients($id);
		}
		$data['partners'] = $this->model->getPartners();
		$data['users'] = $this->model->selectUsers();
		echo view('popup/contacts_edit', $data);
	}


	public function addClientF($id=0){
		$data = array();
		if( !empty( $id ) ){
			$data['info'] = $this->model->getItemClients($id);
		}
		$data['partners'] = $this->model->getPartners();
		$data['users'] = $this->model->selectUsers();
		echo view('popup/customer_f_add', $data);
	}
	public function addClientU($id=0){
		$data = array();
		if( !empty( $id ) ){
			$data['info'] = $this->model->getItemClients($id);
		}
		$data['partners'] = $this->model->getPartners();
		$data['users'] = $this->model->selectUsers();
		echo view('popup/customer_u_add', $data);
	}

	public function addContract(){
		$data = array();
		$data['partners'] = $this->model->getPartners();
		$data['users'] = $this->model->selectUsers();
		echo view('popup/contract_add', $data);
	}
	public function addObject(){
		$data = array();
		$data['partners'] = $this->model->getPartners();
		$data['users'] = $this->model->selectUsers();
		echo view('popup/object_add', $data);
	}
	public function addPartner(){
		$data = array();
		$data['partners'] = $this->model->getPartners();
		$data['users'] = $this->model->selectUsers();
		echo view('popup/partner_add', $data);
	}
	

	public function editContract($id=0){
		$data = array();
		if( !empty( $id ) ){
			$data['info'] = $this->model->getContract($id);
		}
		$data['partners'] = $this->model->getPartners();
		$data['users'] = $this->model->selectUsers();
		echo view('popup/contract_add', $data);
	}
	public function editObject($id=0){
		$data = array();
		if( !empty( $id ) ){
			$data['info'] = $this->model->getObject($id);
		}
		$data['partners'] = $this->model->getPartners();
		$data['users'] = $this->model->selectUsers();
		echo view('popup/object_add', $data);
	}
	public function getPartner($id=0){
		$data = array();
		if( !empty( $id ) ){
			$data['info'] = $this->model->getItemClients($id);
		}
		$data['partners'] = $this->model->getPartners();
		$data['users'] = $this->model->selectUsers();
		echo view('popup/partner_add', $data);
	}


	//--------------------------------------------------------------------

}
