<?php 
namespace App\Controllers;
//namespace App\Controllers\Helloworld;

use App\Models\NewsModel;
use CodeIgniter\Controller;
use App\Controllers\Common;

use Config\Email;
use Config\Services;

use App\Http\Controllers\Base\MasterController;

class Project extends BaseController{
	/**
	 * Access to current session.
	 *
	 * @var \CodeIgniter\Session\Session
	 */
	protected $session;

	/**
	 * Authentication settings.
	 */
	protected $config;
	/**
	 * 
	 */
	protected $security;

	public $model = '';
	public $request = '';
	public $pager = '';
	public $limit = 20;
	public $common;
	public $_data;
	
	public function __construct(){
		// start session
		$this->session = Services::session();

		// load auth settings
		$this->config = config('Auth');

		$this->model = new NewsModel();
		$this->request = \Config\Services::request();
		$this->pager = \Config\Services::pager();
		$this->common = new Common();
		$this->_data['header'] = $this->common->header();
		$this->_data['head'] = $this->common->head();
		$this->_data['footer'] = $this->common->footer();
		$this->_data['footer_scripts'] = $this->common->footer_scripts();
		$this->_data['left_menu'] = $this->common->left_menu();
	}

	public function index($type=''){

		$data = $this->_data;
		return view('home', $data);
    }
    
	public function added($type=''){

		$data = $this->_data;
		return view('added_project', $data);
    }
    
	public function designing($type=''){

		$data = $this->_data;
        $data['ajax'] = site_url("project/MyDataTables/designing");
        $data['ajax'] = '/server.php?action=projects_planning_list';
		return view('designing', $data);
    }

	public function MyDataTables($type=''){
		/*
		$reponse = array(
			'csrfName' => $this->security->get_csrf_token_name(),
			'csrfHash' => $this->security->get_csrf_hash()
		);
		*/

		$aColumns = array('user_id','username','address');
		$sTable = 'user';
		$data = array();

		$iDisplayStart = $this->request->getPost('start');
		$iDisplayLength = $this->request->getPost('length');
		$iSortCol_0 = $this->request->getPost('iSortCol_0');
		$iSortingCols = $this->request->getPost('iSortingCols');
		$sSearch = $this->request->getPost('search');
		$sEcho = $this->request->getPost('sEcho');

		$columns = $this->request->getPost('columns');
		if( isset( $columns[6]['search']['value'] ) && !empty( $columns[6]['search']['value'] ) ){
			$data['type_work'] = explode(',',$columns[6]['search']['value']);
		}

		//order[0][column]: 0
		//order[0][dir]: asc
		$order = $this->request->getPost('order');
		$order_name[] = 'object';
		$order_name[] = 'manager';
		$order_name[] = 'date_end';
		$order_name[] = 'client';
		$order_name[] = 'finalize';
		$order_name[] = 'works';
		$order_name[] = 'status';

		if( $order ){
			$data['sort']['column'] = $order_name[ $order[0]['column'] ];
			$data['sort']['dir'] = $order[0]['dir'];
		}


		$data['search'] = $sSearch['value'];

		// Paging
		if(isset($iDisplayStart) && $iDisplayLength != '-1'){
			//$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
		}

		// Ordering
		if(isset($iSortCol_0))
		{
			for($i=0; $i<intval($iSortingCols); $i++)
			{
				$iSortCol = $this->request->getPost('iSortCol_'.$i, true);
				$bSortable = $this->request->getPost('bSortable_'.intval($iSortCol), true);
				$sSortDir = $this->request->getPost('sSortDir_'.$i, true);

				if($bSortable == 'true')
				{
					//$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
				}
			}
		}


		// Total data set length
		$iTotal = $this->model->getItemsTotal($data);
		$iTotalAll = $this->model->getItemsTotal();

		// Output
		$output = array(
			'sEcho' => intval($sEcho),
			'iTotalRecords' => $iTotalAll,
			'iTotalDisplayRecords' => $iTotal,
			'aaData' => array()
		);

		
		$_json = $this->model->getItems($data, $iDisplayLength, $iDisplayStart);
		if( $_json ){
			foreach ($_json as $key => $value) {
				$row = array();
				foreach ($value as $key2 => $value2) {
					if( $key2 == 'work_status' ){
						if( $value2 == 1 ){
							$row[] = '<i class="fa fa-check text-info"></i>';
						}else{
							$row[] = '<i class="fa fa-minus text-warning"></i>';
						}
					}else{
						$row[] = $value2;
					}
					
					
				}
				
				$row[] = '<button class="btn btn-primary btn-sm popup_edit" type="button" data-toggle="modal" data-target="#editContact" var_id="'. $value['id'] .'"><i class="fa fa-pencil"></i></button>
				<button class="btn btn-danger btn-sm popup_delete" type="button" data-toggle="modal" data-target="#deleteContact" var_id="'. $value['id'] .'"><i class="fa fa-times"></i></button>';
				$output['aaData'][] = $row;
			}
		}

		echo json_encode($output);

    }



	//--------------------------------------------------------------------

}
