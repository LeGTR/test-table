<?php 
namespace App\Controllers;
//namespace App\Controllers\Helloworld;

use App\Models\PostModel;
use CodeIgniter\Controller;
use App\Controllers\Common;

use Config\Email;
use Config\Services;

use App\Http\Controllers\Base\MasterController;

class Post extends BaseController{
	/**
	 * Access to current session.
	 *
	 * @var \CodeIgniter\Session\Session
	 */
	protected $session;

	/**
	 * Authentication settings.
	 */
	protected $config;
	/**
	 * 
	 */
	protected $security;

	public $model = '';
	public $request = '';
	public $pager = '';
	public $limit = 20;
	
	public function __construct(){
		// start session
		$this->session = Services::session();

		// load auth settings
		$this->config = config('Auth');

		$this->model = new PostModel();
		$this->request = \Config\Services::request();
        $this->pager = \Config\Services::pager();
        
	}

	public function index(){
        
    }
    
    
	public function saveContact(){
		$_post = $this->request->getPost();
		if( $_post ){
			$data = $_post;
			$data['date'] = date('Y-m-d');
			$data['date_added'] = date( 'Y-m-d H:i:s' );
			$data['status'] = 1;
			if ( isset($_post['social']) ) {
				$data['social'] = json_encode($_post['social']);
			}
			if ( isset($data['type_work']) ) {
				$data['type_work'] = implode(',', $data['type_work']);
			}
			$id = $this->model->saveClient($data);
			$name = explode(' ', $data['name']);

			$skype = '<div class="input-group m-b">
							<div class="input-group-prepend"><span class="input-group-addon"><i class="fa fa-skype"></i></span></div>
							<input type="text" class="form-control" name="social[][skype]" placeholder="Телефон" value="%s">
							<a onclick="javascript:deleteContact(this)" class="input-group-addon text-danger deleteContact">
								<span class="fa fa-times"></span>
							</a>
						</div>';

			$phone = '<div class="input-group m-b">
						<div class="input-group-prepend">
							<span class="input-group-addon"><i class="fa fa-phone"></i></span>
						</div>
						<input type="text" name="social[][phone]" class="form-control" data-mask="(999) 999-9999" placeholder="Телефон" value="%s">
						<a onclick="javascript:deleteContact(this)" class="input-group-addon text-danger deleteContact"><span class="fa fa-times"></span></a>
					</div>';

			$mail = '<div class="input-group m-b">
						<div class="input-group-prepend">
							<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
						</div>
						<input type="text" name="social[][envelope]" class="form-control" placeholder="Почта" value="%s">
						<a onclick="javascript:deleteContact(this)" class="input-group-addon text-danger deleteContact"><span class="fa fa-times"></span></a>
					</div>';

			$telegram = '<div class="input-group m-b">
							<div class="input-group-prepend">
								<span class="input-group-addon"><i class="fa fa-telegram"></i></span>
							</div>
							<input type="text" name="social[][telegram]" class="form-control" placeholder="Мессенджер" value="%s">
							<a onclick="javascript:deleteContact(this)" class="input-group-addon text-danger deleteContact"><span class="fa fa-times"></span></a>
						</div>';

			$social = '
						<label class="col-sm-2 col-form-label">Контакт</label>
						<div class="col-sm-10">
							<div class="inputHolder">
								<div class="input-group m-b">
									<div class="input-group-prepend">
										<span class="input-group-addon"><i class="fa fa-phone"></i></span>
									</div>
									<input type="text" class="form-control" name="contact" data-mask="(999) 999-9999" placeholder="Телефон" value="(916)345-6789">
								</div>
								';
			$_social = $_post['social'];
			foreach( $_social as $key => $v ){
				foreach ($v as $k2 => $v2) {
					switch ($k2) {
						case 'skype':
							$social .= sprintf($skype, $v2);
							break;
						case 'envelope':
							$social .= sprintf($mail, $v2);
							break;
						case 'telegram':
							$social .= sprintf($telegram, $v2);
							break;
						case 'phone':
							$social .= sprintf($phone, $v2);
							break;
						
						default:
							# code...
							break;
					}
				}
			}
			$social .= '
								<button class="btn btn-primary btn-sm contactAdd" data-inputmask="(999) 999-9999" data-placeholder="Телефон" type="button">+&nbsp;<i class="fa fa-phone"></i></button>
								<button class="btn btn-warning btn-sm contactAdd" data-placeholder="Почта" type="button">+&nbsp;<i class="fa fa-envelope"></i></button>
								<button class="btn btn-success btn-sm contactAdd" data-placeholder="Skype" type="button">+&nbsp;<i class="fa fa-skype"></i></button>
								<button class="btn btn-info btn-sm contactAdd" data-placeholder="Мессенджер" type="button">+&nbsp;<i class="fa fa-telegram"></i></button>
						</div>';
			echo json_encode(
				array(
					'id' 	=> $id,
					'info' 	=> $data,
					'social'=> $social,
					'name' 	=> $name
				)
			);
		}
	}

	public function updateContact(Type $var = null){
		$_post = $this->request->getPost();
		if( $_post ){
			//Array ( [client] => test [number] => test [profit] => test [date] => 16.12.2020 [type_work] => Array ( [0] => option1 ) [type_pay] => test )
			$data = $_post;
			$id = $data['id'];
			unset($data['id']);
			$data['status'] = 1;
			$data['date'] = date('Y-m-d', strtotime( $data['date'] ) );
			$data['date_added'] = date( 'Y-m-d H:i:s' );
			if ( isset($data['type_work']) ) {
				$data['type_work'] = implode(',', $data['type_work']);
			}
			$this->model->updateClient($id,$data);
			/*
			if ( isset($_post['type_work']) ) {
				foreach ($_post['type_work'] as $key => $value) {
					$this->model->saveTypeWork($id, $value);
				}
			}
			*/
			
		}
	}

	public function deleteContact($id = ''){
		if( $id ){
			$this->model->deleteContact($id);
		}
		
	}

	public function saveCustomer(){
		$_post = $this->request->getPost();
		if( $_post ){
			$data = $_post;
			$data['date'] = date('Y-m-d');
			$data['date_added'] = date( 'Y-m-d H:i:s' );
			$data['status'] = 1;
			if ( isset($_post['social']) ) {
				$data['social'] = json_encode($_post['social']);
			}
			if ( isset($data['type_work']) ) {
				$data['type_work'] = implode(',', $data['type_work']);
			}
			$id = $this->model->saveTable($data, 'customer');
			
		}

		$customer_list = $this->model->getCustomer();
		if( isset( $id ) ){
			$customer = '';
			foreach ($customer_list as $key => $value) {
				if ( !empty( $value['company_name'] ) ) {
					if( !isset( $customer_list[ $key + 1 ] ) ){
						$customer .= '<option selected value="'.$value['customer_id'].'">'.$value['company_name'].'</option>';
					}else{
						$customer .= '<option value="'.$value['customer_id'].'">'.$value['company_name'].'</option>';
					}
					
				}else{
					if( !isset( $customer_list[ $key + 1 ] ) ){
						$customer .= '<option selected value="'.$value['customer_id'].'">'.$value['name'].'</option>';
					}else{
						$customer .= '<option value="'.$value['customer_id'].'">'.$value['name'].'</option>';
					}
					
				}
				
			}
			$customer .= '';
		}else{
			$customer = '
							<option></option>';
			foreach ($customer_list as $key => $value) {
				if ( !empty( $value['company_name'] ) ) {
					$customer .= '<option value="'.$value['customer_id'].'">'.$value['company_name'].'</option>';
				}else{
					$customer .= '<option value="'.$value['customer_id'].'">'.$value['name'].'</option>';
				}
				
			}
			$customer .= '';
		}

		echo json_encode(
			array(
				'id' => $id,
				'customer' => $customer
			)
		);

	}

	public function updateCustomer(Type $var = null){
		$_post = $this->request->getPost();
		if( $_post ){
			//Array ( [client] => test [number] => test [profit] => test [date] => 16.12.2020 [type_work] => Array ( [0] => option1 ) [type_pay] => test )
			$data = $_post;
			$id = $data['id'];
			unset($data['id']);
			$data['status'] = 1;
			$data['date'] = date('Y-m-d', strtotime( $data['date'] ) );
			$data['date_added'] = date( 'Y-m-d H:i:s' );
			if ( isset($data['type_work']) ) {
				$data['type_work'] = implode(',', $data['type_work']);
			}
			$this->model->updateTable('customer_id', $id, $data, 'customer');
			/*
			if ( isset($_post['type_work']) ) {
				foreach ($_post['type_work'] as $key => $value) {
					$this->model->saveTypeWork($id, $value);
				}
			}
			*/
			
		}
	}

	public function deleteCustomer($id = ''){
		if( $id ){
			$this->model->deleteContact($id);
		}
		
	}

	
	public function saveObject(){
		$_post = $this->request->getPost();
		if( $_post ){
			$data = $_post;
			$data['date_added'] = date( 'Y-m-d H:i:s' );
			$data['status'] = 1;

			$id = $this->model->saveTable($data, 'objects');
			//echo $id;
		}

		$customer_list = $this->model->getCustomer();
		if( isset( $id ) ){
			$customer = '';
			foreach ($customer_list as $key => $value) {
				if ( !empty( $value['company_name'] ) ) {
					if( !isset( $customer_list[ $key + 1 ] ) ){
						$customer .= '<option selected value="'.$value['customer_id'].'">'.$value['company_name'].'</option>';
					}else{
						$customer .= '<option value="'.$value['customer_id'].'">'.$value['company_name'].'</option>';
					}
					
				}else{
					if( !isset( $customer_list[ $key + 1 ] ) ){
						$customer .= '<option selected value="'.$value['customer_id'].'">'.$value['name'].'</option>';
					}else{
						$customer .= '<option value="'.$value['customer_id'].'">'.$value['name'].'</option>';
					}
					
				}
				
			}
			$customer .= '';
		}else{
			$customer = '
							<option></option>';
			foreach ($customer_list as $key => $value) {
				if ( !empty( $value['company_name'] ) ) {
					$customer .= '<option value="'.$value['customer_id'].'">'.$value['company_name'].'</option>';
				}else{
					$customer .= '<option value="'.$value['customer_id'].'">'.$value['name'].'</option>';
				}
				
			}
			$customer .= '';
		}

		
		$object_list = $this->model->getObjects();
		if( isset( $id ) ){
			$object = '';
			foreach ($object_list as $key => $value) {
				if( !isset( $object_list[ $key + 1 ] ) ){
					$object .= '<option selected value="'.$value['object_id'].'">'.$value['object_name'].'</option>';
				}else{
					$object .= '<option value="'.$value['object_id'].'">'.$value['object_name'].'</option>';
				}
					
				
			}
			$object .= '';
		}else{
			foreach ($object_list as $key => $value) {
				$object .= '<option value="'.$value['object_id'].'">'.$value['object_name'].'</option>';
			}
			$object .= '';
		}
		echo json_encode(
			array(
				'id' => $id,
				'customer' => $customer,
				'object' => $object
			)
		);
		
	}

	public function saveContract(){
		$_post = $this->request->getPost();
		if( $_post ){
			$data = $_post;

			$plan = $data['plan'];
			unset($data['plan']);
			$data['date_added'] = date( 'Y-m-d H:i:s' );
			$data['contract_date'] = date( 'Y-m-d', strtotime( $data['contract_date'] ) );
			$data['status'] = 1;

			$id = $this->model->saveTable($data, 'contracts');
			echo $id;
			/*
			Array ( 
				[stage_name] => Array ( [0] => Этап №1 [1] => Этап №2 ) 
				[stage_start] => Array ( [0] => 07.12.2020 [1] => 10.12.2020 ) 
				[stage_end] => Array ( [0] => 09.12.2020 [1] => 12.12.2020 ) 
				[stage_summ] => Array ( [0] => 123 [1] => 123 ) 
				[comment] => Array ( [0] => Комментарий [1] => Комментарий ) )
			 */
			foreach ($plan['stage_name'] as $key => $value) {
				$_plan = array();
				$_plan['contract_id'] = $id;
				$_plan['stage_name'] = $value;
				if( !empty($value) ){
					$_plan['stage_name'] = $value;
				}
				
				if( isset( $plan['stage_start'][$key] ) && !empty( $plan['stage_start'][$key] ) ){
					$_plan['stage_start'] = date('Y-m-d', strtotime($plan['stage_start'][$key]));
				}
				if( isset( $plan['stage_end'][$key] ) && !empty( $plan['stage_end'][$key] ) ){
					$_plan['stage_end'] = date('Y-m-d', strtotime($plan['stage_end'][$key]));
				}
				if( isset( $plan['stage_summ'][$key] ) && !empty( $plan['stage_summ'][$key] ) ){
					$_plan['stage_summ'] = $plan['stage_summ'][$key];
				}
				if( isset( $plan['comment'][$key] ) && !empty( $plan['comment'][$key] ) ){
					$_plan['comment'] = $plan['comment'][$key];
				}
				$this->model->saveTable($_plan, 'contracts_plan');
			}
		}
	}
	//--------------------------------------------------------------------

}
