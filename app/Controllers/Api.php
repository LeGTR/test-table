<?php namespace App\Controllers;

use App\Models\NewsModel;
use CodeIgniter\Controller;
use App\Controllers\Common;

use Config\Email;
use Config\Services;

use App\Http\Controllers\Base\MasterController;

class Api extends BaseController {
	/**
	 * Access to current session.
	 *
	 * @var \CodeIgniter\Session\Session
	 */
	protected $session;

	/**
	 * Authentication settings.
	 */
	protected $config;
	/**
	 * 
	 */
	protected $security;

	public $model = '';
	public $request = '';
	public $pager = '';
	public $limit = 20;
	

	public function __construct(){
		// start session
		$this->session = Services::session();

		// load auth settings
		$this->config = config('Auth');

		$this->model = new NewsModel();
		$this->request = \Config\Services::request();
        $this->pager = \Config\Services::pager();
        
    }
    
    public function getUsers(){
        $data = $this->model->selectUsers();
        
        return $this->response->setJSON($data);
    }
    public function getUser($id = 0){
        if( !empty( $id ) ){
            $data = $this->model->selectUser( $id );
            return $this->response->setJSON($data);
        }
    }
    
    public function getPartners(){
        $data = $this->model->selectApiPartners();
        
        return $this->response->setJSON($data);
    }
    public function getPartner($id = 0){
        if( !empty( $id ) ){
            $data = $this->model->selectApiPartner( $id );
            return $this->response->setJSON($data);
        }
    }
}