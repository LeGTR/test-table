<?php 
namespace App\Controllers;
//namespace App\Controllers\Helloworld;

use App\Models\NewsModel;
use CodeIgniter\Controller;
use App\Controllers\Common;

use Config\Email;
use Config\Services;

use App\Http\Controllers\Base\MasterController;

class Objects extends BaseController{
	/**
	 * Access to current session.
	 *
	 * @var \CodeIgniter\Session\Session
	 */
	protected $session;

	/**
	 * Authentication settings.
	 */
	protected $config;
	/**
	 * 
	 */
	protected $security;

	public $model = '';
	public $request = '';
	public $pager = '';
	public $limit = 20;
	public $common;
	public $_data;
	
	public function __construct(){
		// start session
		$this->session = Services::session();

		// load auth settings
		$this->config = config('Auth');

		$this->model = new NewsModel();
		$this->request = \Config\Services::request();
		$this->pager = \Config\Services::pager();
		$this->common = new Common();
		$this->_data['header'] = $this->common->header();
		$this->_data['head'] = $this->common->head();
		$this->_data['footer'] = $this->common->footer();
		$this->_data['footer_scripts'] = $this->common->footer_scripts();
		$this->_data['left_menu'] = $this->common->left_menu();
	}

	public function index(){
        
		$data = $this->_data;
		$data['ajax'] = site_url("objects/MyDataTables");
		return view('objects', $data);
    }
    
	public function MyDataTables(){
		/*
		$reponse = array(
			'csrfName' => $this->security->get_csrf_token_name(),
			'csrfHash' => $this->security->get_csrf_hash()
		);
		*/

		$aColumns = array('user_id','username','address');
		$sTable = 'user';
		$data = array();

		$iDisplayStart = $this->request->getPost('start');
		$iDisplayLength = $this->request->getPost('length');
		$iSortCol_0 = $this->request->getPost('iSortCol_0');
		$iSortingCols = $this->request->getPost('iSortingCols');
		$sSearch = $this->request->getPost('search');
		$sEcho = $this->request->getPost('sEcho');

		$columns = $this->request->getPost('columns');
		if( isset( $columns[6]['search']['value'] ) && !empty( $columns[6]['search']['value'] ) ){
			$data['type_work'] = explode(',',$columns[6]['search']['value']);
		}

		//order[0][column]: 0
		//order[0][dir]: asc
		$order = $this->request->getPost('order');
		$order_name[] = 'object_id';
		$order_name[] = 'contract_date';
		$order_name[] = 'customer_name';
		$order_name[] = 'object_district';
		$order_name[] = 'object_address';
		$order_name[] = 'object_type';
		$order_name[] = 'work_type';
		$order_name[] = 'target';
		$order_name[] = 'object_area';

		if( $order ){
			$data['sort']['column'] = $order_name[ $order[0]['column'] ];
			$data['sort']['dir'] = $order[0]['dir'];
		}


		$data['search'] = $sSearch['value'];

		// Paging
		if(isset($iDisplayStart) && $iDisplayLength != '-1'){
			//$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
		}

		// Ordering
		if(isset($iSortCol_0))
		{
			for($i=0; $i<intval($iSortingCols); $i++)
			{
				$iSortCol = $this->request->getPost('iSortCol_'.$i, true);
				$bSortable = $this->request->getPost('bSortable_'.intval($iSortCol), true);
				$sSortDir = $this->request->getPost('sSortDir_'.$i, true);

				if($bSortable == 'true')
				{
					//$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
				}
			}
		}

		/* 
			* Filtering
			* NOTE this does not match the built-in DataTables filtering which does it
			* word by word on any field. It's possible to do here, but concerned about efficiency
			* on very large tables, and MySQL's regex functionality is very limited
			*/
		if(isset($sSearch) && !empty($sSearch)){
			for($i=0; $i<count($aColumns); $i++){
				$bSearchable = $this->request->getPost('bSearchable_'.$i, true);

				// Individual column filtering
				if(isset($bSearchable) && $bSearchable == 'true'){
					//$this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
				}
			}
		}

		// Select Data


		//$this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);               
		//$rResult = $this->db->get($sTable);

		// Data set length after filtering
		//$this->db->select('FOUND_ROWS() AS found_rows');
		//$iFilteredTotal = $this->db->get()->row()->found_rows;

		// Total data set length
		$iTotal = $this->model->getObjectsTotal($data);
		$iTotalAll = $this->model->getObjectsTotal();

		// Output
		$output = array(
			'sEcho' => intval($sEcho),
			'iTotalRecords' => $iTotalAll,
			'iTotalDisplayRecords' => $iTotal,
			'aaData' => array()
		);

		/*
		foreach($rResult->result_array() as $aRow){
			$row = array();

			foreach($aColumns as $col)
			{
					$row[] = $aRow['user_id'];
					$row[] = $aRow['username'];
					$row[] = $aRow['address'];
			}

			$output['aaData'][] = $row;
		}
        */
        
        /*
        id
        date
        name
        distict
        address
        type
        work_type
        target
        square
        progress
        action
        */
		
		$_json = $this->model->getObjects($data, $iDisplayLength, $iDisplayStart);
		if( $_json ){
			foreach ($_json as $key => $value) {
				
				$btn = '<div class="btn-group" role="group" aria-label="Редактировать/удалить объект">
                            <button type="button" class="btn btn-sm btn-info" data-id="'.$value['object_id'].'"><i class="fa fa-pencil"></i></button>
                            <button type="button" class="btn btn-sm btn-danger" data-id="'.$value['object_id'].'"><i class="fa fa-times"></i></button>
                        </div>';

				$row = array(
					0 => $value['object_id'],
					1 => $value['contract_date'],
					2 => $value['customer_name'],
					3 => $value['object_district'],
					4 => $value['object_address'],
					5 => $value['object_type'],
					6 => $value['work_type'],
					7 => $value['target'],
					8 => $value['object_area'],
					9 => '<i class="fa fa-minus text-warning"></i>',
					10 => $btn
				);
				
				
				$output['aaData'][] = $row;
			}
		}

		echo json_encode($output);

    }

	

	public function selectContactForm($id){
		$_json = $this->model->getItem($id);
		//17.12.2020
		$_json->date = date('d.m.Y', strtotime($_json->date) );
		$_json->type_work_array = $this->model->getTypeWork( $id );
		echo json_encode( $_json );
	}

	public function saveContact(){
		$_post = $this->request->getPost();
		if( $_post ){
			//Array ( [client] => test [number] => test [profit] => test [date] => 16.12.2020 [type_work] => Array ( [0] => option1 ) [type_pay] => test )
			$data = $_post;
			$data['date'] = date('Y-m-d', strtotime( $_post['date'] ) );
			$data['date_added'] = date( 'Y-m-d H:i:s' );
			$data['status'] = 1;
			if ( isset($_post['type_work']) ) {
				$data['type_work'] = implode(' ', $_post['type_work']);
			}
			$id = $this->model->saveContact($data);
			if ( isset($_post['type_work']) ) {
				foreach ($_post['type_work'] as $key => $value) {
					$this->model->saveTypeWork($id, $value);
				}
			}
			echo $id;
		}
	}

	public function updateContact(Type $var = null){
		$_post = $this->request->getPost();
		if( $_post ){
			//Array ( [client] => test [number] => test [profit] => test [date] => 16.12.2020 [type_work] => Array ( [0] => option1 ) [type_pay] => test )
			$data = $_post;
			$id = $data['id'];
			unset($data['id']);
			$data['status'] = 1;
			$data['date'] = date('Y-m-d', strtotime( $data['date'] ) );
			$data['date_added'] = date( 'Y-m-d H:i:s' );
			if ( isset($data['type_work']) ) {
				$data['type_work'] = implode(' ', $data['type_work']);
			}
			$this->model->updateContact($id,$data);
			if ( isset($_post['type_work']) ) {
				foreach ($_post['type_work'] as $key => $value) {
					$this->model->saveTypeWork($id, $value);
				}
			}
			
		}
	}

	public function deleteContact($id = ''){
		if( $id ){
			$this->model->deleteContact($id);
		}
		
	}
	

	//--------------------------------------------------------------------

}
