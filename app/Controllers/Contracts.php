<?php 
namespace App\Controllers;
//namespace App\Controllers\Helloworld;

use App\Models\NewsModel;
use CodeIgniter\Controller;
use App\Controllers\Common;

use Config\Email;
use Config\Services;

use App\Http\Controllers\Base\MasterController;

class Contracts extends BaseController{
	/**
	 * Access to current session.
	 *
	 * @var \CodeIgniter\Session\Session
	 */
	protected $session;

	/**
	 * Authentication settings.
	 */
	protected $config;
	/**
	 * 
	 */
	protected $security;

	public $model = '';
	public $request = '';
	public $pager = '';
	public $limit = 20;
	public $common;
	public $_data;
	
	public function __construct(){
		// start session
		$this->session = Services::session();

		// load auth settings
		$this->config = config('Auth');

		$this->model = new NewsModel();
		$this->request = \Config\Services::request();
		$this->pager = \Config\Services::pager();
		$this->common = new Common();
		$this->_data['header'] = $this->common->header();
		$this->_data['head'] = $this->common->head();
		$this->_data['footer'] = $this->common->footer();
		$this->_data['footer_scripts'] = $this->common->footer_scripts();
		$this->_data['left_menu'] = $this->common->left_menu();
	}

	public function index(){

		$data = $this->_data;
        $data['ajax'] = site_url("contracts/MyDataTables");
        //$data['ajax'] = '/server.php?action=contracts_list';
		return view('contracts', $data);
    }
    

	public function MyDataTables($type=''){
		/*
		$reponse = array(
			'csrfName' => $this->security->get_csrf_token_name(),
			'csrfHash' => $this->security->get_csrf_hash()
		);
		*/

		$aColumns = array('user_id','username','address');
		$sTable = 'user';
		$data = array();

		$iDisplayStart = $this->request->getPost('start');
		$iDisplayLength = $this->request->getPost('length');
		$iSortCol_0 = $this->request->getPost('iSortCol_0');
		$iSortingCols = $this->request->getPost('iSortingCols');
		$sSearch = $this->request->getPost('search');
		$sEcho = $this->request->getPost('sEcho');

		$columns = $this->request->getPost('columns');
		if( isset( $columns[6]['search']['value'] ) && !empty( $columns[6]['search']['value'] ) ){
			$data['type_work'] = explode(',',$columns[6]['search']['value']);
		}

		//order[0][column]: 0
		//order[0][dir]: asc
		$order = $this->request->getPost('order');
		$order_name[] = 'contract_id';
		$order_name[] = 'contract_date';
		$order_name[] = 'number';
		$order_name[] = 'customer_id';
		$order_name[] = 'contract_start';
		$order_name[] = 'status';
		$order_name[] = 'contract_end';
		$order_name[] = 'payment';
		$order_name[] = 'payment';

		if( $order ){
			$data['sort']['column'] = $order_name[ $order[0]['column'] ];
			$data['sort']['dir'] = $order[0]['dir'];
		}


		$data['search'] = $sSearch['value'];

		// Paging
		if(isset($iDisplayStart) && $iDisplayLength != '-1'){
			//$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
		}

		// Ordering
		if(isset($iSortCol_0))
		{
			for($i=0; $i<intval($iSortingCols); $i++)
			{
				$iSortCol = $this->request->getPost('iSortCol_'.$i, true);
				$bSortable = $this->request->getPost('bSortable_'.intval($iSortCol), true);
				$sSortDir = $this->request->getPost('sSortDir_'.$i, true);

				if($bSortable == 'true')
				{
					//$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
				}
			}
		}


		// Total data set length
		$iTotal = $this->model->getContractsTotal($data);
		$iTotalAll = $this->model->getContractsTotal();

		// Output
		$output = array(
			'sEcho' => intval($sEcho),
			'iTotalRecords' => $iTotalAll,
			'iTotalDisplayRecords' => $iTotal,
			'aaData' => array()
		);

		/*
		id
		date
		num
		client
		date_start
		progress
		date_end
		sum
		sum
		action
		 */
		
		$_json = $this->model->getContracts($data, $iDisplayLength, $iDisplayStart);
		if( $_json ){
			foreach ($_json as $key => $value) {
				
				$btn = '<div class="btn-group" role="group" aria-label="Редактировать/удалить объект">
                            <button type="button" class="btn btn-sm btn-info" data-id="'.$value['contract_id'].'"><i class="fa fa-pencil"></i></button>
                            <button type="button" class="btn btn-sm btn-danger" data-id="'.$value['contract_id'].'"><i class="fa fa-times"></i></button>
                        </div>';

				$row = array(
					0 => $value['contract_id'],
					1 => strtotime($value['contract_date']),
					2 => $value['customer_id'],
					3 => strtotime($value['contract_start']) ,
					4 => '<i class="fa fa-check text-info"></i>',
					5 => strtotime($value['contract_end']) ,
					6 => $value['payment'],
					7 => 0,
					8 => $btn
				);
				
				
				$output['aaData'][] = $row;
			}
		}

		echo json_encode($output);

    }



	//--------------------------------------------------------------------

}
