<?php namespace App\Controllers;

class Common extends BaseController
{

    public function __construct(){
        parent::__construct();
    }
	public function index(){
		return view('welcome_message');
	}

	public function header($value=''){
		return view('common/header');
	}

	public function head($data=array()){
		return view('common/head', $data);
	}
	
	public function footer_scripts($value=''){
		return view('common/footer_scripts');
	}
	public function footer($data=array()){
		$data['ajax'] = site_url("contacts/MyDataTables");
		$data['_url'] = base_url(uri_string());
		$data['s'] = '';
		return view('common/footer', $data);
	}
	public function left_menu($value=''){
		$_server = $this->request->getServer();
		$url = array();
		$url[] = array(
			'url' => '/',
			'ico' => '<i class="fa fa-home"></i>',
			'name' => 'Главная',
			'active' => ( $_server['REQUEST_URI'] == '/' ) ? TRUE : FALSE
		);
		$url[] = array(
			'url' => '#',
			'ico' => '<i class="fa fa-th-large"></i>',
			'name' => 'Услуги',
			'more' => '16/24/2',
			'active' => strpos($_server['REQUEST_URI'], 'project'),
			'child' => array(
				array(
					'url' => '/project/designing',
					'ico' => '<i class="fa fa-edit"></i>',
					'name' => 'Проектирование',
					'active' => strpos($_server['REQUEST_URI'], '/project/designing')
				),
				array(
					'url' => '#',
					'ico' => '<i class="fa fa-globe"></i>',
					'name' => 'Кадастрирование',
					'active' => false
				),
				array(
					'url' => '#',
					'ico' => '<i class="fa fa-magic"></i>',
					'name' => 'Перепланировки',
					'active' => false
				)
			)
		);
		$url[] = array(
			'url' => '#',
			'ico' => '<i class="fa fa-calendar"></i>',
			'name' => 'Планировщик',
			'active' => strpos($_server['REQUEST_URI'], 'service')
		);
		$url[] = array(
			'url' => '/contracts',
			'ico' => '<i class="fa fa-handshake-o"></i>',
			'name' => 'Договоры',
			'active' => strpos($_server['REQUEST_URI'], 'contracts')
		);
		$url[] = array(
			'url' => '/objects',
			'ico' => '<i class="fa fa-building"></i>',
			'name' => 'Объекты',
			'active' => strpos($_server['REQUEST_URI'], 'objects')
		);
		$url[] = array(
			'url' => '#',
			'ico' => '<i class="fa fa-group"></i>',
			'name' => 'Клиенты',
			'child' => array(
				array(
					'url' => '/clients/contacts',
					'ico' => '<i class="fa fa-user"></i>',
					'name' => 'Контакты',
					'active' => strpos($_server['REQUEST_URI'], 'clients/contacts')
				),
				array(
					'url' => '/clients/customers',
					'ico' => '<i class="fa fa-briefcase"></i>',
					'name' => 'Заказчики',
					'active' => strpos($_server['REQUEST_URI'], 'clients/customers')
				),
			),
			'active' => strpos($_server['REQUEST_URI'], 'clients')
		);
		$url[] = array(
			'url' => '/contacts',
			'ico' => '<i class="fa fa-address-card"></i>',
			'name' => 'Сотрудники',
			'active' => strpos($_server['REQUEST_URI'], 'contacts')
		);
		$url[] = array(
			'url' => '/partners',
			'ico' => '<i class="fa fa-smile-o"></i>',
			'name' => 'Партнеры',
			'active' => strpos($_server['REQUEST_URI'], 'partners')
		);
		$url[] = array(
			'url' => '/reports',
			'ico' => '<i class="fa fa-pie-chart"></i>',
			'name' => 'Отчеты',
			'active' => strpos($_server['REQUEST_URI'], 'reports')
		);
		$url[] = array(
			'url' => '/settings',
			'ico' => '<i class="fa fa-cogs"></i>',
			'name' => 'Настройки',
			'active' => strpos($_server['REQUEST_URI'], 'settings')
		);
		$data = array();
		$data['url'] = $url;
		return view('common/left_menu', $data);
	}

	//--------------------------------------------------------------------

}
