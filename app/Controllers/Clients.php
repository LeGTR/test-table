<?php 
namespace App\Controllers;
//namespace App\Controllers\Helloworld;

use App\Models\NewsModel;
use CodeIgniter\Controller;
use App\Controllers\Common;

use Config\Email;
use Config\Services;

use App\Http\Controllers\Base\MasterController;

class Clients extends BaseController{
	/**
	 * Access to current session.
	 *
	 * @var \CodeIgniter\Session\Session
	 */
	protected $session;

	/**
	 * Authentication settings.
	 */
	protected $config;
	/**
	 * 
	 */
	protected $security;

	public $model = '';
	public $request = '';
	public $pager = '';
	public $limit = 20;
	public $common;
	public $_data;
	
	public function __construct(){
		// start session
		$this->session = Services::session();

		// load auth settings
		$this->config = config('Auth');

		$this->model = new NewsModel();
		$this->request = \Config\Services::request();
		$this->pager = \Config\Services::pager();
		$this->common = new Common();
		$this->_data['header'] = $this->common->header();
		$this->_data['head'] = $this->common->head();
		$this->_data['footer'] = $this->common->footer();
		$this->_data['footer_scripts'] = $this->common->footer_scripts();
		$this->_data['left_menu'] = $this->common->left_menu();
	}

	public function index(){
		return redirect()->to('contacts/contacts');
	}
	public function contacts(){
		
		$json = $this->_data;
		//$data['header'] = $this->common->header();

		$data = $this->request->getGet();

		$url = array();
		$url['type_1'] = site_url('?type_work=1');
		$url['type_2'] = site_url('?type_work=2');
		$url['type_3'] = site_url('?type_work=3');

		$json['ajax'] = site_url("clients/MyDataTables");

		$json['works'] = $this->model->getWorks();
		$json['json'] = array();
		$json['url'] = $url;
		$json['get'] = '';
		$_url = '';
		if( isset( $data['type_work'] ) ){
			$json['get'] = $data['type_work'];
			$json['s'] = '&';
			if( is_array( $data['type_work'] ) ){
				
				$type_work = array();
				foreach ($data['type_work'] as $key => $value) {
					$type_work[] = 'type_work[]='.$value;
				}
				$_url = '?' . implode('&',$type_work);
			}else{
				$_url = '?type_work='.$data['type_work'];
			}
			
		}else{
			$json['s'] = '?';
		}

		$json['search'] = '';
		if( isset( $data['search'] ) ){
			$json['search'] = $data['search'];
		}

		$json['_url'] = $_url;
		$_json = $this->model->getItems($data);
		if( $_json ){
			foreach ($_json as $key => $value) {
				$json['json'][$key] = $value;
				$json['json'][$key]['action'] = '<button class="btn btn-primary btn-sm popup_edit" type="button" data-toggle="modal" data-target="#editContact" var_id="'. $value['id'] .'">Редактировать</button>
				<button class="btn btn-danger btn-sm popup_delete" type="button" data-toggle="modal" data-target="#deleteContact" var_id="'. $value['id'] .'">Удалить</button>';
			}
		}
		return view('clients/contacts', $json);
	}

	
	public function contacts2(){
		
		$json = $this->_data;
		//$data['header'] = $this->common->header();

		$data = $this->request->getGet();

		$url = array();
		$url['type_1'] = site_url('?type_work=1');
		$url['type_2'] = site_url('?type_work=2');
		$url['type_3'] = site_url('?type_work=3');

		$json['ajax'] = site_url("clients/MyDataTables");

		$json['works'] = $this->model->getWorks();
		$json['json'] = array();
		$json['url'] = $url;
		$json['get'] = '';
		$_url = '';
		if( isset( $data['type_work'] ) ){
			$json['get'] = $data['type_work'];
			$json['s'] = '&';
			if( is_array( $data['type_work'] ) ){
				
				$type_work = array();
				foreach ($data['type_work'] as $key => $value) {
					$type_work[] = 'type_work[]='.$value;
				}
				$_url = '?' . implode('&',$type_work);
			}else{
				$_url = '?type_work='.$data['type_work'];
			}
			
		}else{
			$json['s'] = '?';
		}

		$json['search'] = '';
		if( isset( $data['search'] ) ){
			$json['search'] = $data['search'];
		}

		$json['_url'] = $_url;
		$_json = $this->model->getItems($data);
		if( $_json ){
			foreach ($_json as $key => $value) {
				$json['json'][$key] = $value;
				$json['json'][$key]['action'] = '<button class="btn btn-primary btn-sm popup_edit" type="button" data-toggle="modal" data-target="#editContact" var_id="'. $value['id'] .'">Редактировать</button>
				<button class="btn btn-danger btn-sm popup_delete" type="button" data-toggle="modal" data-target="#deleteContact" var_id="'. $value['id'] .'">Удалить</button>';
			}
		}
		return view('clients/contacts2', $json);
	}

	
	public function customers(){
		$json = $this->_data;
		/*
		id -- №
		isCompany
		name -- Название
		who -- Контактное лицо
		contact -- Связь
		sum -- Сумма по договорам
		contracts -- Кол-во
		 */
		//$data['header'] = $this->common->header();

		$data = $this->request->getGet();

		$url = array();
		$url['type_1'] = site_url('?type_work=1');
		$url['type_2'] = site_url('?type_work=2');
		$url['type_3'] = site_url('?type_work=3');

		$json['ajax'] = site_url("clients/MyDataTables2");

		$json['works'] = $this->model->getWorks();
		$json['json'] = array();
		$json['url'] = $url;
		$json['get'] = '';
		$_url = '';
		if( isset( $data['type_work'] ) ){
			$json['get'] = $data['type_work'];
			$json['s'] = '&';
			if( is_array( $data['type_work'] ) ){
				
				$type_work = array();
				foreach ($data['type_work'] as $key => $value) {
					$type_work[] = 'type_work[]='.$value;
				}
				$_url = '?' . implode('&',$type_work);
			}else{
				$_url = '?type_work='.$data['type_work'];
			}
			
		}else{
			$json['s'] = '?';
		}

		$json['search'] = '';
		if( isset( $data['search'] ) ){
			$json['search'] = $data['search'];
		}

		$json['_url'] = $_url;
		$_json = $this->model->getItems($data);
		if( $_json ){
			foreach ($_json as $key => $value) {
				$json['json'][$key] = $value;
				$json['json'][$key]['action'] = '<button class="btn btn-primary btn-sm popup_edit" type="button" data-toggle="modal" data-target="#editContact" var_id="'. $value['id'] .'">Редактировать</button>
				<button class="btn btn-danger btn-sm popup_delete" type="button" data-toggle="modal" data-target="#deleteContact" var_id="'. $value['id'] .'">Удалить</button>';
			}
		}
		return view('clients/clients', $json);
	}

	public function MyDataTables( $table='' ){
		/*
		$reponse = array(
			'csrfName' => $this->security->get_csrf_token_name(),
			'csrfHash' => $this->security->get_csrf_hash()
		);
		*/

		$aColumns = array('user_id','username','address');
		$sTable = 'user';
		$data = array();

		$iDisplayStart = $this->request->getPost('start');
		$iDisplayLength = $this->request->getPost('length');
		$iSortCol_0 = $this->request->getPost('iSortCol_0');
		$iSortingCols = $this->request->getPost('iSortingCols');
		$sSearch = $this->request->getPost('search');
		$sEcho = $this->request->getPost('sEcho');

		$columns = $this->request->getPost('columns');
		if( isset( $columns[6]['search']['value'] ) && !empty( $columns[6]['search']['value'] ) ){
			$data['type_work'] = explode(',',$columns[6]['search']['value']);
		}

		//order[0][column]: 0
		//order[0][dir]: asc
		$order = $this->request->getPost('order');
		$order_name[] = 'client_id';
		$order_name[] = 'date';
		$order_name[] = 'client_id';
		$order_name[] = 'name';
		$order_name[] = 'contact';
		$order_name[] = 'task';
		$order_name[] = 'referrer';
		$order_name[] = 'manager';

		if( $order ){
			$data['sort']['column'] = $order_name[ $order[0]['column'] ];
			$data['sort']['dir'] = $order[0]['dir'];
		}


		$data['search'] = $sSearch['value'];

		// Paging
		if(isset($iDisplayStart) && $iDisplayLength != '-1'){
			//$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
		}

		// Ordering
		if(isset($iSortCol_0))
		{
			for($i=0; $i<intval($iSortingCols); $i++)
			{
				$iSortCol = $this->request->getPost('iSortCol_'.$i, true);
				$bSortable = $this->request->getPost('bSortable_'.intval($iSortCol), true);
				$sSortDir = $this->request->getPost('sSortDir_'.$i, true);

				if($bSortable == 'true')
				{
					//$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
				}
			}
		}

		/* 
			* Filtering
			* NOTE this does not match the built-in DataTables filtering which does it
			* word by word on any field. It's possible to do here, but concerned about efficiency
			* on very large tables, and MySQL's regex functionality is very limited
			*/
		if(isset($sSearch) && !empty($sSearch)){
			for($i=0; $i<count($aColumns); $i++){
				$bSearchable = $this->request->getPost('bSearchable_'.$i, true);

				// Individual column filtering
				if(isset($bSearchable) && $bSearchable == 'true'){
					//$this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
				}
			}
		}

		// Select Data


		//$this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);               
		//$rResult = $this->db->get($sTable);

		// Data set length after filtering
		//$this->db->select('FOUND_ROWS() AS found_rows');
		//$iFilteredTotal = $this->db->get()->row()->found_rows;

		// Total data set length
		$iTotal = $this->model->getItemsClientUserTotal($data);
		$iTotalAll = $this->model->getItemsClientUserTotal();

		// Output
		$output = array(
			'sEcho' => intval($sEcho),
			'iTotalRecords' => $iTotalAll,
			'iTotalDisplayRecords' => $iTotal,
			'aaData' => array()
		);

		/*
		foreach($rResult->result_array() as $aRow){
			$row = array();

			foreach($aColumns as $col)
			{
					$row[] = $aRow['user_id'];
					$row[] = $aRow['username'];
					$row[] = $aRow['address'];
			}

			$output['aaData'][] = $row;
		}
		*/
		
		$_json = $this->model->getItemsClientUser($data, $iDisplayLength, $iDisplayStart);
		if( $_json ){
			foreach ($_json as $key => $value) {
				$btn = '<div class="btn-group" role="group" aria-label="Заключение договора с контактом">
							<button type="button" class="btn btn-sm btn-success" data-target="#addClientF" data-id="'. $value['client_id'] .'"><i class="fa fa-user"></i></button>
							<button type="button" class="btn btn-sm btn-info" data-target="#addClientU" data-id="'. $value['client_id'] .'"><i class="fa fa-briefcase"></i></button>
							<button data-toggle="dropdown" class="btn btn-sm btn-primary dropdown-toggle" aria-expanded="false"><i class="fa fa-plus"></i>&nbsp;ещё</button>
							<ul class="dropdown-menu" x-placement="bottom-start" style="position: absolute; top: 29px; left: 55px; will-change: top, left;">
								<li><a class="dropdown-item" href="#" data-id="'. $value['client_id'] .'">Юридическое лицо&nbsp;<i class="fa fa-money"></i></a></li>
								<li><a class="dropdown-item" href="#" data-id="'. $value['client_id'] .'">Физическое лицо&nbsp;<i class="fa fa-money"></i></a></li>
							</ul>
						</div>';

				$row = array(
					0 => $value['client_id'],
					1 => $value['date'],
					2 => '<a class="popup_edit" href="#" var_id="'. $value['client_id'] .'">'.$value['address'].'</a>',
					3 => $value['name'],
					4 => $value['contact'],
					5 => $value['task'],
					6 => $value['referrer'],
					7 => $value['manager'],
					8 => $btn
				);
				
				/*
				
				client_id
				date
				address
				name
				contact
				task
				referrer
				manager
				comment
				date_added
				status
				deleted

				date -- Дата  round(microtime(true) * 1000)
				1 address -- Адрес объекта
				2 who -- ФИО
				3 contact -- Связь
				4 task -- Суть запроса
				5 referrer -- От кого
				6 manager -- Поручен
				7 action -- Действие
				
				foreach ($value as $key2 => $value2) {
					if( $key2 == 'work_status' ){
						if( $value2 == 1 ){
							$row[] = '<i class="fa fa-check text-info"></i>';
						}else{
							$row[] = '<i class="fa fa-minus text-warning"></i>';
						}
					}else{
						$row[] = $value2;
					}
					
					
				}
				
				$row[] = '<button class="btn btn-primary btn-sm popup_edit" type="button" data-toggle="modal" data-target="#editContact" var_id="'. $value['id'] .'"><i class="fa fa-pencil"></i></button>
				<button class="btn btn-danger btn-sm popup_delete" type="button" data-toggle="modal" data-target="#deleteContact" var_id="'. $value['id'] .'"><i class="fa fa-times"></i></button>';
				*/
				$output['aaData'][] = $row;
			}
		}

		echo json_encode($output);

    }

	public function MyDataTables2( $table='' ){
		/*
		$reponse = array(
			'csrfName' => $this->security->get_csrf_token_name(),
			'csrfHash' => $this->security->get_csrf_hash()
		);
		*/

		$aColumns = array('user_id','username','address');
		$sTable = 'user';
		$data = array();

		$iDisplayStart = $this->request->getPost('start');
		$iDisplayLength = $this->request->getPost('length');
		$iSortCol_0 = $this->request->getPost('iSortCol_0');
		$iSortingCols = $this->request->getPost('iSortingCols');
		$sSearch = $this->request->getPost('search');
		$sEcho = $this->request->getPost('sEcho');

		$columns = $this->request->getPost('columns');
		if( isset( $columns[6]['search']['value'] ) && !empty( $columns[6]['search']['value'] ) ){
			$data['type_work'] = explode(',',$columns[6]['search']['value']);
		}

		//order[0][column]: 0
		//order[0][dir]: asc
		$order = $this->request->getPost('order');
		$order_name[] = 'customer_id';
		$order_name[] = 'name';
		$order_name[] = 'address';
		$order_name[] = 'contact';
		$order_name[] = 'status';

		if( $order ){
			$data['sort']['column'] = $order_name[ $order[0]['column'] ];
			$data['sort']['dir'] = $order[0]['dir'];
		}


		$data['search'] = $sSearch['value'];

		// Paging
		if(isset($iDisplayStart) && $iDisplayLength != '-1'){
			//$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
		}

		// Ordering
		if(isset($iSortCol_0))
		{
			for($i=0; $i<intval($iSortingCols); $i++)
			{
				$iSortCol = $this->request->getPost('iSortCol_'.$i, true);
				$bSortable = $this->request->getPost('bSortable_'.intval($iSortCol), true);
				$sSortDir = $this->request->getPost('sSortDir_'.$i, true);

				if($bSortable == 'true')
				{
					//$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
				}
			}
		}

		/* 
			* Filtering
			* NOTE this does not match the built-in DataTables filtering which does it
			* word by word on any field. It's possible to do here, but concerned about efficiency
			* on very large tables, and MySQL's regex functionality is very limited
			*/
		if(isset($sSearch) && !empty($sSearch)){
			for($i=0; $i<count($aColumns); $i++){
				$bSearchable = $this->request->getPost('bSearchable_'.$i, true);

				// Individual column filtering
				if(isset($bSearchable) && $bSearchable == 'true'){
					//$this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
				}
			}
		}

		// Select Data


		//$this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);               
		//$rResult = $this->db->get($sTable);

		// Data set length after filtering
		//$this->db->select('FOUND_ROWS() AS found_rows');
		//$iFilteredTotal = $this->db->get()->row()->found_rows;

		// Total data set length
		$iTotal = $this->model->getItemsСustomerTotal($data);
		$iTotalAll = $this->model->getItemsСustomerTotal();

		// Output
		$output = array(
			'sEcho' => intval($sEcho),
			'iTotalRecords' => $iTotalAll,
			'iTotalDisplayRecords' => $iTotal,
			'aaData' => array()
		);

		/*
		foreach($rResult->result_array() as $aRow){
			$row = array();

			foreach($aColumns as $col)
			{
					$row[] = $aRow['user_id'];
					$row[] = $aRow['username'];
					$row[] = $aRow['address'];
			}

			$output['aaData'][] = $row;
		}
		*/
		
		$_json = $this->model->getItemsСustomer($data, $iDisplayLength, $iDisplayStart);
		if( $_json ){
			foreach ($_json as $key => $value) {
				$btn = '<div class="btn-group" role="group" aria-label="Заключение договора с заказчиком">
							<button type="button" class="btn btn-success newContract" data-payment-type="rouble" data-id="'. $value['customer_id'] .'"><i class="fa fa-rouble"></i></button>
							<button type="button" class="btn btn-primary newContract" data-payment-type="money" data-id="'. $value['customer_id'] .'"><i class="fa fa-money"></i></button>
						</div>';
						/*
						id -- №
						isCompany -- isCompany
						name -- Название
						who -- Контактное лицо
						contact -- Связь
						isActive -- isActive
						sum -- Сумма по договорам
						contracts -- Кол-во
						action -- Действие 
						*/
				$row = array(
					0 => $value['customer_id'],
					1 => ( !empty( $value['company_name'] ) ) ? $value['company_name'] : $value['firstname'] . ' ' . $value['lastname'] ,
					//2 => $value['firstname'] . ' ' . $value['lastname'] . ' ' . $value['middlename'],
					2 => '<a class="popup_edit" href="#" var_id="'. $value['customer_id'] .'">'. ( ( !empty( $value['name'] ) ) ? $value['name'] : $value['firstname'] . ' ' . $value['lastname'] . ' ' . $value['middlename'] ).'</a>',
					3 => $value['contact'],
					4 => $value['status'],
					5 => 0,
					6 => 0,
					7 => $btn
				);
				
				$output['aaData'][] = $row;
			}
		}

		echo json_encode($output);

    }


	public function selectContactForm($id){
		$_json = $this->model->getItemClients($id);
		//17.12.2020
		$_json->date = date('d.m.Y', strtotime($_json->date) );
		echo json_encode( $_json );
	}

	public function saveContact(){
		$_post = $this->request->getPost();
		if( $_post ){
			$data = $_post;
			$data['date'] = date('Y-m-d');
			$data['date_added'] = date( 'Y-m-d H:i:s' );
			$data['status'] = 1;
			if ( isset($_post['social']) ) {
				$data['social'] = json_encode($_post['social']);
			}
			$id = $this->model->saveContactClients($data);
			echo $id;
		}
	}

	public function updateContact(Type $var = null){
		$_post = $this->request->getPost();
		if( $_post ){
			//Array ( [client] => test [number] => test [profit] => test [date] => 16.12.2020 [type_work] => Array ( [0] => option1 ) [type_pay] => test )
			$data = $_post;
			$id = $data['id'];
			unset($data['id']);
			$data['status'] = 1;
			$data['date'] = date('Y-m-d', strtotime( $data['date'] ) );
			$data['date_added'] = date( 'Y-m-d H:i:s' );
			if ( isset($data['type_work']) ) {
				$data['type_work'] = implode(' ', $data['type_work']);
			}
			$this->model->updateContact($id,$data);
			if ( isset($_post['type_work']) ) {
				foreach ($_post['type_work'] as $key => $value) {
					$this->model->saveTypeWork($id, $value);
				}
			}
			
		}
	}

	public function deleteContact($id = ''){
		if( $id ){
			$this->model->deleteContact($id);
		}
		
	}
	

	//--------------------------------------------------------------------

}
