<?php
error_reporting(E_ALL);

function var_export54($var, $indent="") {
    switch (gettype($var)) {
        case "string":
            return '"' . addcslashes($var, "\\\$\"\r\n\t\v\f") . '"';
        case "array":
            $indexed = array_keys($var) === range(0, count($var) - 1);
            $r = [];
            foreach ($var as $key => $value) {
                $r[] = ""
                     . ($indexed ? "" : var_export54($key) . " => ")
                     . var_export54($value, "$indent");
            }
            return "[" . implode(",", $r) . "" . $indent . "]";
        case "boolean":
            return $var ? "TRUE" : "FALSE";
        default:
            return var_export($var, TRUE);
    }
}

function getRandPhones($cnt=1, $type='string', $separator =', ')
{
	$phones = [];
	for($i=1; $i<=$cnt; $i++)
	{
		$phones[] = '(9'.str_pad(mt_rand(1,68), 2,'0',STR_PAD_LEFT).') '.str_pad(mt_rand(100,999), 3,'0',STR_PAD_LEFT).'-'.str_pad(mt_rand(1,9999), 4,'0',STR_PAD_LEFT);
	}
	return 'string'==$type?join($separator, $phones):$phones;
}

$opf = ['','ИП ','ООО ', 'ПАО ','АО '];
$clients_l = ['Квартал','Собственник','Эталон','Прод-фрут','Вирта','Эмпайер','Корона','Кронбург','ПромСтройЛенМонтаж','СМУ-15','ГСК-33','Площадка','Веста'];
$fio_List = ['Гурген Амаяк Застройщикович','Куликов Аркадий Иванович','Фролов Борис Валерьевич','Матвеев Даниил Степанович','Бабич Федор Петрович','Маслов Степан Арсеньевич','Приходько Богдан Вячеславович','Сергеев Владислав Даниилович','Новиков Герман Владимирович','Нестеренко Константин Вячеславович','Матвеев Георгий Савельевич','Ткаченко Вячеслав Григорьевич','Бондаренко Владимир Вадимович','Павлов Савелий Даниилович'];

function getRandClients($cnt=1){
	global $opf, $clients_l, $fio_List;
	$data = [];
	for($i=1; $i<=$cnt; $i++)
	{
		$opf_k = array_rand($opf, 1);
	    $name = ($opf_k < 2)?$fio_List[array_rand($fio_List, 1)]:$clients_l[array_rand($clients_l, 1)];
	    $name_val = $opf[$opf_k].$name;
	    $name_opf = $opf[$opf_k];
		$name_full = ($opf_k < 2)?$name:$name_opf."&laquo;".$name."&raquo;";
	    
		$data[] = [mt_rand(0,1000), trim($name_opf), $name, $name_val, $name_full]; // first - id ID of client
	}
	return $data;
}

$partners = [     
				[ "text" => "Партнер Росреестр", "id" => 1 , "contacts_count"=>1],
                [ "text" => "Партнер Кадастрович", "id" => 2 , "contacts_count"=>27 ],
                [ "text" => "Менеджер ЖК Высотка", "id" => 3 , "contacts_count"=>197 ],
                [ "text" => "БТИШник", "id" => 4 , "contacts_count"=>217 ]
            ];

$stuff_fio =  	[
				'Муравлев Николай',
				'Левина Анастасия',
				'Федоров Иван',
				'Евгения Артамонова',
				'Левашов Алексей',
				'Евгений Шмулько Оглы',
				'Ответственный Человек',
				'Алексей Соколов',
				'Михаил Коновалов',
				'Клара Нечаева',
				'Мария Бунько',
				'Анна Дорогова',
				'Андрей Новиков',
				'Анна Бунько',
				'Антонина Нечаева',
				'Мария Шматко',
			];
$stuff = [];
foreach ($stuff_fio as $i=>$fio) {
	$stuff[] = ['text' => $fio, 'id' => $i];
}


$objects_names = ['ЖК «Квартал»', 'Парковка', 'ЖК "Арена Парк"','ЖК Ривер Парк (River Park)','ЖК Утесов (Карамышевский бриз)','ЖК Виноградный (Виноградные пруды)','ЖК Мосфильмовский','ЖК Царицыно-2','ЖК Эталон-Сити','ЖК Лобачевский','ЖК Сердце столицы','ЖК Крылатский','ЖК Хорошевский','ЖК ФилиЧета-2','МФК Aquatoria (Акватория)','ЖК Сказочный лес','ЖК Изумрудные Холмы','Новые Ватутинки','ЖК Ясно.Янино','Жилой дом На улице Коминтерна','Поселок Таунхаусов Экодолье Шолохово','ЖК Лучи','Апарт-отель «Резиденция «Сокольники»','МФК Нахимов','МФК Савеловский сити','ЖК Румянцево парк','ЖК Одинбург','ЖК "Граф Орлов", Московский пр., д. 181','ЖК "Duderhof Club", Петергофское шоссе','ЖК Летний сад','ЖК Новоясеневский','ЖК "Премьер Палас", наб. Адмирала Лазарева, ул. Пионерская, д. 50','ЖК "Классика", ул. Глухая Зеленина, д. 4 А, д. 2 А','ЖК "Империал", Киевская ул., д. 3','ЖК "Маршал", Кондратьевский проспект, участок 2','ЖК "Лондон-парк", пр. Просвещения, 43 А','ЖК "Арена Парк"','Микрорайон Новое Сертолово','ЖК Ривер Парк (River Park)'];
$object_address = ['Москва, ул. Дмитрия Устинова, дом 169, квартира 649','Москва, ул. Орловская, дом 187, квартира 773','Москва, ул. Тимакова, дом 73, квартира 433','Москва, ул. Бабаевская, дом 12, квартира 737','Москва, ул. Слепнева, дом 47, квартира 457','Москва, ул. Новый проезд, дом 17, квартира 573','Москва, ул. Рассветная, дом 77, квартира 509','Москва, ул. Подрезова, дом 176, квартира 402','Москва, ул. Гражданская 3-я, дом 47, квартира 324','Москва, ул. Боровая  (Фрунзенский), дом 159, квартира 53','Москва, ул. Арбатские Ворота пл, дом 85, квартира 798','Москва, ул. Офицерский пер, дом 77, квартира 307','Москва, ул. Энтузиастов ш, дом 190, квартира 525','Москва, ул. Войковский 2-й проезд, дом 180, квартира 969','Москва, ул. Старопименовский пер, дом 39, квартира 241','Москва, ул. Чоботовская 10-я аллея, дом 32, квартира 453','Москва, ул. Коктебельская, дом 181, квартира 521','Москва, ул. Грибоедова канала наб, дом 99, квартира 411','Москва, ул. Свечной пер, дом 171, квартира 894','Москва, ул. Войкова туп, дом 187, квартира 465','Москва, ул. Солдатский пер, дом 151, квартира 162','Москва, ул. Николая Коперника, дом 166, квартира 618','Москва, ул. Елецкая, дом 177, квартира 689','Москва, ул. Асафьева, дом 25, квартира 634','Москва, ул. Согласия, дом 175, квартира 218','Москва, ул. Радиальная 8-я, дом 97, квартира 845','Москва, ул. Северная 8-я линия, дом 96, квартира 244','Москва, ул. Полевой 2-й пер, дом 23, квартира 126','Москва, ул. Инюшенский 1-й пер, дом 104, квартира 416','Москва, ул. Ямского Поля 3-я, дом 90, квартира 628','Москва, ул. Расковой, дом 42, квартира 73','Москва, ул. Уссурийский пер, дом 129, квартира 903','Москва, ул. Дачно-Мещерский 1-й проезд, дом 134, квартира 85','Москва, ул. Чертольский пер, дом 52, квартира 856','Москва, ул. Ковенский пер, дом 135, квартира 386','Москва, ул. Хлебозаводский проезд, дом 103, квартира 226','Москва, ул. Борисовские Пруды, дом 131, квартира 823','Москва, ул. Пресненский пер, дом 113, квартира 895','Москва, ул. Двинцев, дом 104, квартира 996','Москва, ул. Рубиновая, дом 3, квартира 262','Москва, ул. Проектируемый 5509-й проезд, дом 65, квартира 580','Москва, ул. Прохладная, дом 68, квартира 303','Москва, ул. Соколиной Горы 9-я, дом 191, квартира 931','Москва, ул. 6 Пятилетки 1-й пер, дом 15, квартира 947','Москва, ул. Сердобольская  (Приморский), дом 52, квартира 18','Москва, ул. Генерала Антонова, дом 135, квартира 374','Москва, ул. 1812 года, дом 22, квартира 472','Москва, ул. Новые Сады 7-я, дом 4, квартира 508','Москва, ул. Текстильщиков 7-я, дом 37, квартира 711','Москва, ул. Ивана Сусанина, дом 61, квартира 533'];
$object_disticts = ['ЦАО','САО','СВАО','ВАО','ЮВАО','ЮАО','ЮЗАО','ЗАО','СЗАО','ЗелАО'];
$object_types=['Жилое','Нежилое'];
$object_targets=['Здание', 'Помещение', 'Квартира', 'Пристройка к жилому дому', 'Некапитальное сооружение'];
$works_types = ['primary'=>'Проектирование','warning'=>'Кадастрирование','danger'=> 'Перепланировка'];
$works = ['АР','ТЗК','ТЗК по факту','АП ТЗК','Колористический паспорт','ФАСАД','ВК','ЭОМ','ПС','О','ОВИК','КР','Акустика','Фотомонтаж','3Д визуализация','Эскиз','Обмеры','ППР','ПОС','Дизайн','АГР','Макулатура','технология','кислород','под ЭЦ'];

$clients = [[1,1,"ПАО «Кронбург»",4,59294520,"9147293919",0],[2,1,"ПАО «Квартал»",2,189726977,"9418534094",1],[3,0,"Матвеев Даниил Степанович",4,89776946,"9578392258",0],[4,1,"АО «Собственник»",4,79206673,"9829107195",0],[5,1,"ПАО «Эталон»",4,11519757,"9329467194",1],[6,1,"АО «ГСК-33»",2,178719595,"9858556653",0],[7,1,"ПАО «Собственник»",3,131956557,"9227104217",0],[8,1,"АО «СМУ-15»",2,110520530,"9128784967",1],[9,1,"ООО «Квартал»",4,5899849,"9225164736",0],[10,1,"АО «ПромСтройЛенМонтаж»",2,50205007,"9100446829",0],[11,0,"Матвеев Даниил Степанович",1,128411182,"9696990237",0],[12,0,"Фролов Борис Валерьевич",2,186173905,"9824701142",1],[13,0,"Бабич Федор Петрович",1,6427310,"93076566",0],[14,1,"ИП Бондаренко Владимир Вадимович",3,67878568,"9280298777",0],[15,1,"ИП Приходько Богдан Вячеславович",4,180253146,"9328625087",1],[16,1,"ООО «Квартал»",2,81257977,"9370214765",1],[17,1,"АО «Площадка»",2,46469758,"9824711443",1],[18,1,"ИП Бондаренко Владимир Вадимович",3,132479550,"9271712385",1],[19,1,"ООО «ПромСтройЛенМонтаж»",2,194896044,"9670937040",1],[20,1,"ИП Матвеев Георгий Савельевич",4,44938530,"9567807837",0],[21,1,"ООО «Эмпайер»",1,85810613,"9815134302",0],[22,1,"АО «Веста»",2,14113463,"9596755444",1],[23,1,"АО «СМУ-15»",3,87698691,"9267120693",0],[24,1,"ООО «СМУ-15»",1,24109583,"998773179",1],[25,1,"ИП Ткаченко Вячеслав Григорьевич",4,176245917,"9426532928",0],[26,0,"Ткаченко Вячеслав Григорьевич",3,159441183,"9511027797",0],[27,1,"ПАО «Площадка»",4,176119342,"9253559777",1],[28,1,"ИП Куликов Аркадий Иванович",1,103471368,"995239654",1],[29,1,"ПАО «ПромСтройЛенМонтаж»",3,12071738,"9866119544",0],[30,0,"Матвеев Георгий Савельевич",1,6092061,"9587353298",1],[31,1,"ИП Маслов Степан Арсеньевич",3,131455141,"9501013967",0],[32,0,"Фролов Борис Валерьевич",3,25025625,"9834638125",1],[33,1,"АО «Вирта»",1,138755795,"9679433830",0],[34,1,"ПАО «Корона»",4,75710597,"9901824534",0],[35,1,"ПАО «Веста»",3,14193470,"931077705",0],[36,1,"ПАО «Вирта»",1,116081536,"9245727238",0],[37,1,"ИП Приходько Богдан Вячеславович",2,66809253,"9261645902",1],[38,1,"ООО «Квартал»",1,157824372,"9957924002",0],[39,1,"АО «Прод-фрут»",4,197498945,"9678809558",0],[40,1,"АО «ГСК-33»",3,24083208,"929949676",0]];

$contracts = '[[1,"11/08",1559398914,"АО «Площадка»",1565150828,"",1607597610,186312092],[2,"19/02",1591348505,"ИП Сергеев Владислав Даниилович",1595295796,"АГР, Макулатура",1632465113,38209180],[3,"61/10",1583387111,"Матвеев Даниил Степанович",1584711297,"АР, ЭОМ, КР",1614795532,24766927],[4,"79/08",1586926113,"Бондаренко Владимир Вадимович",1600279151,"",1604931530,99141591],[5,"76/04",1581121655,"Павлов Савелий Даниилович",1584104954,"КР, Дизайн",1637584256,77323208],[6,"49/08",1563161527,"ПАО «Прод-фрут»",1581700576,"",1608402619,25522272],[7,"95/06",1606774465,"ПАО «ГСК-33»",1607183201,"Акустика, Макулатура",1628386718,162849111],[8,"19/09",1566897308,"Фролов Борис Валерьевич",1600178133,"под ЭЦ",1629020151,163771216],[9,"77/06",1582942232,"ООО «Веста»",1601402329,"АП ТЗК, Акустика",1608873792,55416468],[10,"98/05",1556642975,"Новиков Герман Владимирович",1601925878,"Акустика",1605361420,57741917],[11,"90/04",1601377159,"ПАО «Квартал»",1604294631,"",1618494877,135309042],[12,"84/09",1583015754,"АО «ПромСтройЛенМонтаж»",1597573009,"ТЗК по факту, ФАСАД, Дизайн, кислород",1620685235,65073040],[13,"65/06",1562838624,"Павлов Савелий Даниилович",1571256898,"ТЗК, Колористический паспорт, Обмеры",1582414652,177850853],[14,"27/04",1552529072,"ИП Матвеев Даниил Степанович",1579675479,"Обмеры, Макулатура, технология",1621215717,134506522],[15,"30/01",1551757021,"ИП Нестеренко Константин Вячеславович",1581112336,"ЭОМ, О, Фотомонтаж, Макулатура",1633043450,7455959],[16,"13/12",1590715944,"Фролов Борис Валерьевич",1606976613,"Акустика",1618893373,16671839],[17,"45/02",1605697974,"ИП Ткаченко Вячеслав Григорьевич",1606411935,"АП ТЗК, Дизайн",1638795801,13841822],[18,"88/01",1517609769,"Приходько Богдан Вячеславович",1589565488,"ВК, ПС",1617495674,156208930],[19,"14/12",1518942520,"Бабич Федор Петрович",1567329622,"ФАСАД",1621596706,35566957],[20,"69/03",1583967700,"ООО «Кронбург»",1594690024,"Обмеры, технология",1614371926,199640021],[21,"70/04",1576969198,"АО «Кронбург»",1593265264,"ЭОМ, Фотомонтаж, Дизайн, под ЭЦ",1617817503,193446512],[22,"13/10",1566239129,"ООО «Веста»",1595675067,"Макулатура",1599667732,51481641],[23,"24/02",1546904038,"ИП Фролов Борис Валерьевич",1548382447,"АП ТЗК, Эскиз, Обмеры, Макулатура",1588116846,70968221],[24,"44/01",1603657073,"ООО «Квартал»",1605736027,"ТЗК, ФАСАД, ВК",1617490819,94859886],[25,"70/09",1604390930,"ООО «Эмпайер»",1605364017,"технология",1611515767,115870922],[26,"14/02",1606183675,"ООО «Квартал»",1606790988,"кислород",1632161380,108436534],[27,"66/12",1584537810,"ООО «Вирта»",1593186945,"АР, ОВИК, Фотомонтаж, 3Д визуализация",1606817507,160669823],[28,"12/11",1602166026,"АО «СМУ-15»",1604197369,"АП ТЗК, Колористический паспорт, 3Д визуализация, технология",1605511873,71062889],[29,"35/09",1546994297,"Маслов Степан Арсеньевич",1603096693,"ПОС, кислород",1617387190,192115082],[30,"82/11",1535832845,"ИП Приходько Богдан Вячеславович",1589941796,"",1609457081,116056586],[31,"70/04",1519422476,"ПАО «Прод-фрут»",1546981421,"ТЗК по факту, ПС, ОВИК, Фотомонтаж",1598245819,166401613],[32,"38/09",1599330955,"АО «ГСК-33»",1604366288,"АР",1638219265,167357149],[33,"99/09",1572274137,"АО «Эмпайер»",1604437205,"КР",1607138983,68212936],[34,"84/03",1578254640,"ООО «Эталон»",1581811096,"ПС, под ЭЦ",1605187020,141773765],[35,"25/11",1586294082,"АО «Эмпайер»",1589863544,"ТЗК по факту, под ЭЦ",1603987144,23563454],[36,"12/11",1544766962,"ООО «Вирта»",1596959055,"",1628803271,61255438],[37,"45/07",1563302337,"Матвеев Георгий Савельевич",1593560202,"АР, ФАСАД",1620296773,148065124],[38,"34/08",1558236502,"Фролов Борис Валерьевич",1598580235,"Фотомонтаж, ППР, ПОС",1622508734,82728711],[39,"20/04",1560757423,"АО «Собственник»",1585716540,"",1632451957,130930512],[40,"97/11",1577923522,"АО «Квартал»",1596651683,"ФАСАД, 3Д визуализация",1638729306,197518939]]';


$action = null;
$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
//$term = filter_input(INPUT_GET, 'term', FILTER_SANITIZE_STRING);

if('new_contact_add_partners_list' == $action)
{
	echo json_encode($partners);
}
elseif('new_contact_add_stuff_list' == $action)
{
	
	echo json_encode($stuff);//,  "pagination" => ["more"=> true]]);
}
/*elseif('new_position' == $action){
	echo json_encode(array("position_name"=>$_POST['position_name'],"position_order"=>$_POST['position_order'],"id"=>"77")); 
}
*/
elseif('add_new_partner' == $action){
	echo json_encode(array($_POST['partner_name'], mt_rand(0,9999),mt_rand(5,9999))); 
}
elseif('partner_list' == $action){
	$flat_partners_array = [];

	foreach($partners as $partner){
		$flat_partners_array[] = array_values($partner);
	}

	echo json_encode($flat_partners_array); 
}
elseif('contacts_list' == $action)
{
	echo '[["78","1585180800000","Адмирала Макарова ул., д.23, корп.2","СН + фасад","Эмиль","Андрей Лемешкин"],["79","1605794660000","Корнеева 4А Домодедово","З\/У ","Эмиль","Вера-Дмитрий"],["80","1605805460000"," Гришина 16","БТИ","Петухов ",""],["81","1605805460000","Сколково","СК 2 кв."," Сидоров ",""],["82","1605805460000","Ленинградский проспект 29к2","СК 2 кв.","Дарья Ромас",""],["83","1587772800000","Н.Арбат 11 Не Жигули","ТП Оценить! Нужны правки АП и ТЗК, Вова в курсе!","Павленков",""],["84","1588032000000","Производственная 12","БТИ подогнать под ЕГРН - вернуть площадь","Эмиль","Армен"],["85","1587772800000","Новинский 28-35 стр.1, кв.90А","ТП ТП 45р. + ТЗК 40р.","Павленков",""],["86","1605791120000","МО Солнечногорский Кировский 124","ТП здания+ геодезия, постановка на з\/у Люба 19500, заказчику 45000","Эмиль","Максим"],["87","1605809120000","Карамышевская наб., д.2А, кв.51","С\/К по Ф Оценить","Эмиль","СУД"],["88","1605809120000","Варшавское ш., д.9, стр.28 апартаменты","С\/Н в нежилом АП, ТЗК, ТП, подача РР, 120Р.","Эмиль","Максим"],["89","1605823520000","МО","4 сотки увеличение Сидоров ","Саша Демин ",""],["90","1605830720000","Ст. Басманная 21\/4 стр.5","теплосеть ввести ","Гуленко ",""],["91","1605830720000","Филевский бульвар 6а","сгоревшее здание проекты ","Гуленко ",""],["92","1605830720000","Семашко ","проекты ","ГРанд Кадстр ","Разин"],["93","1605830720000","Радонежского С.","проекты ","ГРанд Кадстр ","Разин"],["94","1590451200000","Волоколамское ш., д.60, корп.2","С\/Н + фасад Просит помоч-прибыль 50\/50","Эмиль","СУД"],["95","1590537600000","Знаменский М. пер., д.7-10, стр.2, кв.32,33","С\/К раздел чердака, поднятие конька Уточнить, просчитать","Эмиль","Максим"],["96","1605747980000","СНТ Гермес","Присоединение участка Сбор справок 65, М\/П 80","Эмиль","Максим"],["97","1605747980000","Троцкая ул., д.9, корп.1, кв.45","С\/К АП+ТЗК 30, Распор 60, Акт и БТИ в зависимости от нарушений, ТП РР с ногами 20","Эмиль","Казакевич Коля"],["98","1605751580000","","СК","Александр ЗАЗ","Мария"],["99","1605751580000","Земляной вал 50","ордер АТИ леса ремонт фасадов Вова дал 400 я 500 короткая схема","Константин Цветной",""],["100","1605751580000","Земляной вал 50","колористически паспорт Дима АПУ 65 я 150, проект 120, ускорение 250 я 300","Константин Цветной",""],["101","1605765980000","ЖК Дыхание, Дмитровское шоссе, д.13","СК","Александр ЗАЗ","Сергей"],["102","1605780380000","Ломоносовский пр-т., д.25, корп.2, кв.211","Пока анализ ","Эмиль","Максим"],["103","1605783980000","Флоткая 7 6й этаж ","АП АП ТЗК 30 000 Электрика с надзором 22 000 Лаборатория 12 000 Петрову откат ","Петров ","Дмитрий"],["104","1605783980000","Земляновай вал 46 кв. 65","СК в письме общая 465 000","Кострома ",""],["105","1605783980000","Ленинградский пр-т., д.80, корп.17","СЭС СЭС для обученияя см.в папке задание","Эмиль","Вячеслав ГЮ"],["106","1605798380000","Басманная Старая ул., д.36, стр.2","Добавить 5й этаж","Эмиль","Руслан"],["107","1605801980000","Новочеремушкинская ул., д.38","Хостел","Морозов ","М.Женя"],["108","1592265600000","Софьи Ковалевской ул., д.20, кв.88","С\/К АП ТЗК 20, Акт по Ф 50,ЕГРН 17, ноги 3, БТИ отдельно ","Эмиль","Эльдар замер"],["109","1605809180000","Сити аппартаменты ","ТЗК АП 45","ГРанд Кадстр ",""]]';
}elseif('clients_list' == $action)
{
	echo json_encode($clients);
}
elseif('sort_positions' == $action)
{
	$result = file_put_contents('positions.txt', serialize($_POST['order']));
	$error = '';
	if(false === $result)
	{
		$error = 'Сохранение не произведено, ошибка сервера';
	}

	echo json_encode(['id'=>0,'result'=>$result,'error'=>$error]);
	
	//var_dump($_POST['order']);
	// необходимо сохранить новый порядок следования
}
elseif('delete_position' == $action)
{
	echo json_encode(['id'=>77,'error'=>'']);
	// необходимо сохранить новый порядок следования
}
elseif('new_position' == $action)
{
	$positions = file_get_contents('positions.txt');
	$positions = unserialize($positions);
	
	
	$positions_max_id = max(array_column($positions, 'id'));
	$positions_max_id++;

	array_unshift($positions, [
		'id'=>$positions_max_id, 
		'position_name'=> $_POST['position_name'], 
		'position_order'=>$_POST['position_order']
	]);
	$positions_order  = array_column($positions, 'position_order');
	array_multisort($positions_order, SORT_ASC, $positions);

	$result = file_put_contents('positions.txt', serialize($positions));
	$error = '';
	if(false === $result)
	{
		$error = 'Сохранение не произведено, ошибка сервера';
	}
	echo json_encode([
		'position_order'=>0, 
		"position_name"=>$_POST['position_name'], 
		"position_order"=>$_POST['position_order'], 
		'id'=>$positions_max_id,
		'result'=>$result, 
		'error'=>$error
	]);
	// необходимо сохранить новый порядок следования
}
elseif('get_positions_list_as_JSON' == $action)
{
	$positions = file_get_contents('positions.txt');
	$positions = unserialize($positions);
	echo json_encode($positions);
}elseif('contracts_list' == $action){
	echo $contracts;
}
elseif('objects_list' == $action){
	$out = [];
	$types = [1,2,3];
	for($i=1;$i<100;$i++)
	{
		
		//shuffle($types);
		$type = array_rand(array_flip($types), mt_rand(1,3));
		$type = is_array($type)?join(',',$type):$type;
		$out[] = [
			$i, 
			mt_rand(1575716540, time()), 
			array_rand(array_flip($objects_names), 1),
			array_rand($object_disticts, 1), 
			mt_rand(0,1) == 0?explode(', квартира ',array_rand(array_flip($object_address), 1))[0]:array_rand(array_flip($object_address), 1), 
			array_rand(array_flip($object_types), 1),
			mt_rand(0,1) == 0?0:$type,
			array_rand(array_flip($object_targets), 1),
			mt_rand(10,200),
			mt_rand(0,1)
		];
	}
	echo json_encode($out);
}
elseif('projects_planning_list' == $action){
	include('color.php');
	$data = [];
	for($i=1;$i<150; $i++)
	{
		$done = round(mt_rand(0,100)/100,2); // % сколько сделано
		$work = array_rand($works, mt_rand(1,3));
		if(!is_array($work)) $work = [$work];
		//$client = [0];
		
		//var_dump($client);//exit();
		$data[] = [
			$i, 
			[ // массив инфы об объекте: ID, название, адрес
				mt_rand(0,100), 
				array_rand(array_flip($objects_names), 1), 
				mt_rand(0,1) == 0?explode(', квартира ',array_rand(array_flip($object_address), 1))[0]:array_rand(array_flip($object_address), 1)
			], 
			[ // массив инфы о сотруднике, ответственном за проект: ID, ФИО
				mt_rand(1,count($stuff_fio)), 
				array_rand(array_flip($stuff_fio), 1)
			],
			mt_rand(1575716540, time()),
			getRandClients(),
			[$done, numberToColorHsl($done, 0, 1)], // Цвет зависит от процента выполненности работы
			$work, // Вернет массив ID`шников работ
			mt_rand(0,2), // Статус проекта - завершен = 0 или еще нет = все остальное
		];

	}
	echo json_encode($data);
}
elseif('get_clients_list_clear' == $action){
	$data = [];
	define('PAGE_SIZE', 10);
	$page = isset($_GET['page'])?intval($_GET['page']):0;
	foreach($opf as $i=>$the_opf)
	{
	    if(0 == $i)
	    {
	        foreach($fio_List as $fio)
	        {
	            $data[] = ['id' => mt_rand(1,10000), 'text' => $fio];
	        }
	    }
	    elseif(1 == $i)
	    {
	        foreach($fio_List as $fio)
	        {
	            $data[] = ['id' => mt_rand(1,10000), 'text' => $the_opf.$fio];
	        }
	    }
	    else{
	        foreach($clients_l as $client)
	        {
	            $data[] = ['id' => mt_rand(1,10000), 'text' => $the_opf.'"'.$client.'"'];
	        }        
	    }
	}

	$filter = array_filter($data, function($v){
	    return false !== stripos(mb_strtolower($v['text']), mb_strtolower($_GET['term']));
	});
	$more_pages = ($page+1)*PAGE_SIZE<=count($filter);

	$filter = array_slice($filter, $page * PAGE_SIZE, PAGE_SIZE);

	echo json_encode(['results'=>$filter, "pagination" => ['more'=>$more_pages]]);
}

elseif('get_objects_list_clear' == $action){
	$data = [];
	define('PAGE_SIZE', 10);
	$page = isset($_GET['page'])?intval($_GET['page']):0;

	foreach($objects_names as $i => $object)
	{
		$data[] = ['id'=>mt_rand(1,1000), 'text'=>$object, 'address'=> $object_address[$i]];
	}
	$filter = array_filter($data, function($v){
	    return false !== stripos(mb_strtolower($v['address']), mb_strtolower($_GET['term'])) || false !== stripos(mb_strtolower($v['text']), mb_strtolower($_GET['term']));
	});
	$more_pages = ($page+1)*PAGE_SIZE<=count($filter);

	$filter = array_slice($filter, $page * PAGE_SIZE, PAGE_SIZE);

	echo json_encode(['results'=>$filter, "pagination" => ['more'=>$more_pages]]);
}

